function get_real_cost

settings_car
spc = [0, 0, 0, 0, 0]
% sim
sim = 1;
for run=1:5
    for policy=1:5
        for iter=1:5
            total_cost = get_cost(cost, sim, run, policy, iter);
            spc(policy) = spc(policy) + total_cost;
        end
    end
end

spc/25


% rw

rwpc = [0, 0, 0, 0, 0]
sim = 0;
for run=1:3
    for policy=1:5
        for iter=1:5
            total_cost = get_cost(cost, sim, run, policy, iter);
            rwpc(policy) = rwpc(policy) + total_cost;
        end
    end
end

rwpc/(3*5)

function total_cost = get_cost(cost, sim, run, n1, n2)
if sim
    folder = 'R0p8_gpops2sim/run';
else
    folder = 'R0p8_sim2rw/run';
end
load(strcat(folder, num2str(run), '/rollout_full_', num2str(n1), num2str(n2)));

total_cost = 0;
for i=1:length(yy)
    total_cost = total_cost + cost.fcn(cost,yy(i,:),zeros(length(yy(i,:))));
end