function dynamics_car_ss()

% Settings for car from paper
param.B = 7;
param.C = 1.6;
param.D = 1.0;
param.rF = 0.3;
param.rR = param.rF;
param.lF = 1.1;
param.lR = 1.59;
param.m = 1450;
param.Iz = 2741.9;
param.IwF = 1.8;
param.IwR = param.IwF;
param.h = 0.4;
param.muW = 0;		% guess
param.MaxTorque = 3000;    % guess
param.MaxWheelAngle = 30*pi/180.0; % guess

R = 1.5;
beta = -43*pi/180;

x0 = [3 8*pi/180 1200 8 32];
options = optimoptions('fsolve','Display','iter');
[x, fval] = fsolve(@solve_ss,x0,options);

V_ss = x(1)
delta_ss = x(2)*180/pi
T_ss = x(3)
wF_ss = x(4)
wR_ss = x(5)


Vx_ss = V_ss*cos(beta);
Vy_ss = V_ss*sin(beta);
dpsi_ss = V_ss/R;

z = [Vx_ss;Vy_ss;dpsi_ss;wF_ss;wR_ss];
dz = dynamics_car(1,z,delta_ss*pi/180.0,T_ss)

function out = solve_ss(x)

V = x(1);
delta = x(2);
T = x(3);
omegaF = x(4);
omegaR = x(5);

dpsi = V/R;

V_Fx = V*cos(beta - delta) + dpsi*param.lF*sin(delta);
V_Fy = V*sin(beta - delta) + dpsi*param.lF*cos(delta);
V_Rx = V*cos(beta);
V_Ry = V*sin(beta) - dpsi*param.lR;

% Calculate friction coefficients
s_Fx = xSlip(V_Fx,omegaF,param.rF);
s_Fy = ySlip(V_Fy,omegaF,param.rF);
s_F = sqrt(s_Fx^2+s_Fy^2);

s_Rx = xSlip(V_Rx,omegaR,param.rR);
s_Ry = ySlip(V_Ry,omegaR,param.rR);
s_R = sqrt(s_Rx^2+s_Ry^2);

mu_Fx = 0.0; mu_Fy = 0.0;
%if (s_F ~= 0.0)
if (abs(s_F) > 0.0001)
    mu_Fx = -s_Fx/s_F*MF(param.B, param.C, param.D, s_F);
    mu_Fy = -s_Fy/s_F*MF(param.B, param.C, param.D, s_F);
end

mu_Rx = 0.0; mu_Ry = 0.0;
%if (s_R ~= 0.0)
if (abs(s_R) > 0.0001)
    mu_Rx = -s_Rx/s_R*MF(param.B, param.C, param.D, s_R);
    mu_Ry = -s_Ry/s_R*MF(param.B, param.C, param.D, s_R);
end


% Calculate friction forces
GRAVITY = 9.81;
num = param.lR*param.m*GRAVITY - param.h*param.m*GRAVITY*mu_Rx;
den = param.h*(mu_Fx*cos(delta) - mu_Fy*sin(delta) - mu_Rx);
f_Fz = num/(param.lF + param.lR + den);
f_Rz = param.m*GRAVITY - f_Fz;
f_Fx = mu_Fx*f_Fz;
f_Fy = mu_Fy*f_Fz;
f_Rx = mu_Rx*f_Rz;
f_Ry = mu_Ry*f_Rz;

% steady-state dynamics
% % xdot = state.Vx*cos(psi) - Vy*sin(psi); % inertial x
% % ydot = Vx*sin(psi) + Vy*cos(psi); % inertial y
% Vxdot = 1/param.m*(param.m*Vy*dpsi + f_Fx*cos(delta) - f_Fy*sin(delta) + f_Rx); % body x velocity
% Vydot = 1/param.m*(-param.m*Vx*dpsi + f_Fx*sin(delta) + f_Fy*cos(delta) + f_Ry); % body y velocity
% % psidot = dpsi; % yaw
% dpsidot = 1/param.Iz*((f_Fy*cos(delta) + f_Fx*sin(delta))*param.lF- f_Ry*param.lR); % yaw rate
% % wFdot = 1/param.IwF*(torque - f_Fx*param.rF - param.muW*omegaF*omegaF);
% % wRdot = 1/param.IwR*(torque - f_Rx*param.rR - param.muW*omegaR*omegaR);
% wFdot = 1/param.IwF*(T - f_Fx*param.rF);
% wRdot = 1/param.IwR*(T - f_Rx*param.rR);

eq1 = param.m*V^2/R*sin(beta) + f_Fx*cos(delta) - f_Fy*sin(delta) + f_Rx; % body x velocity
eq2 = -param.m*V^2/R*cos(beta) + f_Fx*sin(delta) + f_Fy*cos(delta) + f_Ry; % body y velocity
eq3 = (f_Fy*cos(delta) + f_Fx*sin(delta))*param.lF- f_Ry*param.lR; % yaw rate
eq4 = T - f_Fx*param.rF;
eq5 = T - f_Rx*param.rR;

out = [eq1;eq2;eq3;eq4;eq5];

end

end

function slip = xSlip( Vx, omega, r)
%if (omega*r ~= 0.0)
if (abs(omega*r) > 0.001)
	slip = (Vx - omega*r)/(omega*r);
else
    slip = 0.0;
end
end

function slip = ySlip( Vy, omega, r)
%if (omega*r ~= 0.0)
if (abs(omega*r) > 0.001)
    slip = Vy/(omega*r);
else
    slip = 0.0;
end
end

function ret = MF( B, C, D, s)
ret = D*sin(C*atan(B*s));
end