%% cartPole_learn.m
% *Summary:* Script to learn a controller for the car swingup
%
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
% rng(1651)
rng('shuffle')

use_gpops = 1;
use_prior = 0;
use_hw = 0;
use_real_hardware = 1;
% prior = 'R0p8_gpops2sim/car_4_H40.mat';
prior = 'R0p8_gpops2sim/car_2_H40.mat';
% prior = 'car_from_gpops_med_noise_no_omega/car_7_H40.mat';
% prior = 'car_6_H40.mat';
gpops = 'gpops_gp_policy_R0p8.mat';

settings_car;                      % load scenario-specific settings
basename = 'car_';           % filename used for saving data


if use_gpops
    
    % load saved data
    gpops = load(gpops);
    policy = gpops.policy;
    
end

%% load and use prior information
if use_prior
    prior_info = load(prior);
    
    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
    
    % if the prior gp used gpmodel_rw information, then we must account for
    % that in the prior
    % really, we should also probably use the data we collected the last
    % time we ran this system
    if isfield(dynmodel.prior,'gpmodel_rw')
        
        % unless there is good reason not to, copy the data from last time
        % we ran the real world
        x = dynmodel.prior.gpmodel_rw.inputs;
        y = dynmodel.prior.gpmodel_rw.targets;
        
        % need to add back in the prior if it exists -- but it should have
        % been removed when we ran in sim last time
        if isfield(dynmodel.prior.gpmodel_rw,'prior')
            disp('********* WARNING--WE SHOULD NOT BE HERE! **********');
            [~, D] = size(dynmodel.prior.gpmodel_rw.inputs);
            % prior gp mean
            prior_mean = zeros(size(y(:,difi)));
            for i=1:length(y)
                prior_mean(i,:) = dynmodel.prior.gpmodel_rw.prior.fcn(...
                    dynmodel.prior.gpmodel_rw.prior,...
                    dynmodel.prior.gpmodel_rw.inputs(i,:)', zeros(D));
            end
            y(:,difi) = y(:,difi) + prior_mean;
            y(:,plant.angi) = wrapToPi(y(:,plant.angi));
            dynmodel.prior.gpmodel_rw = rmfield(dynmodel.prior.gpmodel_rw,'prior');
        end
        
    end
    
end

%% load and use hardware information
if use_hw
    hw_info = load(hw);
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
end



% tic
% 2. Initial J random rollouts
if use_real_hardware
    jj = 1;
    if  use_gpops || use_prior
        is_random_policy = false;
    else
        is_random_policy = true;
    end
    applyController_hardware_cpp;
    
    latent1 = zeros(size(xx,1)+1, size(xx,2));
    latent1(1:end-1,:) = xx;
    latent1(end,1:end-1) = yy(end, :);
    latent{jj} = latent1;
    
    rc1 = zeros(1,H);
    for i = 1:H
        rc1(i) = cost.fcn(cost,yy(i,:)',zeros(length(dyno)));
    end
    realCost{jj} = rc1;
    
else
    for jj = 1:J
        if use_prior || use_gpops %isfield(dynmodel,'prior')
            [xx, yy, realCost{jj}, latent{jj}] = ...
                rollout(gaussian(mu0, S0), policy, H, plant, cost);
        else
            [xx, yy, realCost{jj}, latent{jj}] = ...
                rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
        end
        x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model        
    end
end
if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_car;
end
% return;

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Save data
filename = [basename num2str(0) '_H' num2str(H)]; save(filename);

is_random_policy = false;

% 3. Controlled learning (N iterations)
for j = 1:N
    
    % 3. Save data
    filename = [basename num2str(j) '_H' num2str(H)]; save(filename);
    
    trainDynModel;   % train (GP) dynamics model
    learnPolicy;     % learn policy
    if use_real_hardware
        applyController_hardware_cpp;
    else
        applyController; % apply controller to system
    end
    disp(['controlled trial # ' num2str(j)]);
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_car;
    end
end
