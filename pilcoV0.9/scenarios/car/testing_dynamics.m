% testing dynamics
function testing_dynamics()
R = 7;
V = 4;
beta = -6*pi/180;
omegaF = 12.59;
omegaR = 29.11;
TF_ss = -1363;
TR_ss = 1448.8;
delta_ss = 1.85*pi/180;

Vx = V*cos(beta);
Vy = V*sin(beta);
dpsi = V/R;

z = [Vx Vy dpsi omegaF omegaR];

% dynamics_car(1,z,delta_ss,TF_ss,TR_ss);

rF = 0.3;
rR = rF;
lF = 1.1;
lR = 1.59;
m = 1450;
Iz = 2741.9;
IwF = 1.8;
IwR = IwF;
h = 0.4;
g = 9.81;
B = 7;
C = 1.6;
D = 1.0;

f_Ry = m*V^2/R*cos(beta)*lF/(lF+lR);
f_Rz = (m*g*lF - m*h*V^2*sin(beta)/R)/(lF+lR);
f_Fz = (m*g*lR + m*h*V^2*sin(beta)/R)/(lF+lR);

mu_Ry = f_Ry/f_Rz;
tan_alpha_R = (V*sin(beta) - V*lR/R)/(V*cos(beta));
x0 = [0.1 0.1 0.1];
% options = optimoptions('fsolve','Display','iter');
[x, fval] = fsolve(@sRcalc,x0) %,options);
s_R = x(1);
s_Rx = x(2);
s_Ry = x(3);

mu_R = MF(s_R);
mu_Rx = -s_Rx/s_R*mu_R;
f_Rx = mu_Rx*f_Rz;

f_F = sqrt(m^2*V^4/R^2 + f_Rx^2 + f_Ry^2 + 2*m*V^2/R*(f_Rx*sin(beta) - f_Ry*cos(beta)));

mu_F = f_F/f_Fz;
s_F = tan(asin(mu_F/D)/C)/B;

[x, fval] = fsolve(@sFcalc,x0) %,options);
s_Fx = x(1);
s_Fy = x(2);
delta = x(3);
delta_deg = delta*180/pi

f_Fx = -s_Fx/s_F*MF(s_F)*f_Fz;
f_Fy = -s_Fy/s_F*MF(s_F)*f_Fz;

w_F = (V*cos(beta-delta) + dpsi*lF*sin(delta))/((1+s_Fx)*rF)
w_R = (V*cos(beta))/((1+s_Rx)*rR)

TF = f_Fx*rF
TR = f_Rx*rR

f_Fz
f_Rz
f_Fx
f_Fy
f_Rx
f_Ry

function F = sRcalc(x)
sR = x(1);
sRx = x(2);
sRy = x(3);
s1 = tan_alpha_R - sRy/(sRx + 1);
s2 = sR - sqrt(sRx^2 + sRy^2);
s3 = mu_Ry + sRy/sR*MF(sR);
F = [s1;s2;s3];
end

function F = sFcalc(x)
sFx = x(1);
sFy = x(2);
d = x(3);
s1 = m*V^2/R*sin(beta) - (f_F/s_F*(sFx*cos(d) - sFy*sin(d)) - f_Rx);
s2 = sFy/(1+sFx) - (V*sin(beta-d)+V*lF*cos(d)/R)/(V*cos(beta-d)+V*lF*sin(d)/R);
s3 = s_F - sqrt(sFx^2 + sFy^2);
F = [s1;s2;s3];
end

function out = MF(s)
out = D*sin(C*atan(B*s));
end

end




