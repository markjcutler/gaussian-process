#!/usr/bin/env python

###################################################
# plot_car_snapshots.py -- Plot snapshots of toy car
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday, 19 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from scipy import ndimage
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
from matplotlib.ticker import MaxNLocator
import  mpl_toolkits.axisartist.angle_helper as angle_helper
from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)


def load_gpops(path):
    data = scipy.io.loadmat(path)
    time = np.array(data['Time'])
    state = np.array(data['State'])
    delta = np.array(data['Force'])

    time = time[0]
    delta = delta[0]

    Vx = state[0, :]
    Vy = state[1, :]
    dpsi = state[2, :]
    omega = state[3, :]

    return time, Vx, Vy, dpsi, omega, delta


def load_pilco(path):
    data = scipy.io.loadmat(path)
    T = data['T']
    dt = data['dt']
    time = np.arange(0, T+dt, dt)

    xx = np.array(data['xx'])
    yy = np.array(data['yy'])

    Vx = xx[:, 0]
    Vy = xx[:, 1]
    dpsi = xx[:, 2]
    omega = xx[:, 3]
    delta = xx[:, 4]

    # add the last value of yy to xx
    Vx = np.append(Vx, yy[-1, 0])
    Vy = np.append(Vy, yy[-1, 1])
    dpsi = np.append(dpsi, yy[-1, 2])
    omega = np.append(omega, yy[-1, 3])
    delta = np.append(delta, delta[-1])

    return time, Vx, Vy, dpsi, omega, delta


def load_rollout(path):
    data = scipy.io.loadmat(path)
    xx = np.array(data['xx'])
    yy = np.array(data['yy'])

    Vx = xx[:, 0]
    Vy = xx[:, 1]
    dpsi = xx[:, 2]
    delta = xx[:, 3]

    # add the last value of yy to xx
    Vx = np.append(Vx, yy[-1, 0])
    Vy = np.append(Vy, yy[-1, 1])
    dpsi = np.append(dpsi, yy[-1, 2])
    delta = np.append(delta, delta[-1])

    dt = 0.1
    T = len(Vx)*dt
    time = np.arange(0, T, dt)

    return time, Vx, Vy, dpsi, np.zeros(len(Vx)), delta


def load_rollouts(path, runs, mid, files):
    vstack = False
    for r in runs:
        for f in files:
            rollout = path + r + mid + f
            print rollout
            if not vstack:
                T, Vx, Vy, Dpsi, Omega, Delta = load_rollout(rollout)
                vstack = True
            else:
                t, vx, vy, dpsi, omega, delta = load_rollout(rollout)
                T = np.vstack((T, t))
                Vx = np.vstack((Vx, vx))
                Vy = np.vstack((Vy, vy))
                Dpsi = np.vstack((Dpsi, dpsi))
                Omega = np.vstack((Omega, omega))
                Delta = np.vstack((Delta, delta))

    t_mean = np.mean(T, axis=0)
    t_std = np.std(T, axis=0)
    Vx_mean = np.mean(Vx, axis=0)
    Vx_std = np.std(Vx, axis=0)
    Vy_mean = np.mean(Vy, axis=0)
    Vy_std = np.std(Vy, axis=0)
    Dpsi_mean = np.mean(Dpsi, axis=0)
    Dpsi_std = np.std(Dpsi, axis=0)
    Omega_mean = np.mean(Omega, axis=0)
    Omega_std = np.std(Omega, axis=0)
    Delta_mean = np.mean(Delta, axis=0)
    Delta_std = np.std(Delta, axis=0)

    return t_mean, t_std, Vx_mean, Vx_std, Vy_mean, Vy_std, Dpsi_mean, Dpsi_std, Omega_mean, Omega_std, Delta_mean, Delta_std


def plot_trajectory(time, Vx, Vy, dpsi, omega, delta, x0, des_dt=None):

    n = len(time)
    x = np.zeros(n)
    y = np.zeros(n)
    psi = np.zeros(n)

    x[0] = x0

    for i in range(1, n):
        dt = time[i] - time[i-1]
        if (des_dt is not None and dt > des_dt):
            print dt
        psi[i] = psi[i-1] + dpsi[i]*dt
        dx = Vx[i]*np.cos(psi[i]) - Vy[i]*np.sin(psi[i])
        dy = Vx[i]*np.sin(psi[i]) + Vy[i]*np.cos(psi[i])
        x[i] = x[i-1] + dx*dt
        y[i] = y[i-1] + dy*dt

    return x, y, psi


def lin_interp(t, t0, t1, x0, x1):
    x = x0 + (x1 - x0)*(t-t0)/(t1-t0)
    return x


def process_trajectory(t, x, y, psi, des_dt):
    xnew = []
    ynew = []
    psinew = []
    tnew = []
    n = len(t)

    for i in range(1, n):
        dt = t[i] - t[i-1]
        m = np.floor(dt/des_dt)
        for j in range(int(m)):
            print m
            tt = t[i-1]+(j+1)*des_dt
            xx = lin_interp(tt, t[i-1],
                            t[i], x[i-1], x[i])
            yy = lin_interp(tt, t[i-1],
                            t[i], y[i-1], y[i])
            pp = lin_interp(tt, t[i-1],
                            t[i], psi[i-1], psi[i])
            xnew.append(xx)
            ynew.append(yy)
            psinew.append(pp)
            tnew.append(tt)
        xnew.append(x[i])
        ynew.append(y[i])
        psinew.append(psi[i])
        tnew.append(t[i])

    print len(xnew)
    return np.array(tnew), np.array(xnew), np.array(ynew), np.array(psinew)


def add_car(x, y, psi, ax, fig, image):
    rotate_car = ndimage.rotate(image, psi*180.0/np.pi)

    dat_coord = [(x, y)]
    # transform the two points from data coordinates to display coordinates:
    tr1 = ax.transData.transform(dat_coord)
    # create an inverse transversion from display to figure coordinates:
    inv = fig.transFigure.inverted()
    tr2 = inv.transform(tr1)

    #scale_val = 2*0.035*(np.cos(np.pi + 4.0*psi)/2.0 + 0.5)
    max_scale = 0.09

    while (psi > np.pi/2.0):
        psi -= np.pi/2.0
    while (psi < 0):
        psi += np.pi/2.0

    scale_val = max_scale*(np.sin(psi) + np.cos(psi)) - max_scale

    datco = [tr2[0, 0]-0.05-scale_val/2.0,
             tr2[0, 1]-0.05-scale_val/2.0,
             0.1 + scale_val, 0.1 + scale_val]
    axins = fig.add_axes(datco)
    axins.set_axis_off()
    axins.imshow(rotate_car, alpha=1.0)
    print 'adding car'


def plot_cars(t, dt, x, y, psi, ax, fig, image, limits):
    t_car = np.arange(0, t[-1], dt)
    current_t = 0
    for i in range(len(x)):
        try:
            if (t[i] >= t_car[current_t] and x[i] >= limits[0] and
                x[i] <= limits[1] and y[i] >= limits[2] and y[i] <= limits[3]):
                add_car(x[i], y[i], psi[i], ax, fig, image)
                current_t += 1
        except:
            pass


def plot_car(t_goal, t, x, y, psi, ax, fig, image, limits):
    for i in range(len(x)):
        if (t_goal <= t[i] and x[i] >= limits[0] and
            x[i] <= limits[1] and y[i] >= limits[2] and y[i] <= limits[3]):
            print t_goal
            print t[i]
            add_car(x[i], y[i], psi[i], ax, fig, image)
            #add_car2(x[i], y[i], psi[i], fig, image)
            return i


path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/car/'
path_gpops = path_main + 'training_car_R0p8.mat'

markersize = 50

color = [sim_dark, '#84DEBD']
image = plt.imread(path_main + 'car_square_dark_sim.png')

legend_fontsize = 29
axis_label_fontsize = 29
linewidth = 1.5
axis_tick_fontsize = 25
annotation_fontsize = 27
linestyle = ['-', '-']

xmin = -2.0
xmax = 2.0
ymin = -1
ymax = 3.1
dt = 1/30.0

t, vx, vy, dpsi, omega, delta = load_gpops(path_gpops)
x, y, psi = plot_trajectory(t, vx, vy, dpsi, omega, delta, 0)

t, x, y, psi = process_trajectory(t, x, y, psi, 1/30.0)

t_goal = np.linspace(0, t[-1], 100)
for i in range(len(t_goal)):
    fig, ax = plt.subplots()

    ax.set_xlabel(r'$x$ $(m)$', size=axis_label_fontsize)
    ax.set_ylabel(r'$y$ $(m)$', size=axis_label_fontsize)

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])

    ya = ax.get_yaxis()
    ya.set_major_locator(MaxNLocator(integer=True))
    xa = ax.get_xaxis()
    xa.set_major_locator(MaxNLocator(integer=True))

    cur_aspect = ax.get_aspect()
    print cur_aspect

    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(axis_tick_fontsize)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(axis_tick_fontsize)

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    limits = np.array([xmin, xmax, ymin, ymax])

    ind = plot_car(t_goal[i], t, x, y, psi, ax, fig, image, limits)
    ax.plot(x[:ind], y[:ind], color=color[0],
            linestyle=linestyle[0], linewidth=4)

    # add_car(0, 0, i/(2*np.pi), ax, fig, image)

    plt.savefig(path_main + 'opt_control/car_traj_oc_' + str(i) + '.png',
                format='png', transparent=True, bbox_inches='tight',
                dpi=200) #300)
    plt.cla()
