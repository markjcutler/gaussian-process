%% dynamics_cp.m
% *Summary:* Implements ths ODE for simulating car dynamics.
%
%    function dz = dynamics_cp(t, z, f)
%
%
% *Input arguments:*
%
%	t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f1,f2 inputs ([steering angle, torque])
%
% *Output arguments:*
%
%   dz    state derivative wrt time
%
%
% Note: It is assumed that the state variables are of the following order:
%       Vx:       [m/s]   body frame x velocity
%       Vy:       [m/s]   body frame y velocity
%       dpsi:     [rad/s] yaw rate
%       omega:    [rad/s] wheel angular velocity
%
%
% A detailed derivation of the dynamics can be found in:
% 
% Velenis, Efstathios, Emilio Frazzoli, and Panagiotis Tsiotras. 
% "Steady-state cornering equilibria and stabilisation for a vehicle during 
%  extreme operating conditions." 
% International Journal of Vehicle Autonomous Systems 8.2 (2010): 217-241.
%

function dz = dynamics_car(t,z,f)
% toc
% tic
%% Code

% RC03 -- drift tires
param.B = 3;	% From initial optimization
param.C = 1.5;     % From initial optimization
param.D = 0.2;	% From initial optimization

param.rF = 0.0606 / 2.0; % measured
param.rR = param.rF;
param.lF = 0.1;	% measured
param.lR = 0.107;	% measured
param.MaxWheelAngle = 18*pi/180.0;
param.MaxWheelSpeed = 352;          % roughly measured
param.BackEMFGain_up = 20.0; % estimated using step data from none-loaded wheels
param.TorqueOffset = 0.115; % estimated using step data from none-loaded wheels
param.TorqueMin = 0.12; % estimated using step data from none-loaded wheels
param.BackEMFGain_down = 1.6; % estimated using step data from none-loaded wheels
param.m = 1.143; % measured
param.Iz = 1.0 / 12.0 * param.m ...
           * (0.1 * 0.1 + 0.2 * 0.2); % estimated by bar of width 10cm,
                                      % length 20cm, mass 1143 g
param.IwF = 0.8*3.2e-5; % approximate %wheel weight = 0.044 kg
param.IwR = param.IwF;
param.h = 0.02794; % roughly measured


% state
Vx = z(1);
Vy = z(2);
dpsi = z(3);
omega = z(4);

% action
delta = -f(t)*param.MaxWheelAngle;
throttle = 0.5;

% Calculate component velocities
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2+Vy.^2);

V_Fx = V.*cos(beta - delta) + dpsi.*param.lF.*sin(delta);
V_Fy = V.*sin(beta - delta) + dpsi.*param.lF.*cos(delta);
V_Rx = V.*cos(beta);
V_Ry = V.*sin(beta) - dpsi*param.lR;

% Calculate friction coefficients
s_Fx = xSlip(V_Fx,omega,param.rF);
s_Fy = ySlip(V_Fy,omega,param.rF);
s_F = sqrt(s_Fx.^2+s_Fy.^2);

s_Rx = xSlip(V_Rx,omega,param.rR);
s_Ry = ySlip(V_Ry,omega,param.rR);
s_R = sqrt(s_Rx.^2+s_Ry.^2);

mu_Fx = -s_Fx./s_F.*MF(param.B, param.C, param.D, s_F);
mu_Fy = -s_Fy./s_F.*MF(param.B, param.C, param.D, s_F);
mu_Fx(isinf(mu_Fx))=0;
mu_Fx(isnan(mu_Fx))=0;
mu_Fy(isinf(mu_Fy))=0;
mu_Fy(isnan(mu_Fy))=0;

mu_Rx = -s_Rx./s_R.*MF(param.B, param.C, param.D, s_R);
mu_Ry = -s_Ry./s_R.*MF(param.B, param.C, param.D, s_R);
mu_Rx(isinf(mu_Rx))=0;
mu_Rx(isnan(mu_Rx))=0;
mu_Ry(isinf(mu_Ry))=0;
mu_Ry(isnan(mu_Ry))=0;


% Calculate friction forces
GRAVITY = 9.81;
num = param.lR*param.m*GRAVITY - param.h*param.m*GRAVITY*mu_Rx;
den = param.h*(mu_Fx.*cos(delta) - mu_Fy.*sin(delta) - mu_Rx);
f_Fz = num./(param.lF + param.lR + den);
f_Rz = param.m*GRAVITY - f_Fz;
f_Fx = mu_Fx.*f_Fz;
f_Fy = mu_Fy.*f_Fz;
f_Rx = mu_Rx.*f_Rz;
f_Ry = mu_Ry.*f_Rz;

% System dynamics
Vxdot = 1/param.m*(param.m*Vy.*dpsi + f_Fx.*cos(delta) - f_Fy.*sin(delta) + f_Rx); % body x velocity
Vydot = 1/param.m*(-param.m*Vx.*dpsi + f_Fx.*sin(delta) + f_Fy.*cos(delta) + f_Ry); % body y velocity
dpsidot = 1/param.Iz*((f_Fy.*cos(delta) + f_Fx.*sin(delta))*param.lF- f_Ry.*param.lR); % yaw rate
wdot = wheelspeed(throttle, omega, (f_Fx+f_Rx)/2);

dz = zeros(4,1);
dz(1) = Vxdot;
dz(2) = Vydot;
dz(3) = dpsidot;
dz(4) = wdot;



function slip = xSlip( Vx, omega, r)
if (omega*r ~= 0.0)
% if (abs(omega*r) > 0.001)
	slip = (Vx - omega*r)/abs(omega*r);
else
    slip = 0.0;
end

function slip = ySlip( Vy, omega, r)
if (omega*r ~= 0.0)
% if (abs(omega*r) > 0.001)
    slip = Vy/abs(omega*r);
else
    slip = 0.0;
end

function ret = MF( B, C, D, s)
ret = D*sin(C*atan(B*s));