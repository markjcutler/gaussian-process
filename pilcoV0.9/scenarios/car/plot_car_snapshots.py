#!/usr/bin/env python

###################################################
# plot_car_snapshots.py -- Plot snapshots of toy car
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday, 19 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from scipy import ndimage
import sys
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)



def load_gpops(path):
    data = scipy.io.loadmat(path)
    time = np.array(data['Time'])
    state = np.array(data['State'])
    delta = np.array(data['Force'])

    time = time[0]
    delta = delta[0]

    Vx = state[0, :]
    Vy = state[1, :]
    dpsi = state[2, :]
    omega = state[3, :]

    return time, Vx, Vy, dpsi, omega, delta


def load_pilco(path):
    data = scipy.io.loadmat(path)
    T = data['T']
    dt = data['dt']
    time = np.arange(0, T+dt, dt)

    xx = np.array(data['xx'])
    yy = np.array(data['yy'])

    Vx = xx[:, 0]
    Vy = xx[:, 1]
    dpsi = xx[:, 2]
    omega = xx[:, 3]
    delta = xx[:, 4]

    # add the last value of yy to xx
    Vx = np.append(Vx, yy[-1, 0])
    Vy = np.append(Vy, yy[-1, 1])
    dpsi = np.append(dpsi, yy[-1, 2])
    omega = np.append(omega, yy[-1, 3])
    delta = np.append(delta, delta[-1])

    return time, Vx, Vy, dpsi, omega, delta


def load_rollout(path):
    data = scipy.io.loadmat(path)
    xx = np.array(data['xx'])
    yy = np.array(data['yy'])

    Vx = xx[:, 0]
    Vy = xx[:, 1]
    dpsi = xx[:, 2]
    delta = xx[:, 3]

    # add the last value of yy to xx
    Vx = np.append(Vx, yy[-1, 0])
    Vy = np.append(Vy, yy[-1, 1])
    dpsi = np.append(dpsi, yy[-1, 2])
    delta = np.append(delta, delta[-1])

    dt = 0.1
    T = len(Vx)*dt
    time = np.arange(0, T, dt)

    return time, Vx, Vy, dpsi, np.zeros(len(Vx)), delta


def load_rollouts(path, runs, mid, files):
    vstack = False
    for r in runs:
        for f in files:
            rollout = path + r + mid + f
            print rollout
            if not vstack:
                T, Vx, Vy, Dpsi, Omega, Delta = load_rollout(rollout)
                vstack = True
            else:
                t, vx, vy, dpsi, omega, delta = load_rollout(rollout)
                T = np.vstack((T, t))
                Vx = np.vstack((Vx, vx))
                Vy = np.vstack((Vy, vy))
                Dpsi = np.vstack((Dpsi, dpsi))
                Omega = np.vstack((Omega, omega))
                Delta = np.vstack((Delta, delta))

    t_mean = np.mean(T, axis=0)
    t_std = np.std(T, axis=0)
    Vx_mean = np.mean(Vx, axis=0)
    Vx_std = np.std(Vx, axis=0)
    Vy_mean = np.mean(Vy, axis=0)
    Vy_std = np.std(Vy, axis=0)
    Dpsi_mean = np.mean(Dpsi, axis=0)
    Dpsi_std = np.std(Dpsi, axis=0)
    Omega_mean = np.mean(Omega, axis=0)
    Omega_std = np.std(Omega, axis=0)
    Delta_mean = np.mean(Delta, axis=0)
    Delta_std = np.std(Delta, axis=0)

    return t_mean, t_std, Vx_mean, Vx_std, Vy_mean, Vy_std, Dpsi_mean, Dpsi_std, Omega_mean, Omega_std, Delta_mean, Delta_std


def plot_trajectory(time, Vx, Vy, dpsi, omega, delta, x0):

    n = len(time)
    x = np.zeros(n)
    y = np.zeros(n)
    psi = np.zeros(n)

    x[0] = x0

    for i in range(1, n):
        dt = time[i] - time[i-1]
        psi[i] = psi[i-1] + dpsi[i]*dt
        dx = Vx[i]*np.cos(psi[i]) - Vy[i]*np.sin(psi[i])
        dy = Vx[i]*np.sin(psi[i]) + Vy[i]*np.cos(psi[i])
        x[i] = x[i-1] + dx*dt
        y[i] = y[i-1] + dy*dt

    return x, y, psi


def add_car(x, y, psi, ax, fig, image):
    rotate_car = ndimage.rotate(image, psi*180.0/np.pi)

    dat_coord = [(x, y)]
    # transform the two points from data coordinates to display coordinates:
    tr1 = ax.transData.transform(dat_coord)
    # create an inverse transversion from display to figure coordinates:
    inv = fig.transFigure.inverted()
    tr2 = inv.transform(tr1)

    scale_val = 0.035*(np.cos(np.pi + 4.0*psi)/2.0 + 0.5)

    datco = [tr2[0, 0]-0.05-scale_val/2.0,
             tr2[0, 1]-0.05-scale_val/2.0,
             0.1 + scale_val, 0.1 + scale_val]
    axins = fig.add_axes(datco)
    axins.set_axis_off()
    axins.imshow(rotate_car, alpha=1.0)
    print 'adding car'


def plot_cars(t, dt, x, y, psi, ax, fig, image, limits):
    t_car = np.arange(0, t[-1], dt)
    current_t = 0
    for i in range(len(x)):
        try:
            if (t[i] >= t_car[current_t] and x[i] >= limits[0] and
                x[i] <= limits[1] and y[i] >= limits[2] and y[i] <= limits[3]):
                add_car(x[i], y[i], psi[i], ax, fig, image)
                current_t += 1
        except:
            pass



path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/car/'
path_gpops = path_main + 'training_car.mat'

# ****************
# gpops to pilco
# ****************
# path_pilco0 = path_main + 'car_from_gpops/car_0_H40.mat'
# path_pilco4 = path_main + 'car_from_gpops/car_4_H40.mat'
# offset1 = 2.5  # 1.0
# offset2 = 2.5  # 4.0
# legend = ['Optimal Control (Sim)', 'Learned Trajectory (Sim)']
# label_offset = 0.5

# ****************
# gpops to pilco
# ****************
path_pilco0 = path_main + 'R0p8_gpops2sim/run1/rollout_11.mat'
path_pilco4 = path_main + 'R0p8_gpops2sim/run1/rollout_51.mat'
offset1 = 1.0
offset2 = 4.0
legend = ['Optimal Control (Sim)', 'Learned Trajectory (Sim)']
label_offset = 0.5
x_center = 0.52
y_center = -0.5
markersize = 50
plotmarker = True

path_rollouts = path_main + 'R0p8_gpops2sim/run'
rollouts = ['1.mat', '2.mat', '3.mat', '4.mat', '5.mat']
runs = ['1', '2', '3', '4', '5']
rollout0 = '/rollout_1'
rollout4 = '/rollout_5'
color = [sim_light, sim_dark, '#84DEBD']
image1 = plt.imread(path_main + 'car_square_light_sim.png')
image2 = plt.imread(path_main + 'car_square_dark_sim.png')


# ****************
# pilco to real world
# ****************
# path_pilco0 = path_main + 'R0p8_sim2rw/run1/rollout_11.mat'
# path_pilco4 = path_main + 'R0p8_sim2rw/run3/rollout_45.mat'
# offset1 = 1.0
# offset2 = 4.0
# legend = ['Prior (Real Car)', 'Learned Trajectory (Real Car)']
# label_offset = 0.5
# x_center = 1.01
# y_center = 0.15
# markersize = 63
# plotmarker = True

# path_rollouts = path_main + 'R0p8_sim2rw/run'
# rollouts = ['1.mat', '2.mat', '3.mat', '4.mat', '5.mat']
# runs = ['1', '2', '3']
# rollout0 = '/rollout_1'
# rollout4 = '/rollout_5'
# color = [rw_light, rw_dark, '#84DEBD']
# image1 = plt.imread(path_main + 'car_square_light_rw.png')
# image2 = plt.imread(path_main + 'car_square_dark_rw.png')



# ****************
# no gpops vs pilco w/ gpops
# ****************
# path_pilco0 = path_main + 'car_wout_gpops/car_7_H40.mat'
# path_pilco4 = path_main + 'car_from_gpops/car_4_H40.mat'
# offset1 = 2.5
# offset2 = 1.5
# legend = ['No Prior (Sim)', 'Optimal Control Prior (Sim)']
# label_offset = 0.73

legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
linestyle = ['-', '-']

xmin = -1.0
xmax = 5.0
ymin = -1
ymax = 4



fig, ax = plt.subplots()
# ax.set_aspect(1)

# t, vx, vy, dpsi, omega, delta = load_pilco(path_pilco0)
t, vx, vy, dpsi, omega, delta = load_rollout(path_pilco0)
x, y, psi = plot_trajectory(t, vx, vy, dpsi, omega, delta, offset1)

# t2, vx2, vy2, dpsi2, omega2, delta2 = load_pilco(path_pilco4)
t2, vx2, vy2, dpsi2, omega2, delta2 = load_rollout(path_pilco4)
x2, y2, psi2 = plot_trajectory(t2, vx2, vy2, dpsi2, omega2, delta2, offset2)

ax.plot(x, y, color=color[0], linestyle=linestyle[0], linewidth=4, label=legend[0])
ax.plot(x2, y2, color=color[1], linestyle=linestyle[1], linewidth=4, label=legend[1])

tm1, ts1, vxm1, vxs1, vym1, vys1, dpsim1, dpsis1, om1, os1, dm1, ds1 = load_rollouts(path_rollouts, runs, rollout0, rollouts)

tm2, ts2, vxm2, vxs2, vym2, vys2, dpsim2, dpsis2, om2, os2, dm2, ds2 = load_rollouts(path_rollouts, runs, rollout4, rollouts)

ax.set_xlabel(r'$x$ $(m)$', size=axis_label_fontsize)
ax.set_ylabel(r'$y$ $(m)$', size=axis_label_fontsize)
ax.legend(fancybox=True,
          fontsize=legend_fontsize,
          loc='best')
ax.annotate(r'Start', xy=(offset2-0.25, -0.6),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="1.0", ec=axes_ticks_color),
            fontsize=legend_fontsize)
ax.annotate(r'Start', xy=(offset1-0.25, -0.6),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="1.0", ec=axes_ticks_color),
            fontsize=legend_fontsize)

ax.set_xlim([xmin, xmax])
ax.set_ylim([ymin, ymax])

cur_aspect = ax.get_aspect()
print cur_aspect

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

limits = np.array([xmin, xmax, ymin, ymax])

# plot_cars(t, 0.4, x, y, psi, ax, fig, image1, limits)
# plot_cars(t2, 0.4, x2, y2, psi2, ax, fig, image2, limits)

plt.savefig(path_main + 'car_traj.pdf',
            format='pdf', transparent=True, bbox_inches='tight',
            dpi=1000)

fig, axarr = plt.subplots(4, sharex=True)

Vx_ss = 0.3892
Vy_ss = -1.3149
dpsi_ss = 1.3713
delta_ss = -0.2533

alpha = 0.55
# axarr[0].plot(t, vxm1, color[0], linewidth=3)
# axarr[0].fill_between(t, vxm1+vxs1, vxm1-vxs1, alpha=alpha, facecolor=color[0], edgecolor='')
# axarr[0].plot(t, vym1, color[0], linewidth=3)
# axarr[0].fill_between(t, vym1+vxs1, vym1-vxs1, alpha=alpha, facecolor=color[0], edgecolor='')
# axarr[0].plot(t2, vxm2, color[1], linewidth=3)
# axarr[0].fill_between(t, vxm2+vxs2, vxm2-vxs2, alpha=alpha, facecolor=color[1], edgecolor='')
# axarr[0].plot(t2, vym2, color[1], linewidth=3)
# axarr[0].fill_between(t, vym2+vys2, vym2-vys2, alpha=alpha, facecolor=color[1], edgecolor='')
# axarr[0].plot([t[0], t[-1]], [Vx_ss, Vx_ss], 'k:', linewidth=1)
# axarr[0].plot([t[0], t[-1]], [Vy_ss, Vy_ss], 'k:', linewidth=1)
# axarr[0].set_ylabel(r'$V$ $(m/s)$', size=axis_label_fontsize)
# axarr[0].annotate(r'$V_x$', xy=(3.0, Vx_ss+label_offset),  xycoords='data',
#                   xytext=(0, 0), textcoords='offset points',
#                   bbox=dict(boxstyle="round", fc="1.0"),
#                   fontsize=legend_fontsize)
# axarr[0].annotate('$V_y$', xy=(1.5, Vy_ss+label_offset),  xycoords='data',
#                   xytext=(0, 0), textcoords='offset points',
#                   bbox=dict(boxstyle="round", fc="1.0"),
#                   fontsize=legend_fontsize)

axarr[0].plot(t, vxm1, color[0], linewidth=3, label=legend[0])
axarr[0].fill_between(t, vxm1+vxs1, vxm1-vxs1, alpha=alpha, facecolor=color[0], edgecolor='')
axarr[0].plot(t2, vxm2, color[1], linewidth=3, label=legend[1])
axarr[0].fill_between(t, vxm2+vxs2, vxm2-vxs2, alpha=alpha, facecolor=color[1], edgecolor='')
axarr[0].plot([t[0], t[-1]], [Vx_ss, Vx_ss], 'k:', linewidth=1)
axarr[0].set_ylabel(r'$V_x$ $(\frac{m}{s})$', size=axis_label_fontsize)

axarr[0].legend(fancybox=True,
                fontsize=legend_fontsize,
                bbox_to_anchor=(0.95, 1.02),
                bbox_transform=plt.gcf().transFigure)

axarr[1].plot(t, vym1, color[0], linewidth=3)
axarr[1].fill_between(t, vym1+vxs1, vym1-vxs1, alpha=alpha, facecolor=color[0], edgecolor='')
axarr[1].plot(t2, vym2, color[1], linewidth=3)
axarr[1].fill_between(t, vym2+vys2, vym2-vys2, alpha=alpha, facecolor=color[1], edgecolor='')
axarr[1].plot([t[0], t[-1]], [Vy_ss, Vy_ss], 'k:', linewidth=1)
axarr[1].set_ylabel(r'$V_y$ $(\frac{m}{s})$', size=axis_label_fontsize)


axarr[2].plot(t, dpsim1, color[0], linewidth=3)
axarr[2].fill_between(t, dpsim1+dpsis1, dpsim1-dpsis1, alpha=alpha, facecolor=color[0], edgecolor='')
axarr[2].plot(t2, dpsim2, color[1], linewidth=3, label=legend[1])
axarr[2].fill_between(t, dpsim2+dpsis2, dpsim2-dpsis2, alpha=alpha, facecolor=color[1], edgecolor='')
axarr[2].plot([t[0], t[-1]], [dpsi_ss, dpsi_ss], 'k:', linewidth=1)
axarr[2].set_ylabel(r'$\dot{\psi}$ $(\frac{rad}{s})$', size=axis_label_fontsize)

axarr[3].plot(t, dm1, color[0], linewidth=3)
axarr[3].fill_between(t, dm1+ds1, dm1-ds1, alpha=alpha, facecolor=color[0], edgecolor='')
axarr[3].plot(t2, dm2, color[1], linewidth=3)
axarr[3].fill_between(t, dm2+ds2, dm2-ds2, alpha=alpha, facecolor=color[1], edgecolor='')
axarr[3].plot([t[0], t[-1]], [delta_ss, delta_ss], 'k:', linewidth=1)
#axarr[3].set_ylim([-1.2, 1.2])
axarr[3].set_ylabel(r'$\delta$', size=axis_label_fontsize)
axarr[3].set_xlabel(r'$t$ $(s)$', size=axis_label_fontsize)

# plot circle
if plotmarker:
    axarr[3].plot(x_center, y_center, 'o', markerfacecolor='none',
                  markeredgecolor='r', markersize=markersize,
                  markeredgewidth=5)

for i in range(4):
    ax = axarr[i]
    ax.get_yaxis().set_label_coords(-0.07, 0.6)

    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(axis_tick_fontsize)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(axis_tick_fontsize)

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    # Find at most 101 ticks on the y-axis at 'nice' locations
    max_yticks = 4
    yloc = plt.MaxNLocator(max_yticks)
    ax.yaxis.set_major_locator(yloc)

axarr[3].set_yticks([-1, 0, 1])


plt.savefig(path_main + 'car_data.pdf',
            format='pdf', transparent=True, bbox_inches='tight',
            dpi=1000)
