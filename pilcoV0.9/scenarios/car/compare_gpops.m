% simple script for collecting data on the car

% first, get data using no gpops
for i=1:20
    car_learn_fun(0, 0, 0, 7, i, 0);
end

% second, use gpops
for i=1:20
    car_learn_fun(0, 0, 'gpops_gp_policy.mat', 7, i+20, 0);
end
