%% applyController.m
% *Summary:* Script to apply the learned controller to a (simulated) system
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-06-04
%
%% High-Level Steps
% # Generate a single trajectory rollout by applying the controller
% # Generate many rollouts for testing the performance of the controller
% # Save the data

%% Code

% 1. Generate trajectory rollout given the current policy
if isfield(plant,'constraint'), HH = maxH; else HH = H; end

% save w, b, H, rate
save_gp_params(policy, nc, length(poli), H, dt, is_random_policy, 0);
% w = policy.p.w;
% b = policy.p.b;
% random = false;
% save('params','w','b','H','dt','random');
% clear w
% clear b
% clear random

pause

% load data
load('rollout');
D = length(dyno);
% %% no omega
% xx = 

% for iii=1:H
%     xx(iii,1:D) = xx(iii,1:D) + randn(1,D)*chol(plant.noise);
%     if iii>1
%         yy(iii-1,:) = xx(iii,1:D);
%     end
% end
% yy(end,:) = yy(end,:) + randn(1,D)*chol(plant.noise);

% pull out every 5th x
% xxx = zeros(40,5); yyy = zeros(40,2);
% cntxxx = 1;
% for i=1:length(xx)
%     if mod(i,5) == 1
%         xxx(cntxxx, :) = xx(i,:);
%         yyy(cntxxx, :) = yy(i,:);
%         cntxxx = cntxxx + 1;
%     end
% end
% x = [x; xxx]; y = [y; yyy];




% %% DEBUG
% xx(2,4) = xx(3,4)/2;
% yy(1,4) = xx(2,4);
% %%



x = [x; xx]; y = [y; yy];                            % augment training set

if exist('j','var')
    latent1 = zeros(size(xx,1)+1, size(xx,2));
    latent1(1:end-1,:) = xx;
    latent1(end,1:end-1) = yy(end, :);
    latent{j} = latent1;

    if plotting.verbosity > 0
      if ~ishandle(3); figure(3); else set(0,'CurrentFigure',3); end
      hold on; plot(1:length(realCost{J+j}),realCost{J+j},'r'); drawnow;
    end

    % 3. Save data
    filename = [basename num2str(j) '_H' num2str(H)]; save(filename);
end