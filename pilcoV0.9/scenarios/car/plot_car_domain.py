#!/usr/bin/env python

###################################################
# plot_car_domain.py -- plot car drifting domain
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Tuesday, 14 July 2015.
###################################################

import matplotlib.pyplot as plt
from scipy import ndimage
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color='k')


def add_car(x, y, psi, ax, fig, image):
    rotate_car = ndimage.rotate(image, psi*180.0/np.pi)

    dat_coord = [(x, y)]
    # transform the two points from data coordinates to display coordinates:
    tr1 = ax.transData.transform(dat_coord)
    # create an inverse transversion from display to figure coordinates:
    inv = fig.transFigure.inverted()
    tr2 = inv.transform(tr1)

    scale_val = 0.035*(np.cos(np.pi + 4.0*psi)/2.0 + 6.5)

    datco = [tr2[0, 0]-0.05-scale_val/2.0,
             tr2[0, 1]-0.05-scale_val/2.0,
             0.1 + scale_val, 0.1 + scale_val]
    axins = fig.add_axes(datco)
    axins.set_axis_off()
    axins.imshow(rotate_car, alpha=0.45)
    print 'adding car'
    return axins


def plot_cars(t, x, y, psi, ax, fig, image, limits):
    t_car = np.linspace(0, t[-1], 10)
    current_t = 0
    for i in range(len(x)):
        if (t[i] >= t_car[current_t] and x[i] >= limits[0] and
            x[i] <= limits[1] and y[i] >= limits[2] and y[i] <= limits[3]):
            add_car(x[i], y[i], psi[i], ax, fig, image)
            current_t += 1


def plot_arrow(x0, y0, length, theta, ax):
    end_x = np.cos(theta)*length + x0
    end_y = np.sin(theta)*length + y0
    ax.annotate("", xy=(end_x, end_y), xycoords='data',
                xytext=(x0, y0), textcoords='data',
                arrowprops=dict(width=2.0, headwidth=8,
                                fc='k', ec='k'), zorder=1)
    ax.set_zorder(0)

des_traj = True

path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/car/'

legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
color = ['#D1B9D4', '#D1D171', '#84DEBD']
linestyle = ['--', '-']

xoffset = 0.0
xmax = 1.45 + xoffset
ymax = 1.15
ymin = -ymax
xmin = -xmax + xoffset

theta = np.linspace(-np.pi, np.pi, 100)
R = 0.8
x = R*np.cos(theta)
y = R*np.sin(theta)


fig, ax = plt.subplots()
#ax.set_aspect('equal')

beta = 60*np.pi/180.0
theta0 = -150*np.pi/180.0
theta0_cheat = 0*5*np.pi/180.0
offset_cheat = 0*0.1  # the axis equal scaling messes with the car placement
x0car = (R - offset_cheat)*np.cos(theta0 + theta0_cheat)
y0car = (R - offset_cheat)*np.sin(theta0 + theta0_cheat)
x0 = R*np.cos(theta0)
y0 = R*np.sin(theta0)
psi0 = theta0-np.pi/2.0-beta
print theta0
print beta*180/np.pi

ax.plot(x, y, color=axes_ticks_color, linestyle=linestyle[0],
        linewidth=2.5, label='Desired Trajectory')
ax.set_xlabel(r'$x$ $(m)$', size=axis_label_fontsize)
ax.set_ylabel(r'$y$ $(m)$', size=axis_label_fontsize)
# ax.legend(fancybox=True,
#           fontsize=legend_fontsize,
#           loc='best')
if des_traj:
    ax.annotate('Desired\nTrajectory', xy=(0.6, 0.6),  xycoords='data',
                xytext=(0, 0), textcoords='offset points',
                fontsize=legend_fontsize, color=axes_ticks_color)
# ax.annotate(r'Start', xy=(offset1-0.25, -0.6),  xycoords='data',
#             xytext=(0, 0), textcoords='offset points',
#             bbox=dict(boxstyle="round", fc="1.0"),
#             fontsize=legend_fontsize)

ax.set_xlim([xmin, xmax])
ax.set_ylim([ymin, ymax])

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

limits = np.array([xmin, xmax, ymin, ymax])

image = plt.imread(path_main + 'car_square_green.png')
ax2 = add_car(x0car, y0car, psi0, ax, fig, image)

Vlen = 0.8
# V
Vang = theta0 - np.pi/2.0
plot_arrow(x0, y0, Vlen, Vang, ax)
# Vy
Vyang = psi0 - np.pi/2
Vylen = -Vlen*np.sin(beta)
plot_arrow(x0, y0, Vylen, Vyang, ax)
# Vx
Vxang = psi0
Vxlen = Vlen*np.cos(beta)
plot_arrow(x0, y0, Vxlen, Vxang, ax)
# r
if des_traj:
    plot_arrow(0, 0, R-0.02, -30.0*np.pi/180.0, ax)

text_fontsize = 30
if des_traj:
    ax.text(0.35, -0.15, r'$R$', fontsize=text_fontsize)
ax.text(x0+Vlen*np.cos(Vang), y0+Vlen*np.sin(Vang)+0.03, r'$V$', fontsize=text_fontsize)
ax.text(x0+Vxlen*np.cos(Vxang)-0.04, y0+Vxlen*np.sin(Vxang)+0.05,
        r'$V_x$', fontsize=text_fontsize)
ax.text(x0+Vylen*np.cos(Vyang), y0+Vylen*np.sin(Vyang)+0.05, r'$V_y$', fontsize=text_fontsize)
ax.text(x0+0.18, y0-0.12, r'$\dot{\psi}$', fontsize=text_fontsize)
ax.text(x0-0.04, y0+0.32, r'$\beta$', fontsize=text_fontsize)

betax0 = x0 + Vlen/3.*np.cos(Vxang)
betay0 = y0 + Vlen/3.*np.sin(Vxang)
betax1 = x0 + Vlen/3.*np.cos(Vang)
betay1 = y0 + Vlen/3.*np.sin(Vang)

ax.text(x0-0.17, y0-0.01, r'$\circlearrowleft$', fontsize=40, rotation='-120')
connectionstyle = "arc3,rad=0.3"
ax.annotate("",
            xy=(betax1, betay1), xycoords='data',
            xytext=(betax0, betay0), textcoords='data',
            arrowprops=dict(arrowstyle="->", #linestyle="dashed",
                            color="0",
                            shrinkA=5, shrinkB=5,
                            patchA=None,
                            patchB=None,
                            connectionstyle=connectionstyle,
                            ),
            )

ax.set_zorder(ax2.get_zorder()+1)
ax.patch.set_visible(False)

# image = plt.imread(path_main + 'car_square_green.png')
# plot_cars(t2, x2, y2, psi2, ax, fig, image, limits)

plt.savefig(path_main + 'car_domain.pdf',
            format='pdf', transparent=True, bbox_inches='tight',
            dpi=1000)
