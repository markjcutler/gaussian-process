#!/bin/bash
process=$1
N=5
set_random=0

# nomial performance
use_prior=0
base_name="nom_"
use_prior_policy=0
for i in $(seq 1 $N); do
    eval "matlab -nodesktop -nosplash -r cartDouble_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done

# just prior transistion data
use_prior=1
base_name="prior_trans_"
use_prior_policy=0
for i in $(seq 1 $N); do
    eval "matlab -nodesktop -nosplash -r cartDouble_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done

# full prior
use_prior=1
base_name="prior_full_"
use_prior_policy=1
for i in $(seq 1 $N); do
    eval "matlab -nodesktop -nosplash -r cartDouble_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done

# just prior policy
use_prior=0
base_name="prior_policy_"
use_prior_policy=1
for i in $(seq 1 $N); do
    eval "matlab -nodesktop -nosplash -r cartDouble_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done
