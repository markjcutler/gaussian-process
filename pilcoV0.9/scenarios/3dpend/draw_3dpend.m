%% draw_pendubot.m
% *Summary:* Draw the Pendubot system with reward, applied torque, 
% and predictive uncertainty of the tips of the pendulums
%
%    function draw_pendubot(theta1, theta2, force, cost, text1, text2, M, S)
%
%
% *Input arguments:*
%
%   theta1     angle of inner pendulum
%   theta2     angle of outer pendulum
%   f1         torque applied to inner pendulum
%   f2         torque applied to outer pendulum
%   cost       cost structure
%     .fcn     function handle (it is assumed to use saturating cost)
%     .<>      other fields that are passed to cost
%   text1      (optional) text field 1
%   text2      (optional) text field 2
%   M          (optional) mean of state
%   S          (optional) covariance of state
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-08


function draw_3dpend(gx, gy, gz, f_phi, f_theta, cost, text1, text2, M, S)
%% Code

l = 1;
xmax = 1.5*l;
xmin = -1.5*l;

% Draw double pendulum
clf; hold on

% sp = sin(phi); st = sin(theta);
% cp = cos(phi); ct = cos(theta);

% R_phi = [1 0 0;
%          0 cp -sp;
%          0 sp cp];
% R_theta = [ct 0 st;
%            0 1 0;
%            -st 0 ct];
%        
% R = R_theta*R_phi;

R = zeros(3);
R(3,1) = -gx;
R(3,2) = -gy;
R(3,3) = gz;
         

pendulum = [0, 0, 0; 0, 0, l];
pendulum = pendulum*R; %(R*pendulum')';

plot3(pendulum(:,1), pendulum(:,2), pendulum(:,3),'r','linewidth',4)

% plot target location
plot3(0,0,l,'k+','MarkerSize',20);
% plot([xmin, xmax], [-height, -height],'k','linewidth',2)
% plot inner joint
plot3(0,0,0,'k.','markersize',24)
plot3(0,0,0,'y.','markersize',14)
% plot tip of outer joint
plot3(pendulum(2,1), pendulum(2,2), pendulum(2,3),'k.','markersize',24)
plot3(pendulum(2,1), pendulum(2,2), pendulum(2,3),'y.','markersize',14)
% plot(0,-2*l,'.w','markersize',0.005)

% plot ellipses around tips of pendulums (if M, S exist)
% try
%   if max(max(S))>0
%     [M1 S1 M2 S2] = getPlotDistr_pendubot(M, S, l, l);
%     error_ellipse(S1, M1, 'style','b'); % inner pendulum
%     error_ellipse(S2, M2, 'style','r'); % outer pendulum
%   end
% catch
% end

% Draw useful information
% plot applied torque
% plot([0 force/umax*xmax],[-0.5, -0.5],'g','linewidth',10)
% % plot immediate reward
% reward = 1-cost.fcn(cost,[0, 0, theta1, theta2]',zeros(4));
% plot([0 reward*xmax],[-0.7, -0.7],'y','linewidth',10)
% text(0,-0.5,'applied  torque (inner joint)')
% text(0,-0.7,'immediate reward')
% if exist('text1','var')  
%   text(0,-0.9, text1)
% end
% if exist('text2','var')
%   text(0,-1.1, text2)
% end

set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[xmin xmax],'ZLim',[xmin xmax]);
% axis off
view([-90 0])
% view([-180 0])
% view([-90 90])
% view([-66 42])
xlabel('x')
ylabel('y')
zlabel('z')
drawnow;