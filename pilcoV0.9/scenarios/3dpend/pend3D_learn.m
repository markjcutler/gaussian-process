%% pendubot_learn.m
% *Summary:* Script to learn a controller for the pendubot
% swingup (a double pendulum with the inner joint actuated)
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
% rng(1651)
rng('shuffle')

use_prior = 0;
use_hw = 0;

plot_dyn_model = false;
extra_plots = false;

settings_3dpend;            % load scenario-specific settings
basename = '3dpend_';       % filename used for saving data
% return

%% load and use prior information
if use_prior
    prior_info = load('sim_prior.mat');
    
    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
end

%% load and use hardware information
if use_hw
    hw_info = load('hw2_bx0p3_by0p2.mat');
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
    
end

% 2. Initial J random rollouts
for jj = 1:J
    if use_prior %isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_3dpend;
    end
end

% x
% return

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
    trainDynModel;   % train (GP) dynamics model

    %%
    if plot_dyn_model
        % plot dynamics model
        figure(5);clf;
        fy_prime = (-policy.maxU(1):0.1:policy.maxU(1))';
        mean = zeros(size(fy_prime));
        var = zeros(size(fy_prime));
        inoutvar = zeros(size(fy_prime));
        for kk=1:length(fy_prime)
            m = [0 0 0 0 0 0 fy_prime(kk)]';
            [mean_all, var_all, inoutvar_all] = gp0(dynmodel, m, zeros(D));
            mean(kk) = mean_all(1); % delta_pdot
            var(kk) = var_all(1,1);
            inoutvar(kk) = inoutvar_all(1,1);
            %       var(kk)
        end
        if isfield(dynmodel,'prior')
            for i=1:length(fy_prime)
                prior_mean = dynmodel.prior.fcn(dynmodel.prior, fy_prime(i), zeros(D));
                mean(i) = mean(i) - prior_mean;
            end
        end


        % get usuable versions of hyperparameters
        log_length_scales = dynmodel.hyp(1:D,:);
        log_signal_sigma = dynmodel.hyp(D+1,:);
        signal_var = exp(2*log_signal_sigma);
        noise_var = exp(2*dynmodel.hyp(D+2,:));

        % get usable version of hyperparameters for the prior
        if isfield(dynmodel,'prior')
            [p, ~] = size(dynmodel.prior.targets);
            log_length_scales_prior = dynmodel.prior.hyp(1:D,:);
            log_signal_sigma_prior = dynmodel.prior.hyp(D+1,:);
            length_scales_2_prior = exp(2*log_length_scales_prior);
            signal_var_prior = exp(2*log_signal_sigma_prior);
            noise_var_prior = exp(2*dynmodel.prior.hyp(D+2,:));
        end


        f = [mean+2*sqrt(var + noise_var(1)); flipdim(mean-2*sqrt(var + noise_var(1)),1)];
        fill([fy_prime; flipdim(fy_prime,1)], f, [7 7 7]/8)
        hold on; plot(fy_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs(:,7), dynmodel.targets(:,1), '+', 'MarkerSize', 12)
        grid on
        xlabel('input, x')
        ylabel('output, y')
        drawnow;
        %     axis([-1.9 1.9 -0.9 3.9])
    %     return;
    end    

    if extra_plots
        
        % plot dynamics model
        figure(6);clf;
        
        fy_prime = (-policy.maxU(1):0.1:policy.maxU(1))';
        mean = zeros(size(fy_prime));
        var = zeros(size(fy_prime));
        inoutvar = zeros(size(fy_prime));
        for kk=1:length(fy_prime)
            m = [0 0 0 0 0 0 fy_prime(kk)]';
            [mean_all, var_all, inoutvar_all] = gp0(dynmodel.gpmodel_rw, m, zeros(D));
            mean(kk) = mean_all(1); % delta_pdot
            var(kk) = var_all(1,1);
            inoutvar(kk) = inoutvar_all(1,1);
            %       var(kk)
            
            [n, D] = size(dynmodel.gpmodel_rw.inputs);    % number of examples and dimension of inputs
            

            % get the variance at the mean
            p_rw(kk) = weight_fnc(dynmodel, m, zeros(D));
            
        end
        
        % get usuable versions of hyperparameters
        log_length_scales = dynmodel.gpmodel_rw.hyp(1:D,:);
        log_signal_sigma = dynmodel.gpmodel_rw.hyp(D+1,:);
        signal_var = exp(2*log_signal_sigma);
        signal_var = signal_var(1);
        noise_var = exp(2*dynmodel.gpmodel_rw.hyp(D+2,:));
        
        f = [mean+2*sqrt(var + noise_var(1)); flipdim(mean-2*sqrt(var + noise_var(1)),1)];
        fill([fy_prime; flipdim(fy_prime,1)], f, [7 7 7]/8)
        hold on; plot(fy_prime, mean, 'LineWidth', 2); plot(dynmodel.gpmodel_rw.inputs(:,7), dynmodel.gpmodel_rw.targets(:,1), '+', 'MarkerSize', 12)
        grid on
        xlabel('input, x')
        ylabel('output, y')
        drawnow;
        
        figure(7);clf
        plot(fy_prime, p_rw)
        hold all
        plot(fy_prime, var/signal_var)
        legend('p','var/signal var');
        axis([-inf, inf, -inf 1.1])
            return
        
        
        % plot dynamics model
        figure(8);clf;
        dynmodel = rmfield(dynmodel,'gpmodel_rw');
        
        for kk=1:length(fy_prime)
            m = [0 0 0 0 0 0 fy_prime(kk)]';
            [mean_all, var_all, inoutvar_all] = gp0(dynmodel, m, zeros(D));
            mean(kk) = mean_all(1); % delta_pdot
            var(kk) = var_all(1,1);
            inoutvar(kk) = inoutvar_all(1,1);
        end
        
                
        % get usuable versions of hyperparameters
        log_length_scales = dynmodel.hyp(1:D,:);
        log_signal_sigma = dynmodel.hyp(D+1,:);
        signal_var = exp(2*log_signal_sigma);
        noise_var = exp(2*dynmodel.hyp(D+2,:));
        
        f = [mean+2*sqrt(var + noise_var(1)); flipdim(mean-2*sqrt(var + noise_var(1)),1)];
        fill([fy_prime; flipdim(fy_prime,1)], f, [7 7 7]/8)
        hold on; plot(fy_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs(:,7), dynmodel.targets(:,1), '+', 'MarkerSize', 12)
        grid on
        xlabel('input, x')
        ylabel('output, y')
        drawnow;
        
        t = 0:0.001:0.1;
        [y, dy] = gen_logistic_function(t);
        
        figure(10); clf;
        plot(t,y)
        drawnow;
        %     hold all
        %     plot(t,dy)
        
        return;
    end
    
    learnPolicy;     % learn policy
    applyController; % apply controller to system
    disp(['controlled trial # ' num2str(j)]);
      if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_3dpend;
      end
end