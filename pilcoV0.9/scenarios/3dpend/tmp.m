phi = 0.2
theta = .9

L = 2 - 2*cos(theta)*cos(phi)

% 2. Define static penalty as distance from target setpoint
ell = 1;
D1 = 5;
Q = zeros(D1); 
Q(3,3) = ell^2; % ell^2*gamma_x^2
Q(4,4) = ell^2; % ell^2*gamma_y^2
Q(5,5) = ell^2; % ell^2*(gamma_z - 1)^2
j = [0 0 -sin(theta) cos(theta)*sin(phi) cos(theta)*cos(phi)];
j_t = [0 0 0 0 1];

L2 = (j-j_t)*Q*(j-j_t)'

L - L2