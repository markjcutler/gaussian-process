theta = 0.5;
phi = 0.5;
gx = -sin(theta);
gy = cos(theta)*sin(phi);
gz = cos(theta)*cos(phi);

draw_3dpend(gx, gy, gz,  0,0,0,0,0,0,0)