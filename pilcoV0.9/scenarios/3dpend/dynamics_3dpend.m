%% dynamics_pendubot.m
% *Summary:* Implements ths ODE for simulating the Pendubot 
% dynamics, where an input torque f can be applied to the inner link 
%
%    function dz = dynamics_pendubot(t,z,f)
%
%
% *Input arguments:*
%
%		t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f     (optional): torque f(t) applied to inner pendulum
%
% *Output arguments:*
%   
%   dz    if 3 input arguments:      state derivative wrt time
%         if only 2 input arguments: total mechanical energy
%
%   Note: It is assumed that the state variables are of the following order:
%         dtheta1:  [rad/s] angular velocity of inner pendulum
%         dtheta2:  [rad/s] angular velocity of outer pendulum
%         theta1:   [rad]   angle of inner pendulum
%         theta2:   [rad]   angle of outer pendulum
%
% A detailed derivation of the dynamics can be found in:
%
% M.P. Deisenroth: 
% Efficient Reinforcement Learning Using Gaussian Processes, Appendix C, 
% KIT Scientific Publishing, 2010.
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-08

function dz = dynamics_3dpend(t, z, f, dir)
%% Code

fx = f(t)*cos(dir(t));
fy = f(t)*sin(dir(t));

% fx = fx(t);
% fy = fy(t);

total_force = sqrt(fx^2 + fy^2);
if total_force > 3.6
    disply('too much total force!');
end

% keep the total force equal
% total_force = sqrt(fx^2 + fy^2);
% max_force = 3.6;
% force_ratio = total_force/max_force;
% if force_ratio > 1
%     fx = fx/force_ratio;
%     fy = fy/force_ratio;
% end

l = 1;    % [m]        length of pendulum
m = 1;    % [kg]       mass of pendulum
g = -9.81; % [m/s^2]    acceleration of gravity
bx = 0.0; %0.0; %0.5; %0.01; % [s*Nm/rad] friction coefficient
by = 0.25; %0.5; %0.1; %0.03;

p     = 1*z(1);
q     = 1*z(2);
gamma_x = 1*z(3);
gamma_y = 1*z(4);
gamma_z = 1*z(5);

% 2.5 is absolute value of max torque
% u(t)
torquex = fx;
torquey = fy;
inertia = m*l^2/3;
gravity_torque_x = m*g*l/2*gamma_x;
gravity_torque_y = m*g*l/2*gamma_y;


dgamma_x = -q*gamma_z;
dgamma_y = p*gamma_z;
dgamma_z = q*gamma_x - p*gamma_y;

drag_x_body = bx*dgamma_x;
drag_y_body = by*dgamma_y;


dp = ( torquex + drag_y_body - gravity_torque_y ) / inertia;
dq = ( torquey - drag_x_body + gravity_torque_x ) / inertia;



dz = zeros(4,1);
dz(1) = dp;
dz(2) = dq;
dz(3) = dgamma_x;
dz(4) = dgamma_y;
dz(5) = dgamma_z;