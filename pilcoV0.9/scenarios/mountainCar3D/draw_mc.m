%% draw_pendubot.m
% *Summary:* Draw the Pendubot system with reward, applied torque, 
% and predictive uncertainty of the tips of the pendulums
%
%    function draw_pendubot(theta1, theta2, force, cost, text1, text2, M, S)
%
%
% *Input arguments:*
%
%   theta1     angle of inner pendulum
%   theta2     angle of outer pendulum
%   f1         torque applied to inner pendulum
%   f2         torque applied to outer pendulum
%   cost       cost structure
%     .fcn     function handle (it is assumed to use saturating cost)
%     .<>      other fields that are passed to cost
%   text1      (optional) text field 1
%   text2      (optional) text field 2
%   M          (optional) mean of state
%   S          (optional) covariance of state
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-08


% function draw_mc(position, height, force, cost, text1, text2, M, S, params)
function draw_mc(position, force, cost, text1, text2, M, S, params)
%% Code

x = -1.5:0.05:1.5;
y = polyval(params.coeffs, x);
xmin = min(x); 
xmax = max(x);    
umax = 1;




% Draw car
clf; hold on
% plot(position, height,'k+','MarkerSize',20);
plot(position, polyval(params.coeffs, position),'k+','MarkerSize',20);

% Draw surface
plot(x, y);

% plot target location
% plot(0,2*l,'k+','MarkerSize',20);
% plot([xmin, xmax], [-height, -height],'k','linewidth',2)

% % Draw sample positions of the joints
% if nargin > 6
%   samples = gaussian(M,S+1e-8*eye(4),1000);
%   t1 = samples(3,:); t2 = samples(4,:);
%   plot(-l*sin(t1),l*cos(t1),'b.','markersize',2)
%   plot(-l*(sin(t1)-sin(t2)),l*(cos(t1)+cos(t2)),'r.','markersize',2)
% end

% plot ellipses around tips of pendulums (if M, S exist)
try
  if max(max(S))>0
    [M1 S1 M2 S2] = getPlotDistr_pendubot(M, S, l, l);
    error_ellipse(S1, M1, 'style','b'); % inner pendulum
    error_ellipse(S2, M2, 'style','r'); % outer pendulum
  end
catch
end

% Draw useful information
% plot applied torque
plot([0 force/umax*xmax],[-0.5, -0.5],'g','linewidth',10)
% plot immediate reward
reward = 1; %1-cost.fcn(cost,[height, 0, 0]',zeros(3));
plot([0 reward*xmax],[-0.7, -0.7],'y','linewidth',10)
text(0,-0.5,'applied  torque (inner joint)')
text(0,-0.7,'immediate reward')
if exist('text1','var')  
  text(0,-0.9, text1)
end
if exist('text2','var')
  text(0,-1.1, text2)
end

set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[-.1 0.5]);
axis off
drawnow;