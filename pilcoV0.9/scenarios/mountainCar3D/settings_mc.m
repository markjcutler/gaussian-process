%% dynamics_mc.m
% *Summary:* Script set up the double-pendulum scenario with two actuators
%
% Copyright (C) 2008-2013 by 
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-05-24
%
%% High-Level Steps
% # Define state and important indices
% # Set up scenario
% # Set up the plant structure
% # Set up the policy structure
% # Set up the cost structure
% # Set up the GP dynamics model structure
% # Parameters for policy optimization
% # Plotting verbosity
% # Some array initializations


%% Code

warning('off','all'); 

% include some paths
try
  rd = '../../';
  addpath([rd 'base'],[rd 'util'],[rd 'gp'],[rd 'control'],[rd 'loss']);
catch
end

% rand('seed',1); randn('seed',1); format short; format compact; 

% 1. Define state and important indices

% 1a. Full state representation (including all augmentations)
%  1  x        path position
%  2  dx       path velocity
%  3  z        path height
%  4  u        applied torque

% 1b. Important indices
% odei  indicies for the ode solver
% augi  indicies for variables augmented to the ode variables
% dyno  indicies for the output from the dynamics model and indicies to loss
% angi  indicies for variables treated as angles (using sin/cos representation)
% dyni  indicies for inputs to the dynamics model
% poli  indicies for variables that serve as inputs to the policy
% difi  indicies for training targets that are differences (rather than values)

odei = [1, 2];                           
augi = [];                                   
dyno = [1, 2];          
angi = [];                                           
dyni = [1, 2]; 
poli = [1, 2];        
difi = [1, 2];         


% 2. Set up the scenario
dt = 0.1;                        % [s] sampling time
T = 3.0;                          % [s] prediction time
H = ceil(T/dt);                   % prediction steps (optimization horizon)
mu0 = zeros(length(odei),1);  % initial state mean
S0 = 0.001*eye(length(odei));               % initial state covariance
N = 10;                           % no. of controller optimizations
J = 1;                            % no. of init. training rollouts (of length H)
K = 1;                            % no. of init. states for which we optimize
nc = 50; %200;                         % size of controller training set

% 3. Set up the plant structure

plant.dynamics = @dynamics_mc;               % dynamics ODE function
plant.augment = @augment_mc; % function to augment the state ODE variables
plant.noise = 0.01*diag([0.1^2 0.01^2]);             % measurement noise
plant.dt = dt;
plant.ctrl = @zoh;        % controler is zero-order-hold
plant.odei = odei;        % indices to the varibles for the ode solver
plant.augi = augi;        % indices of augmented variables
plant.angi = angi;
plant.poli = poli;
plant.dyno = dyno;
plant.dyni = dyni;
plant.difi = difi;
plant.prop = @propagated; % handle to function that propagates state over time

% plant parameters
a = 0.15; b = 0.15; %a = 0.25; b = 0.35;
t1 = 1;
c5 = 0; % from p'(0) = 0
c6 = 0; % from p(0) = 0
A = [t1^5, t1^4, t1^3, t1^2; % p(t1) = b
     (-t1)^5, (-t1)^4, (-t1)^3, (-t1)^2; % p(-t1) = a
     5*t1^4, 4*t1^3, 3*t1^2, 2*t1; % p'(t1) = 0
     5*(-t1)^4, 4*(-t1)^3, 3*(-t1)^2, 2*(-t1)]; % p'(-t1) = 0
B = [b;a;0;0];
coeffs = A\B;
coeffs = [coeffs; c5; c6];

plant.params.m = 1;
plant.params.g = -9.81;
plant.params.sigmoid_gain = 35;
plant.params.sigmoid_xshift = 1.3;
plant.params.coeffs = coeffs;
plant.params.vel_coeffs = polyder(coeffs);
plant.params.drag_coeff = 0.2;

% plant parameters
plant.params.rock_a = 0;
plant.params.rock_b = 1;


% 4. Set up the policy structure
policy.fcn = @(policy,m,s)conCat(@congp,@gSat,policy,m,s);% controller 
                                                          % representation
policy.maxU = 1; %3.5;                                        % max. amplitude of 
                                                          % torque
[mm, ss, cc] = gTrig(mu0, S0, plant.angi);                  % represent angles 
mm = [mu0; mm]; cc = S0*cc; ss = [S0 cc; cc' ss];         % in complex plane  
policy.p.inputs = gaussian(mm, ss, nc)'; % init. location of 
                                                          % basis functions
policy.p.targets = 0.1*randn(nc, length(policy.maxU));    % init. policy targets 
                                                          % (close to zero)
policy.p.hyp = log([1 1 1 0.01]');        % initialize 
                                                          % hyper-parameters

% % 4. Set up the policy structure
% policy.fcn = @(policy,m,s)conCat(@conlin,@gSat,policy,m,s); % linear policy
% policy.maxU = 2.5;                                      % max. amplitude of 
% policy.p.w = 1e-2*randn(length(policy.maxU),length(poli));  % weight matrix
% policy.p.b = zeros(length(policy.maxU),1);                  % bias
                                                            % torques                                                          
                                       

                                                          
% 5. Set up the cost structure
cost.fcn = @loss_mc;                  % cost function
cost.gamma = 1;                             % discount factor
cost.p = [0.5 0.5];                         % lengths of pendulums
% cost.width = 0.3/3;                          % cost function width
cost.width = 0.2; 
cost.expl = -.1;                              % exploration parameter (UCB)
cost.angle = plant.angi;                    % index of angle (for cost function)
% cost.target = [max([a,b]); 1; 0];                   % target state
cost.target = [1; 0];
cost.target2 = [-1; 0];

% 6. Set up the GP dynamics model structure
dynmodel.fcn = @gp1d;                % function for GP predictions
dynmodel.train = @train;             % function to train dynamics model
dynmodel.induce = zeros(300,0,1);    % shared inducing inputs (sparse GP)
trainOpt = [300 500];                % defines the max. number of line searches
                                     % when training the GP dynamics models
                                     % trainOpt(1): full GP,
                                     % trainOpt(2): sparse GP (FITC)

% 7. Parameters for policy optimization
opt.length = 40; %75; %50;                        % max. number of line searches
opt.MFEPLS = 20;                         % max. number of function evaluations
                                         % per line search
opt.verbosity = 3;                       % verbosity: specifies how much 
                                         % information is displayed during
                                         % policy learning. Options: 0-3
opt.method = 'BFGS';                     % optimization method for policy 
                                         % learning.

% 8. Plotting verbosity
plotting.verbosity = 2;            % 0: no plots
                                   % 1: some plots
                                   % 2: all plots

% 9. Some initializations
x = []; y = [];
fantasy.mean = cell(1,N); fantasy.std = cell(1,N);
realCost = cell(1,N); M = cell(N,1); Sigma = cell(N,1);
