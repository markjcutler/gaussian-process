#!/usr/bin/env python

###################################################
# plot_domain.py -- Plot the 2d mountain car domain
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 18 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from scipy import ndimage
import sys
import numpy as np
import scipy.ndimage.interpolation as interp
import scipy.misc
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color='k')


def add_car(x, y, psi, ax, fig, image):
    rotate_car = ndimage.rotate(image, psi*180.0/np.pi)

    dat_coord = [(x, y)]
    # transform the two points from data coordinates to display coordinates:
    tr1 = ax.transData.transform(dat_coord)
    # create an inverse transversion from display to figure coordinates:
    inv = fig.transFigure.inverted()
    tr2 = inv.transform(tr1)

    scale_val = 0.035*(np.cos(np.pi + 4.0*psi)/2.0 + 0.5)

    datco = [tr2[0, 0]-0.05-scale_val/2.0,
             tr2[0, 1]-0.05-scale_val/2.0,
             0.15 + scale_val, 0.15 + scale_val]
    axins = fig.add_axes(datco)
    axins.set_axis_off()
    axins.imshow(rotate_car)
    print 'adding car'


path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/mountainCar3D/'

# plant parameters
coeffs = np.array([0, -0.15, 0, 0.3, 0, 0])
x = np.linspace(-1.5, 1.5, 200)
y = np.polyval(coeffs, x)

fig, ax = plt.subplots()
ax.set_ylim([-0.01, 0.2])

# load car
car = plt.imread(path + 'car.png')
car = np.fliplr(car)

ax.plot(x, y, color=grey2, linestyle='-', linewidth=3)
# ax.plot(1.0, 0.15, 'y*', markersize=15)
# ax.plot(-1.0, 0.15, 'y*', markersize=15)

rotation_rad = 1.05
rotation = rotation_rad*180.0/np.pi
add_car(0.65, 0.132, rotation_rad, ax, fig, car)

legend_fontsize = 20
axis_label_fontsize = 30
linewidth = 1.5
axis_tick_fontsize = 20
annotation_fontsize = 20

ax.set_xlabel('$x~(m)$', size=axis_label_fontsize)
ax.set_ylabel('$z~(m)$', size=axis_label_fontsize)

ax.annotate(r'$\theta$', xy=(0.87, .125),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=axis_label_fontsize)
start_x = 0.75
start_y = 0.121
length = 0.25
rotation_rad = 0.16
end_x = np.cos(rotation_rad)*length + start_x
end_y = np.sin(rotation_rad)*length + start_y
ax.plot([start_x, end_x], [start_y, end_y], linewidth=2, color='k')
ax.plot([start_x, start_x+0.4], [start_y, start_y], linewidth=2, color='k')

ax.annotate(r'$mg$', xy=(0.7, .07),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=axis_label_fontsize)
ax.annotate("", xy=(0.81, 0.083), xycoords='data',
            xytext=(0.81, 0.118), textcoords='data',
            arrowprops=dict(width=1.5, headwidth=6, fc='k', ec='k'))

ax.annotate(r'$-(D+R) \dot{x}$', xy=(-0.148, .078),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=axis_label_fontsize, rotation=rotation)

ax.annotate(r'$u$', xy=(1., .19),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=axis_label_fontsize, rotation=rotation)

start_x = 0.885
start_y = 0.158
length = 0.16
rotation_rad = 0.17
end_x = np.cos(rotation_rad)*length + start_x
end_y = np.sin(rotation_rad)*length + start_y
ax.annotate("", xy=(end_x, end_y), xycoords='data',
            xytext=(start_x, start_y), textcoords='data',
            arrowprops=dict(width=1.5, headwidth=6, fc='k', ec='k'))

start_x = 0.62
start_y = 0.118
length = 0.16
rotation_rad = 0.17
end_x = -np.cos(rotation_rad)*length + start_x
end_y = -np.sin(rotation_rad)*length + start_y
print end_x
print end_y
ax.annotate("", xy=(end_x, end_y), xycoords='data',
            xytext=(start_x, start_y), textcoords='data',
            arrowprops=dict(width=1.5, headwidth=6, fc='k', ec='k'))



for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + 'figures/mc_domain.pdf',
            format='pdf', transparent=True, bbox_inches='tight',
            dpi=1000)


fig, ax = plt.subplots()

# load car
# car = plt.imread(path + 'car.png')
# scaled_car = scipy.misc.imresize(car, .01)
# rotated_car = interp.rotate(scaled_car, 30, reshape=True)
# ax.imshow(rotated_car, aspect='auto')

ax.plot([-1, -0.25, 0, 0.25, 1], [2, 2, 0, 1, 1], color='k', linestyle='-',
        linewidth=2)

ax.set_xlabel(r'$x~(m)$', size=axis_label_fontsize)
ax.set_ylabel(r'$R$', size=axis_label_fontsize)

ax.set_ylim([0, 3])

plt.yticks([1, 2], [r'$b$', r'$a$'])

plt.xticks([-1, -0.25, 0, 0.25, 1], [r'$-1$', r'$-1/4$', r'$0$',
                                        r'$1/4$', r'$1$'])

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + 'figures/mc_rock_force.pdf',
            format='pdf', transparent=True, bbox_inches='tight')
