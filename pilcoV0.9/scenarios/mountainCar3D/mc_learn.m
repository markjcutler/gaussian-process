%% pendubot_learn.m
% *Summary:* Script to learn a controller for the pendubot
% swingup (a double pendulum with the inner joint actuated)
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
rng(1651)
rng('shuffle')

use_prior = 0;
use_hw = 1;
use_gpops = 0;

extra_plots = 0;

settings_mc;            % load scenario-specific settings
basename = 'mc_';       % filename used for saving data


% %% DEBUG!!!
% plant.params.rock_a = 0.0;
% plant.params.rock_b = 2.0;
% % plant.params.drag_coeff = 0;
% % mu0(1) = -0.3;

if use_gpops
    
    % load saved data
    load('training.mat');
    
    %% use kmeans clustering to find nc rbf centers
    [idx, c] = kmeans([input',target'], nc);
    
    % 4. Set up the policy structure                                                              % torque
    policy.p.inputs = c(:,1); % init. location of                           % basis functions
    policy.p.targets = c(:,2);    % init. policy targets
end

%% Execute in HARDWARE
% load and use prior information
if use_prior
    prior_info = load('sim_prior2.mat');
    
    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
    
    % if the prior gp used gpmodel_rw information, then we must account for
    % that in the prior
    % really, we should also probably use the data we collected the last
    % time we ran this system
    if isfield(dynmodel.prior,'gpmodel_rw')
        
        % unless there is good reason not to, copy the data from last time
        % we ran the real world
        x = dynmodel.prior.gpmodel_rw.inputs;
        y = dynmodel.prior.gpmodel_rw.targets;
        
        % need to add back in the prior if it exists -- but it should have
        % been removed when we ran in sim last time
        if isfield(dynmodel.prior.gpmodel_rw,'prior')
            disp('********* WARNING--WE SHOULD NOT BE HERE! **********');
            [~, D] = size(dynmodel.prior.gpmodel_rw.inputs);
            % prior gp mean
            prior_mean = zeros(size(y(:,difi)));
            for i=1:length(y)
                prior_mean(i,:) = dynmodel.prior.gpmodel_rw.prior.fcn(...
                    dynmodel.prior.gpmodel_rw.prior,...
                    dynmodel.prior.gpmodel_rw.inputs(i,:)', zeros(D));
            end
            y(:,difi) = y(:,difi) + prior_mean;
            y(:,plant.angi) = wrapToPi(y(:,plant.angi));
            dynmodel.prior.gpmodel_rw = rmfield(dynmodel.prior.gpmodel_rw,'prior');
        end
        
        
        
        %         % also get an accurate prior by accounting for real-world data
        %         [n, D] = size(dynmodel.gpmodel_rw.inputs);
        %         % prior gp mean
        %         prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        %         for i=1:length(dynmodel.gpmodel_rw.targets)
        %             prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, ...
        %                 dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        %         end
        %         dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) +...
        %             prior_mean;
        %         dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(...
        %             dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        %         dynmodel.prior = rmfield(dynmodel.prior,'gpmodel_rw');
        
    end
    
    
    
    % plant parameters
    plant.params.rock_a = 2;
    plant.params.rock_b = 1;
    
end

%% Execute in SIM
% load and use hardware information
if use_hw
    hw_info = load('rw3.mat');
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    
    
    %         %% DEBUG!!!
    %     figure(13); clf;
    %     x = dynmodel.gpmodel_rw.prior.inputs;
    %     plot3(x(:,1), x(:,2), x(:,3), '*');
    %     hold all
    %     xp = dynmodel.gpmodel_rw.inputs;
    %     plot3(xp(:,1), xp(:,2), xp(:,3), '*');
    %     axis([-1.1 1.1 -2.1 2.1 -2.5 2.5])
    % %     return
    
    
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
    
        %% DEBUG!!!
        figure(14); clf;
        x = -1.1:0.05:1.1;
        dx = -2.1:0.05:2.1;
        u = 2.5;
        p = zeros(length(x), length(dx));
%         dp = zeros(length(x), length(dx), 2);
        for i=1:length(x)
            for j=1:length(dx)
                [p(i,j), dpdm, dpds] = weight_fnc2(dynmodel,[x(i);dx(j);u],zeros(3));
            end
        end
        surf(x, dx, p');
%         figure(15); clf;
%         surf(x, dx, dp');
        return
    
    
    
    
    
end


% return


% 2. Initial J random rollouts
for jj = 1:J
    if use_prior || use_gpops %isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_mc;
    end
end

% x
% return

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
    %     plot_policy(policy);
    trainDynModel;   % train (GP) dynamics model
    
    learnPolicy;     % learn policy
    applyController; % apply controller to system
    disp(['controlled trial # ' num2str(j)]);
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_mc;
    end
end