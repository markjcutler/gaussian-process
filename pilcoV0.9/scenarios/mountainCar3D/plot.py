#!/usr/bin/env python

###################################################
# plot.py -- Load mat files and create pretty plots
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Wednesday, 17 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
import sys
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)


def make_plot(main_path, path_data, env, plot_leg):
    data = scipy.io.loadmat(main_path + 'data/' + path_data)
    cost = np.transpose(np.array(data['cost_array']))
    H = np.transpose(np.array(data['H']))
    fcm = np.array(data['fcm'])
    fcs = np.array(data['fcs'])
    x_actual = np.transpose(np.array(data['x_actual']))
    dx_actual = np.transpose(np.array(data['dx_actual']))
    x_test = np.transpose(np.array(data['x_test']))
    dx_test = np.transpose(np.array(data['dx_test']))
    cost_test = np.transpose(np.array(data['cost_test']))
    real_cost = np.array(data['real_cost'])
    fsm = np.array(data['fsm'])
    fss = np.array(data['fss'])

    cost = cost[0]
    H = H[0]
    fcm = fcm[0]
    fcs = fcs[0]
    x_actual = x_actual[0]
    dx_actual = dx_actual[0]
    real_cost = real_cost[0]

    x = np.arange(H+1)/10.0

    # total_costs = []
    # for j in range(1, i2+1):
    #     total_cost = np.sum(cost[H*(j-1):H*j])
    #     # print total_cost
    #     total_costs.append(total_cost)

    c = ['k', 'b', 'g', 'm']

    linestyle = ['-', '--', '-.', ':']
    legend = ['Test Run', 'Predicted Data', 'Used Data']
    legend_fontsize = 20
    axis_label_fontsize = 22
    linewidth = 1.5
    axis_tick_fontsize = 18
    annotation_fontsize = 20
    labelx = -0.07  # axes coords
    fig, axarr = plt.subplots(3, sharex=True)

    for i in range(10):
        if env == 'Real World':
            color = rw_dark
        else:
            color = sim_dark
        axarr[0].plot(x[1:], cost_test[i], color=color, linestyle='--',
                      linewidth=linewidth/3.0)

    # axarr[0].errorbar(x, fcm, yerr=fcs, color='b',
    #                   linestyle='-',
    #                   linewidth=linewidth)
    axarr[0].fill_between(x, fcm+fcs,
                          fcm-fcs, alpha=0.15,
                          facecolor='k', edgecolor='')

    axarr[0].plot(x[1:], real_cost, color='k', linestyle='-',
                  linewidth=linewidth)

    for i in range(10):
        if i == 0:
            axarr[1].plot(x, x_test[i], color=color, linestyle='--',
                          linewidth=linewidth/3.0, label=legend[0])
        else:
            axarr[1].plot(x, x_test[i], color=color, linestyle='--',
                          linewidth=linewidth/3.0)

    axarr[1].fill_between(x, fsm[0]+fss[0],
                          fsm[0]-fss[0], alpha=0.15,
                          facecolor='k', edgecolor='')

    # h1 = axarr[1].errorbar(x, fsm[0], yerr=fss[0], color='b', linestyle='-',
    #                   linewidth=linewidth, label=legend[1])

    axarr[1].plot([], [], color="k", alpha=0.15, linewidth=15, label=legend[1])

    axarr[1].plot(x, x_actual, color='k', linestyle='-',
                  linewidth=linewidth, label=legend[2])

    if plot_leg:
        axarr[1].legend(fancybox=True,
                        fontsize=legend_fontsize,
                        bbox_to_anchor=(.9, .69),
                        bbox_transform=plt.gcf().transFigure)

    for i in range(10):
        axarr[2].plot(x, dx_test[i], color=color, linestyle='--',
                      linewidth=linewidth/3.0)

    axarr[2].fill_between(x, fsm[1]+fss[1],
                          fsm[1]-fss[1], alpha=0.15,
                          facecolor='k', edgecolor='')
# axarr[2].errorbar(x, fsm[1], yerr=fss[1], color='b', linestyle='-',
#                       linewidth=linewidth)

    axarr[2].plot(x, dx_actual, color='k', linestyle='-',
                  linewidth=linewidth)

    axarr[0].set_ylabel(r'$c(x)$', size=axis_label_fontsize)
    axarr[0].yaxis.set_label_coords(labelx, 0.5)
    axarr[1].set_ylabel(r'$x~(m)$', size=axis_label_fontsize)
    axarr[1].yaxis.set_label_coords(labelx, 0.5)
    axarr[2].set_ylabel(r'$\dot{x}~(m/s)$', size=axis_label_fontsize)
    axarr[2].yaxis.set_label_coords(labelx, 0.5)

    cost_str = '%.1f' % np.sum(real_cost)

    axarr[0].annotate(env+'\nCost = '+cost_str, xy=(2.2, 0.7),  xycoords='data',
                      xytext=(0, 0), textcoords='offset points',
                      bbox=dict(boxstyle="round", fc="1.0", edgecolor=axes_ticks_color),
                      fontsize=legend_fontsize)

    for i in range(3):
        ax = axarr[i]
        #ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)

        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(axis_tick_fontsize)

        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Only show ticks on the left and bottom spines
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')

        ax.set_xlim(0, H/10.0)

        # Find at most 101 ticks on the y-axis at 'nice' locations
        max_yticks = 2
        yloc = plt.MaxNLocator(max_yticks)
        ax.yaxis.set_major_locator(yloc)

    ax.set_xlabel('$t~(s)$', size=axis_label_fontsize)

    plt.savefig(main_path + 'figures/' + path_data + '_mc.pdf',
                format='pdf', transparent=True, bbox_inches='tight')

    total_cost = np.zeros(11)
    for i in range(11):
        if i == 0:
            total_cost[i] = np.sum(real_cost)
        else:
            total_cost[i] = np.sum(cost_test[i-1])
    print total_cost
    return total_cost


path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/mountainCar3D/'

costs = []
costs_std = []
plot_leg = True
for i1 in range(1, 5):
    i2 = 0
    costs.append([])
    costs_std.append([])
    while 1:
        i2 += 1
        print 'i1 = ' + str(i1) + ', i2 = ' + str(i2)
        try:
            path_data = 'gp_data_' + str(i1) + '_' + str(i2)
            if i1 == 1 or i1 == 3:
                env = 'Simulator'
            else:
                env = 'Real World'
            total_cost = make_plot(path_main, path_data, env, plot_leg)
            if plot_leg:
                plot_leg = False
            total_cost_mean = np.mean(total_cost)
            total_cost_std = np.std(total_cost)
            costs[i1-1].append(total_cost_mean)
            costs_std[i1-1].append(total_cost_std)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            break

        # path_data = 'gp_data_' + str(i1) + '_' + str(i2)
        # if i1 == 1 or i1 == 3:
        #     env = 'Simulator'
        # else:
        #     env = 'Real World'
        # total_cost = make_plot(path_main, path_data, env, plot_leg)
        # if plot_leg:
        #     plot_leg = False
        # total_cost_mean = np.mean(total_cost)
        # total_cost_std = np.std(total_cost)
        # costs[i1-1].append(total_cost_mean)
        # costs_std[i1-1].append(total_cost_std)



fig, ax = plt.subplots()

legend = ['Simulation Run 1', 'Real World Run 1', 'Simulation Run 2',
          'Real World Run 2']
#c = ['b', 'g', 'r', 'c']
c = [sim_light, rw_light, sim_dark, rw_dark]
# ls = ['-', '-.', ':', '--']
ls = ['-', '-', '-', '-']
alpha = 0.65

x5 = range(1, 6)
x4 = range(1, 5)

for i in range(4):
    x = x5
    if i == 3:
        x = x4
    ax.plot(x, costs[i], color=c[i], linestyle=ls[i], linewidth=3,
            label=legend[i], zorder=i)
    ax.fill_between(x, np.array(costs[i]) - np.array(costs_std[i]),
                    np.array(costs[i]) + np.array(costs_std[i]), alpha=alpha,
                    facecolor=c[i], edgecolor='', zorder=i)

legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20

xloc = plt.MaxNLocator(integer=True)
ax.xaxis.set_major_locator(xloc)

ax.legend([legend[0], legend[1], legend[2], legend[3]],
          loc='upper right', fancybox=True,
          fontsize=legend_fontsize)

ax.set_ylabel('Total Cost', size=axis_label_fontsize)
ax.set_xlabel('Iteration', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path_main + 'figures/total_costs.pdf',
            format='pdf', transparent=True, bbox_inches='tight')
