function mc_learn_fun(prior, hw, gpops, num_runs, global_iter)

rng('default')
% rng(1651)
rng('shuffle')

settings_mc;            % load scenario-specific settings
N = num_runs;
basename = 'mc_';       % filename used for saving data

use_prior = 0;
use_hw = 0;
use_gpops = 0;

if prior ~= 0
    use_prior = 1;
    plant.params.c = 0.25;
end
if hw ~= 0
    use_hw = 1;
    plant.params.c = 1;
end
if gpops ~= 0
    use_gpops = 1;
    plant.params.c = 1;
end


if use_gpops
    
    % load saved data
    gpops = load(gpops);
    policy = gpops.policy;
    
end

%% load and use prior information
if use_prior
    prior_info = load(prior);
    
    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
    
    % if the prior gp used gpmodel_rw information, then we must account for
    % that in the prior
    % really, we should also probably use the data we collected the last
    % time we ran this system
    if isfield(dynmodel.prior,'gpmodel_rw')
        
        % unless there is good reason not to, copy the data from last time
        % we ran the real world
        x = dynmodel.prior.gpmodel_rw.inputs;
        y = dynmodel.prior.gpmodel_rw.targets;
        
        % need to add back in the prior if it exists -- but it should have
        % been removed when we ran in sim last time
        if isfield(dynmodel.prior.gpmodel_rw,'prior')
            disp('********* WARNING--WE SHOULD NOT BE HERE! **********');
            [~, D] = size(dynmodel.prior.gpmodel_rw.inputs);
            % prior gp mean
            prior_mean = zeros(size(y(:,difi)));
            for i=1:length(y)
                prior_mean(i,:) = dynmodel.prior.gpmodel_rw.prior.fcn(...
                    dynmodel.prior.gpmodel_rw.prior,...
                    dynmodel.prior.gpmodel_rw.inputs(i,:)', zeros(D));
            end
            y(:,difi) = y(:,difi) + prior_mean;
            y(:,plant.angi) = wrapToPi(y(:,plant.angi));
            dynmodel.prior.gpmodel_rw = rmfield(dynmodel.prior.gpmodel_rw,'prior');
        end
        
    end
    
    % plant parameters
    plant.params.rock_a = 2;
    plant.params.rock_b = 1;
    
end

%% load and use hardware information
if use_hw
    hw_info = load(hw);
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
end

% 2. Initial J random rollouts
for jj = 1:J
    if use_prior || use_gpops %isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_mc;
    end
end


mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
    trainDynModel;   % train (GP) dynamics model
    
    rng('shuffle')
    
    learnPolicy;     % learn policy
    applyController; % apply controller to system
    disp(['controlled trial # ' num2str(j)]);
    if plotting.verbosity > 0;      % visualization of trajectory
        if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
        draw_rollout_mc;
    end
    
    save_data(global_iter, j, y, cost, H, fantasy.mean{j}, 2*fantasy.std{j}, ...
        realCost{J+j}, M{j}, [2*sqrt(squeeze(Sigma{j}(1,1,:))), ...
        2*sqrt(squeeze(Sigma{j}(2,2,:)))]', latent, lat, lat_cost)
end


function save_data(i,j,state,cost,H,fcm,fcs,real_cost,fsm,fss,latent, lat, ...
    lat_cost)

cost_array = zeros(size(state));
n = size(state,2);
for ii=1:size(state,1)
    cost_array(ii,:) = cost.fcn(cost,state(ii,:),zeros(n));
end

% build controller test outputs
x_actual = latent{j}(:,1);
dx_actual = latent{j}(:,2);
x_test = [];
dx_test = [];
cost_test = [];
for ii=1:10
    x_test = [x_test, lat{ii}(:,1)];
    dx_test = [dx_test, lat{ii}(:,2)];
    cost_test = [cost_test, lat_cost{ii}'];
end

save(strcat('data/gp_data_',num2str(i),'_',num2str(j)','.mat'),...
    'cost_array','H','fcm','fcs',...
    'real_cost','fsm','fss','x_actual','dx_actual','x_test',...
    'dx_test', 'cost_test');