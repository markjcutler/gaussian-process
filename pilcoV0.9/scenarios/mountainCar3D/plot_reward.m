clc; clear all;




x = -2:0.01:2;

xt1 = -1;
xt2 =  1;
sc = 0.5;

% cost = zeros(size(x));
cost = 1 - exp(-1/(2*sc^2)*(x-xt1).^2) - exp(-1/(2*sc^2)*(x-xt2).^2);

figure(1); clf;
plot(x,cost)










% x = -1.5:0.01:1.5;
% 
% % p(t) = c1*t^5 + c2*t^4 + c3*t^3 + c4*t^2 + c5*t + c6
% 
% a = .5;
% b = .3;
% t1 = 1;
% 
% c5 = 0; % from p'(0) = 0
% c6 = 0; % from p(0) = 0
% 
% A = [t1^5, t1^4, t1^3, t1^2; % p(t1) = b
%      (-t1)^5, (-t1)^4, (-t1)^3, (-t1)^2; % p(-t1) = a
%      5*t1^4, 4*t1^3, 3*t1^2, 2*t1; % p'(t1) = 0
%      5*(-t1)^4, 4*(-t1)^3, 3*(-t1)^2, 2*(-t1)]; % p'(-t1) = 0
% B = [b;a;0;0];
% coeffs = A\B;
% coeffs = [coeffs; c5; c6];
% 
% y = polyval(coeffs, x);
% 
% x_test = 2*rand - 1;
% dp = polyder(coeffs);
% slope = polyval(dp, x_test);
% m = 1;
% g = -9.81;
% sin_theta = slope/sqrt(1+slope^2);
% gravity_force = m*g*sin_theta;
% 
% gain = 35;
% x_shift = -1.3;
% y2 = 1./(1+exp(-gain*(x-x_shift)));
% x_shift = 1.3;
% y3 = 1./(1+exp(gain*(x-x_shift)));
% 
% figure(1);clf;
% plot(x,y.*y2.*y3)
% hold all
% plot(x,y2)
% plot(x,y3)
% plot([-t1, 0, t1], [a, 0, b], '*')
% plot(x_test, polyval(coeffs, x_test), '*');
% axis([-1.5, 1.5, -0.1, a+.1])
% axis('equal')

