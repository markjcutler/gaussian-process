% mfrl_reverse_mc.m

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
% rng(1651)
rng(1496011235)
% rng('shuffle')
current_seed = rng;

% first, learn in sim until convergence (possibly with gpops bootstrap)
global_iter = 1;
mc_learn_fun(0, 0, 'gpops_gp_policy.mat', 5, global_iter); global_iter = global_iter+1;
% mc_learn_fun(0, 0, 0, 5, global_iter); global_iter = global_iter+1;

% next, learn in real world using sim as prior
mc_learn_fun('mc_5_H30.mat', 0, 0, 5, global_iter); global_iter = global_iter+1;

% learn in sim again using real-world data
mc_learn_fun(0, 'mc_5_H30.mat', 0, 5, global_iter); global_iter = global_iter+1;
% mc_learn_fun(0, 'sim_prior2.mat', 0, 5, global_iter); global_iter = global_iter+1;

% finally, learn in real world again with updated simulation prior
mc_learn_fun('mc_5_H30.mat', 0, 0, 4, global_iter); global_iter = global_iter+1;