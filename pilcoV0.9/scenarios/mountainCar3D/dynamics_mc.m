%% dynamics_mc.m
% *Summary:* Implements ths ODE for simulating the Pendubot 
% dynamics, where an input torque f can be applied to the inner link 
%
%    function dz = dynamics_mc(t,z,f)
%
%
% *Input arguments:*
%
%	t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f     (optional): torque f(t) applied to inner pendulum
%
% *Output arguments:*
%   
%   dz    if 3 input arguments:      state derivative wrt time
%         if only 2 input arguments: total mechanical energy
%
%   Note: It is assumed that the state variables are of the following order:
%         x:  [m] car position
%         dx:  [m/s] car velocity
%
% A detailed derivation of the dynamics can be found in:
%
% M.P. Deisenroth: 
% Efficient Reinforcement Learning Using Gaussian Processes, Appendix C, 
% KIT Scientific Publishing, 2010.
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-08

function dz = dynamics_mc(t,z,f, params)
%% Code

x = z(1);
dx = z(2);

applied_force = f(t)*2.5;

% compute gravity force
slope = polyval(params.vel_coeffs, x);
sin_theta = slope/sqrt(1+slope^2);
gravity_force = params.m*params.g*sin_theta;

% compute drag force
drag_force = -params.drag_coeff*dx;

% compute rock forces
if x < 0
    if x < -0.25
        rock_force = -params.rock_a*dx;
    else
        rock_force = -interp1([-0.25, 0], [params.rock_a, 0], x)*dx;
    end
else
    if x > 0.25
        rock_force = -params.rock_b*dx;
    else
        rock_force = -interp1([0, 0.25], [0, params.rock_b], x)*dx;
    end
end

xdot = dx;
dxdot = (applied_force + gravity_force + drag_force + rock_force)/params.m;

dz = [xdot; dxdot];


