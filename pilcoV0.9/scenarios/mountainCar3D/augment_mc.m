%% augment_mc.m
% *Summary:* The function computes the $(x,y)$ velocities of the contact point 
% in both absolute and unicycle coordinates as well as the the unicycle 
% coordinates of the contact point themselves.
%
%       function r = augment(s)
%
% *Input arguments:*
%
%		s     state of the car.             [1 x 18]
%         The state is assumed to be given as follows:
%         z      empty (to be filled by this function)
%         x      path position
%         dx     path velocity

%
% *Output arguments:*
%
%   r     additional variables that are computed based on s:          [1 x 6]
%         z    height


function r = augment_mc(s, params)
%% Code

% compute height
x = s(2);
r = polyval(params.coeffs, x);

% r2 = 1./(1+exp(-params.sigmoid_gain*(x+params.sigmoid_xshift)));
% r3 = 1./(1+exp( params.sigmoid_gain*(x-params.sigmoid_xshift)));
% 
% r = r.*r2.*r3;
