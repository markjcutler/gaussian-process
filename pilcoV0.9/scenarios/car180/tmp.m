% tmp.m

x = 0:180;

y = cos(pi + 4*x*pi/180)/2 + .5;

figure(1); clf;
plot(x,y)