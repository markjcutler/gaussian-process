function save_gp_params(policy, n, D, H, dt, random, dy0)
%% save gp params
X = policy.p.hyp;    

E = size(X,2);

for i=1:E
    inp = bsxfun(@rdivide,policy.p.inputs,exp(X(1:D,i))');

    X(D+1,i) = log(1);
    X(D+2,i) = log(0.01);
    K = exp(2*X(D+1,i)-maha(inp,inp)/2);

    L = chol(K + exp(2*X(D+2,i))*eye(n))';
    beta = L'\(L\policy.p.targets(:,i));
    inputs = policy.p.inputs;
    iL = diag(exp(-X(1:D,i))); % inverse length-scales
    iL2 = diag(iL*iL);
    save(strcat('gp_params',num2str(i),'.mat'),'n','D','E','inputs','beta','iL2','H','dt','random','dy0');
end