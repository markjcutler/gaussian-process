#!/usr/bin/env python

###################################################
# plot_car_snapshots.py -- Plot snapshots of toy car
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday, 19 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from scipy import ndimage
import sys
import numpy as np
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)


def plot_trajectory(time, dx, dy, dpsi, x0):

    n = len(time)
    x = np.zeros(n)
    y = np.zeros(n)
    psi = np.zeros(n)
    psi[0] = np.pi/2.0

    x[0] = x0

    for i in range(1, n):
        dt = time[i] - time[i-1]
        psi[i] = psi[i-1] + dpsi[i]*dt
        x[i] = x[i-1] + dx[i]*dt
        y[i] = y[i-1] + dy[i]*dt

    return x, y, psi


def add_car(x, y, psi, ax, fig, image):
    rotate_car = ndimage.rotate(image, psi*180.0/np.pi)

    dat_coord = [(x, y)]
    # transform the two points from data coordinates to display coordinates:
    tr1 = ax.transData.transform(dat_coord)
    # create an inverse transversion from display to figure coordinates:
    inv = fig.transFigure.inverted()
    tr2 = inv.transform(tr1)

    scale_val = 0.035*(np.cos(np.pi + 4.0*psi)/2.0 + 0.5)

    datco = [tr2[0, 0]-0.05-scale_val/2.0,
             tr2[0, 1]-0.05-scale_val/2.0,
             0.1 + scale_val, 0.1 + scale_val]
    axins = fig.add_axes(datco)
    axins.set_axis_off()
    axins.imshow(rotate_car, alpha=1.0)
    print 'adding car'


def plot_cars(t, dt, x, y, psi, ax, fig, image, limits):
    t_car = np.arange(0, t[-1], dt)
    current_t = 0
    for i in range(len(x)):
        try:
            if (t[i] >= t_car[current_t] and x[i] >= limits[0] and
                x[i] <= limits[1] and y[i] >= limits[2] and y[i] <= limits[3]):
                add_car(x[i], y[i], psi[i], ax, fig, image)
                current_t += 1
        except:
            pass


path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/car180/'
path_car = '/home/mark/gaussian-process/pilcoV0.9/scenarios/car/'
data = 'gpops_vs_pilco.mat'

data = scipy.io.loadmat(path_main + data)

pm = np.array(data['pm'][0])
pstd = np.array(data['pstd'][0])
psem = np.array(data['psem'][0])
pc = np.array(data['pc'])
pdx = np.array(data['pdx'])
pdy = np.array(data['pdy'])
pdpsi = np.array(data['pdpsi'])

gm = np.array(data['gm'][0])
gstd = np.array(data['gstd'][0])
gsem = np.array(data['gsem'][0])
gc = np.array(data['gc'])
gdx = np.array(data['gdx'])
gdy = np.array(data['gdy'])
gdpsi = np.array(data['gdpsi'])

dt = 0.1
T = len(pm)*dt
t = np.arange(0, T, dt)

legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
color = [sim_light, sim_dark, '#84DEBD']
linestyle = ['-', '-']

xmin = -1.0
xmax = 5.0
ymin = -1
ymax = 4

pcosts = np.sum(pc, axis=1)
gcosts = np.sum(gc, axis=1)


# fig, axarr = plt.subplots(2)
gs = GridSpec(2, 1, height_ratios=[3,1])

ax = plt.subplot(gs[0])
axin = plt.subplot(gs[1])

ax.set_ylabel(r'$c(x)$', size=axis_label_fontsize)
ax.set_xlabel(r'$t$ $(s)$', size=axis_label_fontsize)

n, m = pc.shape
for i in range(n):
    ax.plot(t, pc[i, :], color=color[0], linewidth=1.8)
    ax.plot(t, gc[i, :], color=color[1], linewidth=1.8)
# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

# ax.get_yaxis().set_label_coords(-0.07, 0.6)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

ax.set_ylim([-0.05, 1.05])

max_yticks = 6
yloc = plt.MaxNLocator(max_yticks)
ax.yaxis.set_major_locator(yloc)

axin.hist(gcosts, 20, color=color[1], edgecolor=color[1], alpha=1)
axin.hist(pcosts, 20, color=color[0], edgecolor=color[0], alpha=0.8)
axin.set_xlabel(r'sum($c(x)$)', size=axis_label_fontsize)
axin.spines['left'].set_visible(False)
axin.spines['right'].set_visible(False)
axin.spines['top'].set_visible(False)
axin.xaxis.set_ticks_position('bottom')
axin.yaxis.set_visible(False)


plt.savefig(path_main + 'gpops_vs_pilco.pdf',
            format='pdf', transparent=True, bbox_inches='tight', dpi=1000)

xmin = -2
ymin = -2
xmax = 3
ymax = 3

legend = ['No Prior (Sim)', 'Optimal Control Prior (Sim)']

fig, ax = plt.subplots()

ax.set_xlim([xmin, xmax])
ax.set_ylim([ymin, ymax])

limits = np.array([xmin, xmax, ymin, ymax])

no_prior = [3, 4, 5, 6]
prior = 0
legend_set = False
for n in no_prior:
    x, y, psi = plot_trajectory(t, pdx[n, :], pdy[n, :],
                                pdpsi[n, :], 0)
    if not legend_set:
        ax.plot(x, y, color=color[0], linewidth=4, label=legend[0])
        legend_set = True
    else:
        ax.plot(x, y, color=color[0], linewidth=4)
    image = plt.imread(path_car + 'car_square_light_sim.png')
    plot_cars(t, 0.4, x, y, psi, ax, fig, image, limits)


x2, y2, psi2 = plot_trajectory(t, gdx[prior, :], gdy[prior, :],
                               gdpsi[prior, :], 0)
ax.plot(x2, y2, color=color[1], linewidth=4, label=legend[1])

ax.legend(fancybox=True,
          fontsize=legend_fontsize,
          loc='lower right')

image = plt.imread(path_car + 'car_square_dark_sim.png')
plot_cars(t, 0.4, x2, y2, psi2, ax, fig, image, limits)

ax.set_xlabel(r'$x$ $(m)$', size=axis_label_fontsize)
ax.set_ylabel(r'$y$ $(m)$', size=axis_label_fontsize)
ax.annotate(r'$V_0=2.0 \frac{m}{s}$', xy=(0.25, -0.25),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=legend_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path_main + 'gpops_vs_pilco_traj.pdf',
            format='pdf', transparent=True, bbox_inches='tight', dpi=1000)
