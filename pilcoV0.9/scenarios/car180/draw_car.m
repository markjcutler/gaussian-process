%% draw_cp.m
% *Summary:* Draw the simple car
%
%    function draw_cp(x, y, psi, delta, vel, text1, text2, M, S)
%
%
% *Input arguments:*
%
%		x      x position 
%		y      y position 
%     psi      heading angle
%   delta      steering angle
%     vel      velocity command
%   cost       cost function
%     .fcn     function handle (it is assumed to use saturating cost)
%     .<>      other fields that are passed to cost
%   M          (optional) mean of state
%   S          (optional) covariance of state
%   text1      (optional) text field 1
%   text2      (optional) text field 2
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-07

function draw_sc(x, y, psi, delta, vel, cost, text1, text2, M, S)
% function draw_sc(x, y, psi, vel, cost, text1, text2, M, S)
% delta = 0;
%% Code
 
height = 0.1;
width  = 0.3;

l = 0.6;
xmin = -7; 
xmax = 2;  
ymin = -1;
ymax = 8;
maxDelta = 45*pi/180;
maxVel = 0.5;

% Compute positions 
car = [  width,  height
         width, -height
        -width, -height
        -width,  height
         width,  height ];
     
% rotate
R = [cos(psi), -sin(psi); sin(psi), cos(psi)];
car = car*R';


clf; hold on
% plot(0,2*l,'k+','MarkerSize',20,'linewidth',2)
% plot([xmin, xmax], [-1, -1],'k','linewidth',2)

% Plot delta
plot([0 delta/maxDelta*xmax],[-1.3, -1.3],'g','linewidth',10)

% Plot vel
plot([0 vel/maxVel*xmax],[-1.5, -1.5],'g','linewidth',10)

% Plot reward
% reward = 1-cost.fcn(cost,[x, y, psi]', zeros(3));
% plot([0 reward*xmax],[-1.7, -1.7],'y','linewidth',10)

% Plot the car
fill(x+car(:,1), y+car(:,2),'k','edgecolor','k');


% % plot ellipse around tip of pendulum (if M, S exist)
% try
%   [M1 S1] = getPlotDistr_cp(M,S,2*l);
%   error_ellipse(S1,M1,'style','b');
% catch
% end
% 
% % Text
% text(0,-0.3,'applied force')
% text(0,-0.5,'immediate reward')
% if exist('text1','var')
%   text(0,-0.9, text1)
% end
% if exist('text2','var')
%   text(0,-1.1, text2)
% end

set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[ymin ymax]);
axis on;
grid on;
drawnow;