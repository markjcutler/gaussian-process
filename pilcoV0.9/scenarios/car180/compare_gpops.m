% simple script for collecting data on the car

clc; clear all;

num_runs = 7;
num_iters = 20;

% first, get data using no gpops
for i=1:num_iters
    car_learn_fun(0, 0, 0, 0, num_runs, i, 0);
end

% second, use gpops rollout
for i=1:num_iters
    car_learn_fun(0, 0, 0, 'car_guess.mat', num_runs, i+num_iters, 0);
end

% third, use gpops rollout and gpops policy
for i=1:num_iters
    car_learn_fun(0, 0, 'gpops_gp_policy_180.mat', 'car_guess.mat', num_runs, i+2*num_iters, 0);
end

% forth, use gpops rollout and gpops policy
for i=1:num_iters
    car_learn_fun(0, 0, 'gpops_gp_policy_180.mat', 0, num_runs, i+3*num_iters, 0);
end
