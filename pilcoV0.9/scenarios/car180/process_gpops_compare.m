function process_gpops_compare

file_num = 7;
base_name = 'data/gp_data_';
end_name = strcat('_', num2str(file_num), '.mat');

pilco = process_group(1, 20, base_name, end_name);
gpops_real = process_group(21, 40, base_name, end_name);
gpops = process_group(41, 60, base_name, end_name);

pcosts = sum(pilco.costs)
gcosts = sum(gpops.costs)


figure(2); clf;
hist(pcosts, 20, 'b')
figure(3); clf;
hist(gcosts, 20, 'r')

figure(1); clf;
errorbar(pilco.mean, pilco.std)
hold all
% errorbar(gpops_real.mean, gpops_real.std)
errorbar(gpops.mean, gpops.std)
% hold on
% for i=1:size(pilco.costs,2)
%     plot(pilco.costs(:,i), 'b', 'linewidth', 2)
% end
% for i=1:size(gpops.costs,2)
%     plot(gpops.costs(:,i), 'r', 'linewidth', 2)
% end
% for i=1:size(gpops_real.costs,2)
%     plot(gpops_real.costs(:,i), 'r')
% end

save_vals(pilco, gpops)

function save_vals(pilco, gpops)

pm = pilco.mean';
pstd = pilco.std';
psem = pilco.sem';
pc = pilco.costs';
pdx = pilco.dx';
pdy = pilco.dy';
pdpsi = pilco.dpsi';
pomega = pilco.omega';
pdelta = pilco.delta';

gm = gpops.mean';
gstd = gpops.std';
gsem = gpops.sem';
gc = gpops.costs';
gdx = gpops.dx';
gdy = gpops.dy';
gdpsi = gpops.dpsi';
gomega = gpops.omega';
gdelta = gpops.delta';

save('gpops_vs_pilco.mat')

function out = process_group(s, e, base_name, end_name)

cost_sum = [];
costs = [];
dx = [];
dy = [];
dpsi = [];
omega = [];
delta = [];

for i=s:e
    current_costs = process_file(strcat(base_name, num2str(i), end_name));
    cost_sum = [cost_sum, current_costs.cost];
    costs = [costs, current_costs.costs];
    dx = [dx, current_costs.dx];
    dy = [dy, current_costs.dy];
    dpsi = [dpsi, current_costs.dpsi];
    omega = [omega, current_costs.omega];
    delta = [delta, current_costs.delta];
end

out.mean_sum = mean(cost_sum);
out.std_sum = std(cost_sum);
out.sem_sum = std(cost_sum)/sqrt(length(cost_sum));

out.mean = mean(costs, 2);
out.std = std(costs, 0, 2);
out.sem = out.std/sqrt(length(costs));
out.costs = costs;
out.dx = dx;
out.dy = dy;
out.dpsi = dpsi;
out.omega = omega;
out.delta = delta;



function out = process_file(file_name)

data = load(file_name);

% calculate costs
% cost = sum(data.cost_actual, 1);
% out.cost = cost;
% out.costs = data.cost_actual;

cost = sum(data.real_cost);
out.cost = cost;
out.costs = data.real_cost';
out.dx = data.Vx_actual;
out.dy = data.Vy_actual;
out.dpsi = data.dpsi_actual;
out.omega = data.omega_actual;
out.delta = data.delta_actual;
