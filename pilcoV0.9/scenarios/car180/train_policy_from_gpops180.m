% train initial policy using data from GPOPs

clear all; clc;

% load saved data
% load('training.mat');
% load('training2.mat');

% load('training_car.mat');
% load('car180.mat');
load('car_guess.mat');
maxU = [1, 0.5];
% input = State;
input = State([1,2,3,4,5],:); % ignore wheelspeed
target_raw = Force(1,:);
target2 = Force(2,:);

%% filter the control signal from gpops
d1 = designfilt('lowpassiir','FilterOrder',12, ...
    'HalfPowerFrequency',1*0.15,'DesignMethod','butter');
target = filtfilt(d1,target_raw);

% target = target_raw;

figure(1);clf;

% plot(input(4,:));
plot(target_raw);
hold all
plot(target, 'linewidth',3);
% return;

% load('/home/mark/gaussian-process/gpops/mountainCar/training_mc.mat');
% maxU = 1; %2.5;
% input = State;
% target = Force/2.5;

num_inputs = size(input,1);

% input = linspace(0, 1, 41);
% target = sin(input*5);

% net = newrb(input, target);
% 
% % input2 = input(1):-0.1:input(end);
% test = sim(net,input);
% 
% figure(1); clf;
% plot(input,target);
% hold all
% plot(input,test);

%% use kmeans clustering to find nc rbf centers
nc = 50;
target = [target; target2];
[idx, c] = kmeans([input',target'], nc);


% 4. Set up the policy structure
policy.fcn = @(policy,m,s)conCat(@congp,@gSat,policy,m,s);% controller 
                                                          % representation
policy.maxU = maxU;                                        % max. amplitude of 
                                                          % torque
policy.p.inputs = c(:,1:num_inputs); % init. location of 
                                                          % basis functions
policy.p.targets = c(:,[num_inputs+1,num_inputs+2]);    % init. policy targets 
                                                          % (close to zero)
policy.p.hyp = repmat(log([ones(1, num_inputs) 1 0.01]'),1,2);        % initialize 
                   
policy.train = @train;
trainOpt = [300 500];

policy.p = policy.train(policy.p, 0, trainOpt);  %  train dynamics GP

% display some hyperparameters
Xh = policy.p.hyp;     
% noise standard deviations
disp(['Learned noise std: ' num2str(exp(Xh(end,:)))]);
% signal-to-noise ratios (values > 500 can cause numerical problems)
disp(['SNRs              : ' num2str(exp(Xh(end-1,:)-Xh(end,:)))]);

% input2 = linspace(0,3, 100);
testout = zeros(size(target));
for i=1:length(testout)
    [testout(:,i), ~, ~] = policy.fcn(policy,input(:,i),zeros(num_inputs));
%     [testout(i), ~, ~] = gp2(policy.p,input(i),0);
%     [testout(i), ~, ~] = gp2(policy.p,input2(i),0);
end

figure(2); clf
plot(input,target(1,:))
hold all
plot(input,testout(1,:))

figure(3); clf
plot(input,target(2,:))
hold all
plot(input,testout(2,:))

figure(4); clf
plot3(input(1,:), input(2,:) ,target)
hold all
plot3(input(1,:), input(2,:) ,testout)
plot3(c(:,1), c(:,2), c(:,3), '*')

save('gpops_gp_policy_180.mat','policy');