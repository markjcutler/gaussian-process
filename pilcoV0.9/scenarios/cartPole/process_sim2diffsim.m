% process_pend_sim2sim.m

function process_sim2diffsim
cost_steps = 25;
num_learning_tries = 20;
num_repeat = 5;
veh = [2,3,4];
num_par_tracks = 4;
num_iterations=2;
num_episodes = 7;
vary_b = {'0', '2', '5', '10', '15'};
vary_T = {'0p7', '0p8', '0p9', '1p3', '1p5', '2p0'};

[b_mean, b_std, b_stderr] = parse_data(vary_b, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/vary_b_', num_repeat, num_learning_tries, cost_steps);
[b_mean_prior, b_std_prior, b_stderr_prior] = parse_data(vary_b, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/vary_b_prior_', num_repeat, num_learning_tries, cost_steps);
[nom_mean, nom_std, nom_stderr] = parse_data(1, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/nom_param_', num_repeat, num_learning_tries, cost_steps);
[nom_mean_prior, nom_std_prior, nom_stderr_prior] = parse_data(1, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/nom_param_prior_', num_repeat, num_learning_tries, cost_steps);
[T_mean, T_std, T_stderr] = parse_data(vary_T, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/vary_T_', num_repeat, num_learning_tries, cost_steps);
[T_mean_prior, T_std_prior, T_stderr_prior] = parse_data(vary_T, veh, num_par_tracks, num_iterations, ...
    num_episodes, '/vary_T_prior_', num_repeat, num_learning_tries, cost_steps);

save 'cartPole_sim2diffsim.mat' b_mean b_std b_stderr b_mean_prior b_std_prior b_stderr_prior ...
    nom_mean nom_std nom_stderr nom_mean_prior nom_std_prior nom_stderr_prior...
    T_mean T_std T_stderr T_mean_prior T_std_prior T_stderr_prior
              

function [m,s,sem] = parse_data(param, veh, num_par_tracks, num_iterations, num_episodes, ...
    base_name, num_repeat, num_learning_tries, cost_steps)

cost = zeros(length(param), num_repeat*num_learning_tries, num_episodes+1);

for ii=1:length(param)
    learning_try=0;
    cnt=1;
    for v=1:length(veh)
        if learning_try >= num_learning_tries
            break
        end
        for i=1:num_par_tracks
            if learning_try >= num_learning_tries
                break
            end
            for j=1:num_iterations
                if learning_try >= num_learning_tries
                    break
                end
                for k=1:num_repeat
                    for l=0:num_episodes
                        if length(param) == 1
                            data = load(['sim2diffsim/veh' num2str(veh(v)) base_name ...
                                num2str(i) num2str(j) num2str(l) '_H40']);
                        else
                            data = load(['sim2diffsim/veh' num2str(veh(v)) base_name ...
                            param{ii} num2str(i) num2str(j) num2str(l) '_H40']);
                        end
    %                     data.policy.p.targets
    %                     data.realCost
                        [xx, yy, realCost, latent] = ...
                            rollout(gaussian(data.mu0, data.S0), data.policy, data.H, data.plant, data.cost);
    %                     realCost
                        cost(ii, cnt, l+1) = sum(realCost(end-cost_steps+1:end));
                    end
                    cnt = cnt + 1
                end
                learning_try = learning_try+1
            end
        end
    end
end

m = zeros(length(param),num_episodes+1);
s = zeros(length(param),num_episodes+1);
sem = zeros(length(param),num_episodes+1);
for i=1:length(param)
    m(i,:) = reshape(mean(cost(i,:,:)),1,num_episodes+1);
    s(i,:) = reshape(std(cost(i,:,:)),1,num_episodes+1);
    sem(i,:) = reshape(std(cost(i,:,:))/sqrt(num_repeat*num_learning_tries),1,num_episodes+1);
end
m
s
sem