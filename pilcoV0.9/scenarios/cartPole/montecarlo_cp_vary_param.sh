#!/bin/bash
process=$1
N=2
set_random=0

# set parameter values
b=( 0 2 5 10 15 )
b_name=( 0 2 5 10 15 )
T=( 0.7 0.8 0.9 1.3 1.5 2.0 )
T_name=( "0p7" "0p8" "0p9" "1p3" "1p5" "2p0" )
nom_val=1

# nomial performance
use_prior=0
base_name="nom_param_"
use_prior_policy=0
for (( i=1; i<=$N; i++)) 
do
    eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done

# nomial performance -- use prior
use_prior=1
base_name="nom_param_prior_"
use_prior_policy=1
for (( i=1; i<=$N; i++)) 
do
    eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${process}${i}\',${use_prior_policy}\)"
done

# vary b
use_prior=0
base_name="vary_b_"
use_prior_policy=0
for (( i=0; i<${#b[@]}; i++)) 
do
    for (( j=1; j<=$N; j++)) 
    do
        eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${b_name[i]}${process}${j}\',${use_prior_policy},\${b[i]},\${nom_val}\)"
    done
done

# vary b -- use prior
use_prior=1
base_name="vary_b_prior_"
use_prior_policy=1
for (( i=0; i<${#b[@]}; i++)) 
do
    for (( j=1; j<=$N; j++)) 
    do
        eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${b_name[i]}${process}${j}\',${use_prior_policy},\${b[i]},\${nom_val}\)"
    done
done

# vary T
use_prior=0
base_name="vary_T_"
use_prior_policy=0
for (( i=0; i<${#T[@]}; i++)) 
do
    for (( j=1; j<=$N; j++)) 
    do
        echo ${T[i]}
        eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${T_name[i]}${process}${j}\',${use_prior_policy},\${nom_val},\${T[i]}\)"
    done
done

# vary T -- use_prior
use_prior=1
base_name="vary_T_prior_"
use_prior_policy=1
for (( i=0; i<${#T[@]}; i++)) 
do
    for (( j=1; j<=$N; j++)) 
    do
        echo ${T[i]}
        eval "matlab -nodesktop -nosplash -r cartPole_learn_fun\(${set_random},\${use_prior},\'${base_name}${T_name[i]}${process}${j}\',${use_prior_policy},\${nom_val},\${T[i]}\)"
    done
done

