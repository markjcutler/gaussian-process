#!/usr/bin/env python

###################################################
# plot.py -- Load mat files and create pretty plots
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 18 September 2014.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/cartPole/cartPole_sim2sim'
data = scipy.io.loadmat(path)
nom_mean = data['m'][0]
trans_mean = data['m'][1]
full_mean = data['m'][2]
policy_mean = data['m'][3]

nom_std = data['s'][0]
trans_std = data['s'][1]
full_std = data['s'][2]
policy_std = data['s'][3]

nom_stderr = data['sem'][0]
trans_stderr = data['sem'][1]
full_stderr = data['sem'][2]
policy_stderr = data['sem'][3]

c = ['b', 'r', 'g', 'm']
alpha = 0.28
linestyle = ['-', '--', '-.', ':']
legend = [r'Nominal', r'Prior Policy', r'Prior Transition',
          r'Prior Transition and Policy']
legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
fig, ax = plt.subplots()

handle = ax.plot(range(len(nom_mean)), nom_mean, color=c[0],
                 linestyle=linestyle[0], #label=legend[i],
                 linewidth=linewidth)
ax.fill_between(range(len(nom_mean)), nom_mean - 1.96*nom_stderr,
                nom_mean + 1.96*nom_stderr, alpha=alpha,
                facecolor=c[0], edgecolor=c[0])
handle = ax.plot(range(len(policy_mean)), policy_mean, color=c[1],
                 linestyle=linestyle[1], #label=legend[i],
                 linewidth=linewidth)
ax.fill_between(range(len(policy_mean)), policy_mean - 1.96*policy_stderr,
                policy_mean + 1.96*policy_stderr, alpha=alpha,
                facecolor=c[1], edgecolor=c[1])
handle = ax.plot(range(len(trans_mean)), trans_mean, color=c[2],
                 linestyle=linestyle[2], #label=legend[i],
                 linewidth=linewidth)
ax.fill_between(range(len(trans_mean)), trans_mean - 1.96*trans_stderr,
                trans_mean + 1.96*trans_stderr, alpha=alpha,
                facecolor=c[2], edgecolor=c[2])
handle = ax.plot(range(len(full_mean)), full_mean, color=c[3],
                 linestyle=linestyle[3], #label=legend[i],
                 linewidth=linewidth)
ax.fill_between(range(len(full_mean)), full_mean - 1.96*full_stderr,
                full_mean + 1.96*full_stderr, alpha=alpha,
                facecolor=c[3], edgecolor=c[3])

p1 = plt.Rectangle((0, 0), 1, 1, fc=c[0], ec=c[0], alpha=alpha)
p2 = plt.Rectangle((0, 0), 1, 1, fc=c[1], ec=c[1], alpha=alpha)
p3 = plt.Rectangle((0, 0), 1, 1, fc=c[2], ec=c[2], alpha=alpha)
p4 = plt.Rectangle((0, 0), 1, 1, fc=c[3], ec=c[3], alpha=alpha)
ax.legend([p1, p2, p3, p4], [legend[0], legend[1], legend[2], legend[3]],
          loc='upper right', fancybox=True, fontsize=legend_fontsize)

#ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
ax.set_ylabel('Cost', size=axis_label_fontsize)
ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + '.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')
