#!/usr/bin/env python

###################################################
# plot_simdiff.py -- Load mat files and create plots.
# Simulator to sim with different parameter settings.
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Tuesday, 23 September 2014.
###################################################

import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import stats


def parse_data(param, veh, num_par_tracks, num_iterations,
               num_episodes, base_name):
    path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/cartPole/sim2diffsim/veh'

    reward = np.zeros((len(param), len(veh)*num_par_tracks*num_iterations,
                       num_episodes+1))

    for p in range(len(param)):
        cnt = 0
        for v in veh:
            for i in range(num_par_tracks):
                for j in range(num_iterations):
                    # open the last episode since it will have reward information
                    # for all the episodes before it
                    if len(param) == 1:
                        matfile = scipy.io.loadmat(path + str(v) + base_name + str(i+1) +
                                                   str(j+1) + str(num_episodes) + '_H40')
                    else:
                        matfile = scipy.io.loadmat(path + str(v) + base_name + param[p] + str(i+1) +
                                                   str(j+1) + str(num_episodes) + '_H40')
                    cost = matfile['realCost'][0]
                    for k in range(num_episodes+1):
                        reward[p, cnt, k] = np.sum(cost[k][0][-cost_steps:]*exp_weighting)
                    cnt += 1

    mean_cost = np.mean(reward, axis=1)
    std_cost = np.mean(reward, axis=1)
    stderr_cost = stats.sem(reward, axis=1)

    return mean_cost, std_cost, stderr_cost

# loop over vehicles
# loop over instances
# insert norm of episodes into array
# plt.ion()
veh = np.array([2, 3, 4])
num_par_tracks = 4
num_iterations = 2
num_episodes = 7
time_steps = 40
sample_rate = 10
cost_steps = 25
exp_weighting = np.exp(np.arange(cost_steps)/float(sample_rate))
exp_weighting = np.ones(cost_steps)

vary_b = ['0', '2', '5', '10', '15']
vary_T = ['0p7', '0p8', '0p9', '1p3', '1p5', '2p0']

nom_mean, nom_std, nom_stderr = parse_data([1], veh, num_par_tracks, num_iterations,
                                           num_episodes, '/nom_param_')
b_mean, b_std, b_stderr = parse_data(vary_b, veh, num_par_tracks, num_iterations,
                                     num_episodes, '/vary_b_')
T_mean, T_std, T_stderr = parse_data(vary_T, veh, num_par_tracks, num_iterations,
                                     num_episodes, '/vary_T_')
nom_mean_prior, nom_std_prior, nom_stderr_prior = parse_data([1], veh, num_par_tracks, num_iterations,
                                           num_episodes, '/nom_param_prior_')
b_mean_prior, b_std_prior, b_stderr_prior = parse_data(vary_b, veh, num_par_tracks, num_iterations,
                                     num_episodes, '/vary_b_prior_')
T_mean_prior, T_std_prior, T_stderr_prior = parse_data(vary_T, veh, num_par_tracks, num_iterations,
                                     num_episodes, '/vary_T_prior_')

print nom_mean
print b_mean
print T_mean

c = ['b', 'r', 'g', 'm']
alpha = 0.28
linestyle = ['-', '--', '-.', ':']
legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
fig, ax = plt.subplots()


path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/cartPole/cp_'
color = ['red', 'green', 'blue', 'magenta', 'cyan', 'black', 'yellow']


fig, ax = plt.subplots()
figs = [0, 1, 3, 4]
legend = ['0%', '200%', '1000%', '1500%']
cnt = 0
for i in figs:
    handle = ax.plot(range(num_episodes+1), b_mean[i]-b_mean_prior[i],
                     color=color[cnt], label=legend[cnt], linewidth=3.0,
                     linestyle=linestyle[cnt])
    cnt += 1
handle = ax.plot(range(num_episodes+1), np.zeros(num_episodes+1), color='k',
                 linewidth=2.0, linestyle='dashed')

ax.legend(loc='best', prop={'size':20}, fancybox=True)
ax.set_ylabel('Cost Difference', size=axis_label_fontsize)
ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + 'vary_b.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')


fig, ax = plt.subplots()
figs = [0, 2, 3, 4]
legend = ['75%', '90%', '130%', '150%']
cnt = 0
for i in figs:
    handle = ax.plot(range(num_episodes+1), T_mean[i]-T_mean_prior[i],
                     color=color[cnt], label=legend[cnt], linewidth=3.0,
                     linestyle=linestyle[cnt])
    cnt += 1
handle = ax.plot(range(num_episodes+1), np.zeros(num_episodes+1), color='k',
                 linewidth=2.0, linestyle='dashed')

ax.legend(loc='best', prop={'size':20}, fancybox=True)
ax.set_ylabel('Cost Difference', size=axis_label_fontsize)
ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + 'vary_T.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')


# plt.figure(1)
# plt.clf()
# # plt.errorbar(range(num_episodes+1), nom_mean[0], yerr=1.96*nom_stderr[0])
# # plt.errorbar(range(num_episodes+1), nom_mean_prior[0], yerr=1.96*b_stderr[0])
# color = ['red', 'green', 'blue', 'magenta', 'cyan', 'black', 'yellow']
# fig_start = 0
# fig_end = 6
# for i in range(fig_start, fig_end, 2):
#     plt.plot(range(num_episodes+1), I_mean[i], color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), I_mean[i], yerr=1.96*I_stderr[i],
#     #             linestyle="None", marker="None", color=color[fig_start+i])
#     plt.plot(range(num_episodes+1), I_mean_prior[i], linestyle="dashed",
#              color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), I_mean_prior[i], yerr=1.96*I_stderr_prior[i],
#      #            linestyle="None", marker="None", color=color[fig_start+i])

# plt.figure(2)
# plt.clf()
# fig_start = 0
# fig_end = 6
# for i in range(fig_start, fig_end,2):
#     plt.plot(range(num_episodes+1), T_mean[i], color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), T_mean[i], yerr=1.96*T_stderr[i],
#      #            linestyle="None", marker="None", color=color[fig_start+i])
#     plt.plot(range(num_episodes+1), T_mean_prior[i], linestyle="dashed",
#              color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), T_mean_prior[i], yerr=1.96*T_stderr_prior[i],
#      #            linestyle="None", marker="None", color=color[fig_start+i])


# plt.figure(3)
# plt.clf()
# fig_start = 0
# fig_end = 5
# for i in range(fig_start, fig_end,2):
#     plt.plot(range(num_episodes+1), b_mean[i], color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), b_mean[i], yerr=1.96*b_stderr[i],
#     #             linestyle="None", marker="None", color=color[fig_start+i])
#     plt.plot(range(num_episodes+1), b_mean_prior[i], linestyle="dashed",
#              color=color[fig_start+i])
#     #plt.errorbar(range(num_episodes+1), b_mean_prior[i], yerr=1.96*b_stderr_prior[i],
#     #             linestyle="None", marker="None", color=color[fig_start+i])



# # plt.plot(range(num_episodes+1), I_mean[1], marker="o", color="green")
# # plt.errorbar(range(num_episodes+1), I_mean[1], yerr=1.96*I_stderr[1],
# #              linestyle="None", marker="None", color="green")
# # plt.plot(range(num_episodes+1), I_mean_prior[1], marker="o", linestyle="dashed",
# #          color="green")
# # plt.errorbar(range(num_episodes+1), I_mean_prior[1], yerr=1.96*I_stderr_prior[1],
# #              linestyle="None", marker="None", color="green")

# # plt.plot(range(num_episodes+1), I_mean[2], marker="o", color="blue")
# # plt.errorbar(range(num_episodes+1), I_mean[2], yerr=1.96*I_stderr[2],
# #              linestyle="None", marker="None", color="blue")
# # plt.plot(range(num_episodes+1), I_mean_prior[2], marker="o", linestyle="dashed",
# #          color="blue")
# # plt.errorbar(range(num_episodes+1), I_mean_prior[2], yerr=1.96*I_stderr_prior[2],
# #              linestyle="None", marker="None", color="blue")

# plt.show()
