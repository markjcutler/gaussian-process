%% pendulum_learn.m
% *Summary:* Script to learn a controller for the pendulum swingup
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% restoredefaultpath; clear all; close all; clc;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

% rng shuffle;
rng('default');
rng shuffle;
% rng(12);
settings_pendulum;            % load scenario-specific settings
basename = 'pendulum_';       % filename used for saving data

%% test derivatives using prior information
use_prior = 0;
use_real_world_data = 1;
num_runs = 100;
max_diff = 0;
deriv='dVds' %'dMdm'
D=1;E=1;
% D=3;E=2;

for rr=1:num_runs
    % main gp
    nn=50; %250;
    dynmodel.inputs=randn(nn,D);
    dynmodel.targets=randn(nn,E);
    dynmodel.fnc=@gp0d;
    dynmodel.hyp = [randn(D+1,E); zeros(1,E)];
    
    % prior gp
    if use_prior
        
        nn=40; %350;
        dynmodel.prior.fnc=@gp0d;
        dynmodel.prior.hyp = [randn(D+1,E); zeros(1,E)];
        %             dynmodel.prior.hyp = dynmodel.hyp;%[randn(D+1,E); zeros(1,E)];
        dynmodel.prior.hyp(D+1,:) = randn(1,E);
        dynmodel.prior.inputs=randn(nn,D);
        dynmodel.prior.targets=randn(nn,E);
    end
    
    % 'real-world' data
    if use_real_world_data
        nn=60;
        dynmodel.gpmodel_rw.fnc=@gp0d;
        dynmodel.gpmodel_rw.hyp = [randn(D+1,E); zeros(1,E)];
        dynmodel.gpmodel_rw.hyp(D+1,:) = randn(1,E);
        dynmodel.gpmodel_rw.inputs=randn(nn,D);
        dynmodel.gpmodel_rw.targets=randn(nn,E);
        dynmodel.p_rw = 0.2;
    end
    
    [dd, dy, dh] = gpT(deriv,dynmodel);
    if dd > max_diff
        max_diff = dd;
    end
end
max_diff

return
