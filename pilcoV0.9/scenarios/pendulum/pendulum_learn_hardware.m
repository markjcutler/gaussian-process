%% pendulum_learn.m
% *Summary:* Script to learn a controller for the pendulum swingup
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
clear all; close all; %restoredefaultpath; 

% clc;
% temporaryBreakpointData=dbstatus('-completenames'); 
% clear functions; 
% dbstop(temporaryBreakpointData); 
% clear global; 
% clear variables;

% rng shuffle;
rng(1);
settings_pendulum;            % load scenario-specific settings
basename = 'pendulum_';       % filename used for saving data
use_prior = false;

plant.noise = diag([0.01^2 0.001^2]);     % measurement noise
H = H*2;

% prior_info = load('pendulum_prior.mat');
% policy.p.inputs = prior_info.policy.p.inputs;
% policy.p.targets = prior_info.policy.p.targets;
% policy.p.hyp = prior_info.policy.p.hyp;

%% load and use prior information
if use_prior
    prior_info = load('pendulum_prior.mat');

    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
end
%%

% 2. Initial J random rollouts
for jj = 1:J
    if isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            hardware_rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            hardware_rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
  x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
  if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
  end
end
return

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
  trainDynModel;   % train (GP) dynamics model
  learnPolicy;     % learn policy
  applyController_hardware; % apply controller to system
  disp(['controlled trial # ' num2str(j)]);
  if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
  end
end