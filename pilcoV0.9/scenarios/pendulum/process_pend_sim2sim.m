% process_pend_sim2sim.m

clc; clear all;

path = '/home/mark/icra2015_pilco_data/pendulum/sim2sim/veh';

H = 40;
rate = 10;
cost_steps = 25;
num_learning_tries = 20;
num_repeat = 5;
veh = 2;
num_par_tracks = 3;
num_iterations=12;
num_episodes = 7;
type = {'/nom_', '/prior_trans_', '/prior_full_', '/prior_policy_'};


cost = zeros(length(type), num_repeat*num_learning_tries, num_episodes+1);

for ii=1:length(type)
    learning_try=0;
    cnt=1;
    for i=1:num_par_tracks
        if learning_try >= num_learning_tries
            break
        end
        for j=1:num_iterations
            if learning_try >= num_learning_tries
                break
            end
            for k=1:num_repeat
                for l=0:num_episodes
                    data = load([path num2str(veh) type{ii} num2str(i) num2str(j) num2str(l) '_H40']);
%                     data.policy.p.targets
%                     data.realCost
                    [xx, yy, realCost, latent] = ...
                        rollout(gaussian(data.mu0, data.S0), data.policy, data.H, data.plant, data.cost);
%                     realCost
                    cost(ii, cnt, l+1) = sum(realCost(end-cost_steps+1:end));
                end
                cnt = cnt + 1
            end
            learning_try = learning_try+1
        end
    end
end

m = zeros(length(type),num_episodes+1);
s = zeros(length(type),num_episodes+1);
sem = zeros(length(type),num_episodes+1);
for i=1:length(type)
    m(i,:) = reshape(mean(cost(i,:,:)),1,num_episodes+1);
    s(i,:) = reshape(std(cost(i,:,:)),1,num_episodes+1);
    sem(i,:) = reshape(std(cost(i,:,:))/sqrt(num_repeat*num_learning_tries),1,num_episodes+1);
end
m
s
sem
% figure(1);clf;
% for i=1:4
%     plot(m(i,:));
%     hold all
% end
save 'pend_sim2sim.mat' cost m s sem
                 