%% pendulum_learn.m
% *Summary:* Script to learn a controller for the pendulum swingup
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
clear all; close all; %restoredefaultpath; 

% clc;
% temporaryBreakpointData=dbstatus('-completenames'); 
% clear functions; 
% dbstop(temporaryBreakpointData); 
% clear global; 
% clear variables;

rng shuffle;
% rng(1);
settings_pendulum;            % load scenario-specific settings
basename = 'pendulum_';       % filename used for saving data
use_prior = false;
new_random_rollout = true;
% plant.noise = diag([0.001^2 0.0001^2]);     % measurement noise

plant.noise = diag([0.01^2 0.001^2]);     % measurement noise
% H = H*2;

if use_prior
    prior_info = load('pendulum_prior_H60_small_b');
else
    prior_info = load('pendulum_prior.mat');
end

% set policy to one from prior information
policy.p.inputs = prior_info.policy.p.inputs;
policy.p.targets = prior_info.policy.p.targets;
policy.p.hyp = prior_info.policy.p.hyp;

%% load and use prior information
if use_prior
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
end
%%

% 2. Initial random rollout
% load rollout or create new one
jj = 1;

if new_random_rollout
    save_gp_params(policy, H, dt, true);

    if use_prior
        save_gp_params(policy, H, dt, true);
    end
    
    pause
end

load('rollout');
% xx(:,1) = xx(:,1)/10;
% add noise to xx and yy
for iii=1:H
    xx(iii,1:2) = xx(iii,1:2) + randn(1,2)*chol(plant.noise);
    xx(iii,3) = sin(xx(iii,2));
    xx(iii,4) = cos(xx(iii,2));
    if iii>1
        yy(iii-1,:) = xx(iii,1:2);
    end
end
yy(end,:) = yy(end,:) + randn(1,2)*chol(plant.noise);

% pull out every 5th x
% xxx = zeros(40,5); yyy = zeros(40,2);
% cntxxx = 1;
% for i=1:length(xx)
%     if mod(i,5) == 1
%         xxx(cntxxx, :) = xx(i,:);
%         yyy(cntxxx, :) = yy(i,:);
%         cntxxx = cntxxx + 1;
%     end
% end
% x = [x; xxx]; y = [y; yyy];
x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
latent1 = zeros(length(xx)+1, 3);
latent1(1:end-1,1:2) = xx(:,1:2);
latent1(1:end-1,3) = xx(:,end);
latent1(end,1:2) = yy(end, :);
latent{jj} = latent1;
if plotting.verbosity > 0;      % visualization of trajectory
if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
end
% return


%% reset random policy params
if ~use_prior
    policy.p.inputs = gaussian(mm(poli), ss(poli,poli), nc)'; % init. location of 
                                                              % basis functions
    policy.p.targets = 0.1*randn(nc, length(policy.maxU));    % init. policy targets 
                                                              % (close to zero)
    policy.p.hyp = log([1 0.7 0.7 1 0.01]');                  % initialize 
                                                              % hyper-parameters
end

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
  trainDynModel;   % train (GP) dynamics model
  learnPolicy;     % learn policy
  applyController_hardware_cpp; % apply controller to system
  disp(['controlled trial # ' num2str(j)]);
  if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
  end
end