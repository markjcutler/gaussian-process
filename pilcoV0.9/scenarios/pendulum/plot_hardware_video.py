#!/usr/bin/env python

###################################################
# plot_hardware.py -- Plot hardware rollouts for ICRA2015 paper
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Wednesday,  1 October 2014.
###################################################

import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/hardware_data'
matfile = scipy.io.loadmat(path)

m_nom = matfile['m_nom'][0]
s_nom = matfile['s_nom'][0]
sem_nom = matfile['sem_nom'][0]

m_prior = matfile['m_prior'][0]
s_prior = matfile['s_prior'][0]
sem_prior = matfile['sem_prior'][0]

c = ['b', 'r']
alpha = 0.28
linestyle = ['-', '-', '-.', ':']
legend = [r'Simulator Prior', r'Zero Prior']
legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 4.5
axis_tick_fontsize = 50
annotation_fontsize = 20
markersize = 16
fig, ax = plt.subplots()

num_points = 6

handle = ax.plot(range(num_points), m_prior[0:num_points], color=[145.0/255, 1, 118.0/255],
                 linestyle=linestyle[0], marker='o',  markersize=markersize,#label=legend[i],
                 linewidth=linewidth)
handle = ax.plot(range(num_points), m_nom[0:num_points], color=[118.0/255, 145.0/255, 1],
                 linestyle=linestyle[1], marker='o',  markersize=markersize, #label=legend[i],
                 linewidth=linewidth)


#ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
#ax.set_ylabel('Cost', size=axis_label_fontsize)
#ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
plt.xlim(xmax=5.5)
plt.ylim(ymax=125, ymin=20)
ax.set_aspect(0.03)

plt.savefig(path + str(num_points) + '.png',
                 format='png', transparent=True, bbox_inches='tight')
