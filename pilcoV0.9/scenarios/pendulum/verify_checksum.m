function valid = verify_checksum(data, checksum)

ch = sum(data);
while ch > 255
    ch = ch-256;
end

if ch + checksum == 256
    valid = 1;
else
    valid = 0;
end