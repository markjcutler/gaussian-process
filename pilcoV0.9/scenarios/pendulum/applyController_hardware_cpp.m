%% applyController.m
% *Summary:* Script to apply the learned controller to a (simulated) system
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-06-04
%
%% High-Level Steps
% # Generate a single trajectory rollout by applying the controller
% # Generate many rollouts for testing the performance of the controller
% # Save the data

%% Code

% 1. Generate trajectory rollout given the current policy
if isfield(plant,'constraint'), HH = maxH; else HH = H; end

% save w, b, H, rate
save_gp_params(policy, H, dt, false);
% w = policy.p.w;
% b = policy.p.b;
% random = false;
% save('params','w','b','H','dt','random');
% clear w
% clear b
% clear random

pause

% load data
load('rollout');
% x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
for iii=1:H
    xx(iii,1:2) = xx(iii,1:2) + randn(1,2)*chol(plant.noise);
    xx(iii,3) = sin(xx(iii,2));
    xx(iii,4) = cos(xx(iii,2));
    if iii>1
        yy(iii-1,:) = xx(iii,1:2);
    end
end
yy(end,:) = yy(end,:) + randn(1,2)*chol(plant.noise);

% pull out every 5th x
% xxx = zeros(40,5); yyy = zeros(40,2);
% cntxxx = 1;
% for i=1:length(xx)
%     if mod(i,5) == 1
%         xxx(cntxxx, :) = xx(i,:);
%         yyy(cntxxx, :) = yy(i,:);
%         cntxxx = cntxxx + 1;
%     end
% end
% x = [x; xxx]; y = [y; yyy];

latent1 = zeros(length(xx)+1, 3);
latent1(1:end-1,1:2) = xx(:,1:2);
latent1(1:end-1,3) = xx(:,end);
latent1(end,1:2) = yy(end, :);
latent{j} = latent1;

% [xx, yy, realCost{j+J}, latent{j}] = ...
%   hardware_rollout(gaussian(mu0, S0), policy, HH, plant, cost);

x = [x; xx]; y = [y; yy];                            % augment training set
if plotting.verbosity > 0
  if ~ishandle(3); figure(3); else set(0,'CurrentFigure',3); end
  hold on; plot(1:length(realCost{J+j}),realCost{J+j},'r'); drawnow;
end

% 3. Save data
filename = [basename num2str(j) '_H' num2str(H)]; save(filename);
