function [x y L latent] = hardware_rollout(start, policy, H, plant, cost)



if isfield(plant,'augment'), augi = plant.augi;             % sort out indices!
else plant.augment = inline('[]'); augi = []; end
if isfield(plant,'subplant'), subi = plant.subi;
else plant.subplant = inline('[]',1); subi = []; end
odei = plant.odei; poli = plant.poli; dyno = plant.dyno; angi = plant.angi;
simi = sort([odei subi]);
nX = length(simi)+length(augi); nU = length(policy.maxU); nA = length(angi);

rollout_cnt = 0;
while 1
    if rollout_cnt > 0
        pause
    end
    %% set up hardware
    % open serial port
    seen_bad_checksum = false;
    serial_port = serial('/dev/ttyUSB0','BaudRate',115200);
    fopen(serial_port);

    % do a few reads to make sure things are working and to get the initial
    % state
    for i=1:20
        [start, ch] = hardware_single_step(serial_port, 0, 1);
        if ~ch
            seen_bad_checksum = true;
        end
    end
    % tic
    state(simi) = start; state(augi) = plant.augment(state);      % initializations
    x = zeros(H+1, nX+2*nA);
    x(1,simi) = start' + randn(size(simi))*chol(plant.noise); % no need to add noise to hardware
    x(1,augi) = plant.augment(x(1,:));
    u = zeros(H, nU); latent = zeros(H+1, size(state,2)+nU);
    y = zeros(H, nX); L = zeros(1, H); next = zeros(1,length(simi));


    %% Code
    for i = 1:H % --------------------------------------------- generate trajectory
      s = x(i,dyno)'; sa = gTrig(s, zeros(length(s)), angi); s = [s; sa];
      x(i,end-2*nA+1:end) = s(end-2*nA+1:end);

      % 1. Apply policy ... or random actions --------------------------------------
      if isfield(policy, 'fcn')
        u(i,:) = policy.fcn(policy,s(poli),zeros(length(poli)));
      else
        u(i,:) = policy.maxU.*(2*rand(1,nU)-1);
      end
    %   u(i,:) = 2.0;
      latent(i,:) = [state u(i,:)];                                  % latent state
      
%       controller
      kp = 3.0;
      kd = 0.8;
      theta = state(2);
      theta_dot = state(1);
      u(i,:) = kp*wrapToPi(pi - theta) - kd*theta_dot;
%       u(i,:) = -u(i,:);

      % 2. Simulate dynamics -------------------------------------------------------
      %% Call hardware here
    %   toc
    %   tic
      [next(odei), ch] = hardware_single_step(serial_port, u(i,:), 1);
      if ~ch
        seen_bad_checksum = true;
      end
      %next(odei) = simulate(state(odei), u(i,:), plant);
      %next(subi) = plant.subplant(state, u(i,:));

      % 3. Stop rollout if constraints violated ------------------------------------
      if isfield(plant,'constraint') && plant.constraint(next(odei))
        H = i-1;
        fprintf('state constraints violated...\n');
        break;
      end

      % 4. Augment state and randomize ---------------------------------------------
      state(simi) = next(simi); state(augi) = plant.augment(state);
      x(i+1,simi) = state(simi) + randn(size(simi))*chol(plant.noise);
      x(i+1,augi) = plant.augment(x(i+1,:));

      % 5. Compute Cost ------------------------------------------------------------
      if nargout > 2
        L(i) = cost.fcn(cost,state(dyno)',zeros(length(dyno)));
      end
    end

    %% shut down the hardware
    for i=1:10
        hardware_single_step(serial_port, 0, 0);
    end
    % close serial port
    fclose(serial_port);
    
    if ~seen_bad_checksum
        break
    end
    rollout_cnt = rollout_cnt + 1;
end

y = x(2:H+1,1:nX); x = [x(1:H,:) u(1:H,:)]; 
latent(H+1, 1:nX) = state; latent = latent(1:H+1,:); L = L(1,1:H);
