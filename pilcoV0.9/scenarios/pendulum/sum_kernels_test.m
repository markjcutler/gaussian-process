%% pendulum_learn.m
% *Summary:* Script to learn a controller for the pendulum swingup
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code


% 1. Initialization
% restoredefaultpath; clear all; close all; clc;

clc;
temporaryBreakpointData=dbstatus('-completenames'); 
clear functions; 
dbstop(temporaryBreakpointData); 
clear global; 
clear variables;

% rng shuffle;
rng('default');
rng shuffle;
% rng(1);
settings_pendulum;            % load scenario-specific settings

%% test derivatives using prior information
use_prior = 0;
use_real_world_data = 0;
D=1;E=1; 
% D=3;E=2;

% main gp
nn=50; %250;
dynmodel.inputs=randn(nn,D);
dynmodel.targets=randn(nn,E);
dynmodel.fnc=@gp0d;
dynmodel.hyp = [randn(D+1,E); zeros(1,E)];

%% plots

% plot dynamics model
figure(5);clf;
hold all
u_prime = (-8:0.1:8)';
mean = zeros(size(u_prime));
var = zeros(size(u_prime));
inoutvar = zeros(size(u_prime));
for kk=1:length(u_prime)
    [mean(kk), var(kk), inoutvar(kk)] = gp0(dynmodel, u_prime(kk), 0);
end
f = [mean+2*sqrt(var + exp(2*dynmodel.hyp(3))); flipdim(mean-2*sqrt(var + exp(2*dynmodel.hyp(3))),1)];
fill([u_prime; flipdim(u_prime,1)], f, [7 7 7]/8)
hold on; plot(u_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs, dynmodel.targets, '+', 'MarkerSize', 12)
plot(dynmodel.inputs(1), dynmodel.targets(1), 'ko', 'MarkerSize', 20)
grid on
xlabel('input, x')
ylabel('output, y')
drawnow;

% plot the first data point covariance

% get usuable versions of hyperparameters
log_length_scales = dynmodel.hyp(1:D,:);
length_scales_squared = exp(2*log_length_scales);
log_signal_sigma = dynmodel.hyp(D+1,:);
signal_var = exp(2*log_signal_sigma);
noise_var = exp(2*dynmodel.hyp(D+2,:));

cov = zeros(size(u_prime));
for i=1:length(u_prime)
    for j=1:nn
        cov(i) = cov(i) + signal_var*exp(1/(-2*length_scales_squared)*...
            (u_prime(i) - dynmodel.inputs(j))^2);
    end
end
figure(4); clf;
plot(u_prime, cov)

