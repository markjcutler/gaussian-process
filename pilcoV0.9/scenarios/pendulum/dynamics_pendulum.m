%% dynamics_pendulum.m
% *Summary:* Implements ths ODE for simulating the pendulum dynamics, where 
% an input torque f can be applied  
%
%    function dz = dynamics_pendulum(t,z,u)
%
%
% *Input arguments:*
%
%		t     current time step (called from ODE solver)
%   z     state                                                    [2 x 1]
%   u     (optional): torque f(t) applied to pendulum
%
% *Output arguments:*
%   
%   dz    if 3 input arguments:      state derivative wrt time
%
%   Note: It is assumed that the state variables are of the following order:
%         dtheta:  [rad/s] angular velocity of pendulum
%         theta:   [rad]   angle of pendulum
%
% A detailed derivation of the dynamics can be found in:
%
% M.P. Deisenroth: 
% Efficient Reinforcement Learning Using Gaussian Processes, Appendix C, 
% KIT Scientific Publishing, 2010.
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-18

function dz = dynamics_pendulum(t,z,u)
%% warning -- not a great idea to use global variables here, but I'm doing it anyway
global b_modify_mark_global T_modify_mark_global I_modify_mark_global
if isempty(b_modify_mark_global)
    b_modify_mark_global = 1;
    T_modify_mark_global = 1;
    I_modify_mark_global = 1;
end

%% Code

l = 1;    % [m]        length of pendulum
m = 1;    % [kg]       mass of pendulum
g = 9.82; % [m/s^2]    acceleration of gravity
b = b_modify_mark_global*0.01; % [s*Nm/rad] friction coefficient

dtheta = z(1);
theta = z(2);

% 2.5 is absolute value of max torque
% u(t)
% T_modify_mark_global = 0.11;
torque = T_modify_mark_global*u(t);
inertia = I_modify_mark_global*m*l^2/3;
gravity_force = m*g*l*sin(theta)/2;

%% hardware values
% inertia = 0.0148;
% gravity_force = 0.5298*sin(theta);
% torque = 0.23*u(t)/2.5; %% WARNING, assuming maxU = 2.5 here;
% b = 0.0;

dz = zeros(2,1);
dz(1) = ( torque - b*z(1) - gravity_force ) / inertia;
dz(2) = dtheta;
