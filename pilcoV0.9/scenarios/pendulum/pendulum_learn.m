%% pendulum_learn.m
% *Summary:* Script to learn a controller for the pendulum swingup
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% restoredefaultpath; clear all; close all; 

clc;
temporaryBreakpointData=dbstatus('-completenames'); 
clear functions; 
dbstop(temporaryBreakpointData); 
clear global; 
clear variables;

% rng shuffle;
rng(1);
settings_pendulum;            % load scenario-specific settings
basename = 'pendulum_';       % filename used for saving data
use_prior = false;


% prior_info = load('pendulum_prior_linear.mat');
% policy.p.w = prior_info.policy.p.w;
% policy.p.b = prior_info.policy.p.b;

% prior_info = load('pendulum_prior.mat');
% policy.p.inputs = prior_info.policy.p.inputs;
% policy.p.targets = prior_info.policy.p.targets;
% policy.p.hyp = prior_info.policy.p.hyp;

%% load and use prior information
if use_prior
    prior_info = load('pendulum_prior.mat');

    % set policy to one from prior information
    policy.p.inputs = prior_info.policy.p.inputs;
    policy.p.targets = prior_info.policy.p.targets;
    policy.p.hyp = prior_info.policy.p.hyp;
    % set prior gp as prior in dynmodel struct
    dynmodel.prior = prior_info.dynmodel;
    
    
%     %% save gp params
%     D = 3; n=20;
%     inp = bsxfun(@rdivide,policy.p.inputs,exp(policy.p.hyp(1:D)'));
%     K = exp(2*policy.p.hyp(D+1)-maha(inp,inp)/2);
%     if isfield(dynmodel,'nigp')
%       L = chol(K + exp(2*policy.p.hyp(D+2))*eye(n) + diag(gpmodel.nigp(:)))';
%     else
%       L = chol(K + exp(2*policy.p.hyp(D+2))*eye(n))';
%     end
%     beta = L'\(L\policy.p.targets(:));
%     inputs = policy.p.inputs;
%     iL = diag(exp(-policy.p.hyp(1:D))); % inverse length-scales
%     iL2 = diag(iL*iL);
%     random=false;
%     m = [-0.9;-0.43;0.756];
%     s = zeros(3);
%     [M, S, V] = gp2(policy.p, m, s)
%     save('params','inputs','beta','iL2','H','dt','random');
%     clear inputs;
    %% test derivatives using prior information
    use_my_data=1;
    use_prior = 1;
    deriv='dSdm'
    if use_my_data
%         D=1;E=1; 
        D=4;E=2;

        % main gp
        nn=50; %250;
        dynmodel.inputs=randn(nn,D);
        dynmodel.targets=randn(nn,E);
        dynmodel.fnc=@gp0d;
        dynmodel.hyp = [randn(D+1,E); zeros(1,E)];
        
        % prior gp
        if use_prior
            nn=40; %350;
            % set prior gp as prior in dynmodel struct
            dynmodel.prior = prior_info.dynmodel;
            dynmodel.prior.fnc=@gp0d;
            dynmodel.prior.hyp = [randn(D+1,E); zeros(1,E)];
%             dynmodel.prior.hyp = dynmodel.hyp;%[randn(D+1,E); zeros(1,E)];
            dynmodel.prior.hyp(D+1,:) = randn(1,E);
            dynmodel.prior.inputs=randn(nn,D);
            dynmodel.prior.targets=randn(nn,E);
        end
        [dd, dy, dh] = gpT(deriv,dynmodel);
    else
        [dd, dy, dh] = gpT(deriv);
    end
    return
end
%%

% 2. Initial J random rollouts
for jj = 1:J
    if isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
%     save('rollout.mat','xx','yy');
    % pull out every 5th x
%     xxx = zeros(40,5); yyy = zeros(40,2);
%     cntxxx = 1;
%     for i=1:length(xx)
%         if mod(i,5) == 1
%             xxx(cntxxx, :) = xx(i,:);
%             yyy(cntxxx, :) = yy(i,:);
%             cntxxx = cntxxx + 1;
%         end
%     end
%     x = [x; xxx]; y = [y; yyy];
xx
yy
  x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
  if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
  end
end

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);
% return
% 3. Controlled learning (N iterations)
for j = 1:N
  trainDynModel;   % train (GP) dynamics model
  learnPolicy;     % learn policy
  applyController; % apply controller to system
  disp(['controlled trial # ' num2str(j)]);
  if plotting.verbosity > 0;      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_pendulum;
  end
end