
temporaryBreakpointData=dbstatus('-completenames'); 
clear functions; 
dbstop(temporaryBreakpointData); 
clear global; 
clear variables;

settings_pendulum;            % load scenario-specific settings
basename = 'pendulum_';       % filename used for saving data


%% load and use prior information
prior_info = load('pendulum_prior.mat');

% set policy to one from prior information
policy.p.inputs = prior_info.policy.p.inputs;
policy.p.targets = prior_info.policy.p.targets;
policy.p.hyp = prior_info.policy.p.hyp;

% set prior gp as prior in dynmodel struct
dynmodel.prior = prior_info.dynmodel;
dynmodel.prior.fnc = @gp0d;

%% need to work on: dSdm, dSds 
use_prior = 1;
% D=1;E=1; 
D=4;E=2;
nn=50; %350;
nnp=40; %250;
samples1 = 3000;
samples2 = 600;
% rand('seed',5); randn('seed',13); 
% rand('seed',5145); randn('seed',13456);

% prior gp
dynmodel.prior.fnc=@gp0d;
dynmodel.prior.hyp = [randn(D+1,E); zeros(1,E)];
dynmodel.prior.inputs=randn(nn,D);
dynmodel.prior.targets=randn(nn,E);

% main gp
if use_prior
    dynmodel.inputs=randn(nnp,D);
    dynmodel.targets=randn(nnp,E);
    dynmodel.fnc=@gp0d;
    dynmodel.hyp = [randn(D+1,E); zeros(1,E)];
end

%% generate samples from input dist
m = randn(D,1); s = randn(D); s = s*s';
input_samples = mvnrnd(m,s,samples1);
total_samples = zeros(samples1*samples2, E);
% for i=1:samples1
%     if use_prior
%        [M, S, V] = gp0(dynmodel,input_samples(i,:)',zeros(D)); %, prior_info.dynmodel, m, s, delta)
%     else
%        [M, S, V] = gp0(dynmodel.prior,input_samples(i,:)',zeros(D));
%     end
% %     S
%     post_samples = mvnrnd(M,S,samples2);
%     total_samples((i-1)*samples2+1:i*samples2,:) = post_samples;
% end
emp_mean = mean(total_samples);
emp_cov = cov(total_samples);
% emp_V = cov(in
if use_prior
   [M_true, S_true, V_true] = gp0(dynmodel,m,s); %, prior_info.dynmodel, m, s, delta)
   [M_true2, S_true2, V_true2, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d(dynmodel,m,s);
else
   [M_true, S_true, V_true] = gp0(dynmodel.prior,m,s);
end
emp_mean
M_true'
emp_cov
S_true
M_true2-M_true
S_true2-S_true
V_true2-V_true

