#!/usr/bin/env python

###################################################
# plot.py -- Load mat files and create pretty plots
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 18 September 2014.
###################################################

import matplotlib.pyplot as plt
import scipy.io
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# loop over vehicles
# loop over instances
# insert norm of episodes into array
# plt.ion();
# veh = np.array([2, 3, 4])
# num_par_tracks = 3
# num_iterations = 12 #20
# num_episodes = 7
# time_steps = 40
# sample_rate = 10
# cost_steps = 25
# exp_weighting = np.exp(np.arange(cost_steps)/float(sample_rate))
# exp_weighting = np.ones(cost_steps)

# nom_reward_norm = np.zeros((len(veh)*num_par_tracks*num_iterations,
#                             num_episodes+1))
# trans_reward_norm = np.zeros((len(veh)*num_par_tracks*num_iterations,
#                               num_episodes+1))
# full_reward_norm = np.zeros((len(veh)*num_par_tracks*num_iterations,
#                              num_episodes+1))
# policy_reward_norm = np.zeros((len(veh)*num_par_tracks*num_iterations,
#                                num_episodes+1))

#path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/sim2sim/veh'
# cnt = 0
# for v in veh:
#     for i in range(num_par_tracks):
#         for j in range(num_iterations):
#             # open the last episode since it will have reward information
#             # for all the episodes before it
#             matfile_nom = scipy.io.loadmat(path + str(v) + '/nom_' + str(i+1) +
#                                        str(j+1) + str(num_episodes) + '_H40')
#             cost_nom = matfile_nom['realCost'][0]
#             matfile_trans = scipy.io.loadmat(path + str(v) + '/prior_trans_' +
#                                              str(i+1) + str(j+1) +
#                                              str(num_episodes) + '_H40')
#             cost_trans = matfile_trans['realCost'][0]
#             matfile_full = scipy.io.loadmat(path + str(v) + '/prior_full_' +
#                                              str(i+1) + str(j+1) +
#                                              str(num_episodes) + '_H40')
#             cost_full = matfile_full['realCost'][0]
#             matfile_policy = scipy.io.loadmat(path + str(v) + '/prior_policy_' +
#                                              str(i+1) + str(j+1) +
#                                              str(num_episodes) + '_H40')
#             cost_policy = matfile_policy['realCost'][0]
#             for k in range(num_episodes+1):
#                 nom_reward_norm[cnt, k] = np.sum(cost_nom[k][0][-cost_steps:]*exp_weighting)
#                 trans_reward_norm[cnt, k] = np.sum(cost_trans[k][0][-cost_steps:]*exp_weighting)
#                 full_reward_norm[cnt, k] = np.sum(cost_full[k][0][-cost_steps:]*exp_weighting)
#                 policy_reward_norm[cnt, k] = np.sum(cost_policy[k][0][-cost_steps:]*exp_weighting)
#             cnt += 1
# # print exp_weighting
# # print cost_policy[7][0]
# # print cost_policy[7][0]*exp_weighting
# nom_mean_cost = np.mean(nom_reward_norm, axis=0)
# nom_std_cost = np.std(nom_reward_norm, axis=0)
# nom_stderr_cost = stats.sem(nom_reward_norm, axis=0)
# trans_mean_cost = np.mean(trans_reward_norm, axis=0)
# trans_std_cost = np.std(trans_reward_norm, axis=0)
# trans_stderr_cost = stats.sem(trans_reward_norm, axis=0)
# full_mean_cost = np.mean(full_reward_norm, axis=0)
# full_std_cost = np.std(full_reward_norm, axis=0)
# full_stderr_cost = stats.sem(full_reward_norm, axis=0)
# policy_mean_cost = np.mean(policy_reward_norm, axis=0)
# policy_std_cost = np.std(policy_reward_norm, axis=0)
# policy_stderr_cost = stats.sem(policy_reward_norm, axis=0)
#path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/pend_sim2sim'
path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/cartPole/cartPole_sim2sim'
data = scipy.io.loadmat(path)
nom_mean = data['m'][0]
trans_mean = data['m'][1]
full_mean = data['m'][2]
policy_mean = data['m'][3]

nom_std = data['s'][0]
trans_std = data['s'][1]
full_std = data['s'][2]
policy_std = data['s'][3]

nom_stderr = data['sem'][0]
trans_stderr = data['sem'][1]
full_stderr = data['sem'][2]
policy_stderr = data['sem'][3]



# First illustrate basic pyplot interface, using defaults where possible.
# plt.figure()
# plt.clf()
# plt.errorbar(range(len(nom_mean)), nom_mean, yerr=1.96*nom_stderr)
# plt.errorbar(range(len(trans_mean)), trans_mean, yerr=1.96*trans_stderr)
# plt.errorbar(range(len(full_mean)), full_mean, yerr=1.96*full_stderr)
# plt.errorbar(range(len(policy_mean)), policy_mean, yerr=1.96*policy_stderr)
# plt.xlabel('Episode')
# plt.ylabel('Cost')
# plt.legend(['Nominal', 'Prior Transition', 'Prior Transition and Policy', 'Prior Policy'])
# plt.show()

c = ['b', 'r', 'g', 'm']
alpha = 0.28
linestyle = ['-', '--', '-.', ':']
legend = [r'Nominal', r'Prior Policy', r'Prior Transition',
          r'Prior Transition and Policy']
legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20
fig, ax = plt.subplots()

handle = ax.plot(range(len(nom_mean)), nom_mean, color=c[0],
                 linestyle=linestyle[0], marker='o', label=legend[0],
                 linewidth=linewidth)
ax.fill_between(range(len(nom_mean)), nom_mean - 1.96*nom_stderr,
                nom_mean + 1.96*nom_stderr, alpha=alpha,
                facecolor=c[0], edgecolor=c[0])
handle = ax.plot(range(len(policy_mean)), policy_mean, color=c[1],
                 linestyle=linestyle[1], marker='o', label=legend[1],
                 linewidth=linewidth)
ax.fill_between(range(len(policy_mean)), policy_mean - 1.96*policy_stderr,
                policy_mean + 1.96*policy_stderr, alpha=alpha,
                facecolor=c[1], edgecolor=c[1])
handle = ax.plot(range(len(trans_mean)), trans_mean, color=c[2],
                 linestyle=linestyle[2], marker='o', label=legend[2],
                 linewidth=linewidth)
ax.fill_between(range(len(trans_mean)), trans_mean - 1.96*trans_stderr,
                trans_mean + 1.96*trans_stderr, alpha=alpha,
                facecolor=c[2], edgecolor=c[2])
handle = ax.plot(range(len(full_mean)), full_mean, color=c[3],
                 linestyle=linestyle[3], marker='o', label=legend[3],
                 linewidth=linewidth)
ax.fill_between(range(len(full_mean)), full_mean - 1.96*full_stderr,
                full_mean + 1.96*full_stderr, alpha=alpha,
                facecolor=c[3], edgecolor=c[3])

# p1 = plt.Rectangle((0, 0), 1, 1, fc=c[0], ec=c[0], alpha=alpha)
# p2 = plt.Rectangle((0, 0), 1, 1, fc=c[1], ec=c[1], alpha=alpha)
# p3 = plt.Rectangle((0, 0), 1, 1, fc=c[2], ec=c[2], alpha=alpha)
# p4 = plt.Rectangle((0, 0), 1, 1, fc=c[3], ec=c[3], alpha=alpha)
# ax.legend([p1, p2, p3, p4], [legend[0], legend[1], legend[2], legend[3]],
#           loc='upper right', fancybox=True, fontsize=legend_fontsize)

ax.legend([legend[0], legend[1], legend[2], legend[3]],
          loc='upper right', fancybox=True, fontsize=legend_fontsize)


#ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
ax.set_ylabel('Cost', size=axis_label_fontsize)
ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig(path + '.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')
