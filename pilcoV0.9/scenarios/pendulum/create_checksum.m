function checksum = create_checksum(data)

checksum = sum(data);
while checksum > 255
    checksum = checksum-256;
end
checksum = 256 - checksum;