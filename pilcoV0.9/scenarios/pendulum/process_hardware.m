% process_pend_sim2sim.m

function process_hardware

cost_steps = 1;
num_repeat = 5;
num_episodes = 6;
trial = {'nom_hardware/trial3/','nom_hardware/trial4/','nom_hardware/trial5/'};
% trial = {'nom_hardware/trial4/'};
[m_nom, s_nom, sem_nom] = parse_data(num_episodes, num_repeat, trial, cost_steps);
trial = {'new_hardware/trial6/','new_hardware/trial7/','new_hardware/trial8/'};
% trial = {'new_hardware/trial8/'};
[m_prior, s_prior, sem_prior] = parse_data(num_episodes, num_repeat,trial, cost_steps);

figure(1); clf;
errorbar(m_nom,1.96*sem_nom)
hold all
errorbar(m_prior,1.96*sem_prior)

save 'hardware_data.mat' m_nom s_nom sem_nom m_prior s_prior sem_prior
              

function [m,s,sem] = parse_data(num_episodes, ...
    num_repeat, trials, cost_steps)

path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/';
num_trials = length(trials);
settings_pendulum;
cost_nom = zeros(num_trials*num_repeat, num_episodes);
for t=1:num_trials
    for i=0:num_episodes-1
        for j=0:num_repeat-1
            data = load([path trials{t} 'rollout_full' num2str(i) num2str(j)]);
            xx = data.xx;
            yy = data.yy;
            c = zeros(length(yy), 1);
            for k=1:length(yy)
                c(k) = cost.fcn(cost,yy(k,:),zeros(2));
            end
            cost_nom((t-1)*num_repeat+(j+1),i+1) = sum(c(cost_steps:end));
    %         print matfile
        end
    end
end
% cost_nom
% m = min(cost_nom);
m = mean(cost_nom);
s = std(cost_nom);
sem = std(cost_nom)/sqrt(num_trials*num_repeat);
% figure(1);clf;
% plot(m)

% m = zeros(length(param),num_episodes+1);
% s = zeros(length(param),num_episodes+1);
% sem = zeros(length(param),num_episodes+1);
% for i=1:length(param)
%     m(i,:) = reshape(mean(cost(i,:,:)),1,num_episodes+1);
%     s(i,:) = reshape(std(cost(i,:,:)),1,num_episodes+1);
%     sem(i,:) = reshape(std(cost(i,:,:))/sqrt(num_repeat*num_learning_tries),1,num_episodes+1);
% end
% m
% s
% sem