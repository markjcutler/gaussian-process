% run a bunch of monte-carlo tests

% nominal performance
clear all; close all; restoredefaultpath; clc; 
set_random = false;
use_prior = false;
basename = 'nom_';
use_prior_policy = false;
for i=1:1
    pendulum_learn_fun(set_random,use_prior,[basename num2str(i)],use_prior_policy);
end

% just prior transition data
clear all; close all; restoredefaultpath; clc; 
set_random = false;
use_prior = true;
basename = 'prior_trans_';
use_prior_policy = false;
for i=1:1
    pendulum_learn_fun(set_random,use_prior,[basename num2str(i)],use_prior_policy);
end

% full prior
clear all; close all; restoredefaultpath; clc; 
set_random = false;
use_prior = true;
basename = 'prior_full_';
use_prior_policy = true;
for i=1:1
    pendulum_learn_fun(set_random,use_prior,[basename num2str(i)],use_prior_policy);
end

% just prior policy
clear all; close all; restoredefaultpath; clc; 
set_random = false;
use_prior = false;
basename = 'prior_policy_';
use_prior_policy = true;
for i=1:1
    pendulum_learn_fun(set_random,use_prior,[basename num2str(i)],use_prior_policy);
end