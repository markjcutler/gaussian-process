% pend_hardware_params.m

bar = 0.0303; %kg
pend_and_bar = 0.348; %kg
pend_minus_bar = pend_and_bar - bar;
Lcm = 0.17; %m
T = 5.25/5; % sec
% T = 2*pi*sqrt(Is/(m*g*lcm));
g = 9.81;
Is = pend_minus_bar*g*Lcm*(T/(2*pi))^2 % kg/s^2
gravity_force = Lcm*pend_minus_bar*g % this val times sin(theta)

% torque calculation
input = [0, .25, .5, .75, 1];
angle = [0, .132, 0.26, 0.32, 0.45];

Lf = 0.183; %m, distance to motor
% motor torque = gravity torque
torque = pend_minus_bar*g*sin(angle)*Lcm
figure(1);clf;
plot(input, torque)
% basically linear, so take maxU = torque(end)
torque(end)
