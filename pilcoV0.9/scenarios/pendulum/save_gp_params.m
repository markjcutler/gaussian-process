function save_gp_params(policy, H, dt, random)
%% save gp params
    D = 3; n=20;
    inp = bsxfun(@rdivide,policy.p.inputs,exp(policy.p.hyp(1:D)'));
    K = exp(2*policy.p.hyp(D+1)-maha(inp,inp)/2);

    L = chol(K + exp(2*policy.p.hyp(D+2))*eye(n))';
    beta = L'\(L\policy.p.targets(:));
    inputs = policy.p.inputs;
    iL = diag(exp(-policy.p.hyp(1:D))); % inverse length-scales
    iL2 = diag(iL*iL);
    save('params','inputs','beta','iL2','H','dt','random');
end