#!/usr/bin/env python

###################################################
# plot_hardware.py -- Plot hardware rollouts for ICRA2015 paper
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Wednesday,  1 October 2014.
###################################################

import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

path = '/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/hardware_data'
matfile = scipy.io.loadmat(path)

m_nom = matfile['m_nom'][0]
s_nom = matfile['s_nom'][0]
sem_nom = matfile['sem_nom'][0]

m_prior = matfile['m_prior'][0]
s_prior = matfile['s_prior'][0]
sem_prior = matfile['sem_prior'][0]

c = ['b', 'm']
alpha = 0.28
linestyle = ['-', ':']
legend = [r'Nominal', r'Prior Transition' '\n' r'and Policy']
legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 2.5
axis_tick_fontsize = 18
annotation_fontsize = 20
fig, ax = plt.subplots()

handle = ax.plot(range(len(m_nom)), m_nom, color=c[0],
                 linestyle=linestyle[0], marker='o', label=legend[0],
                 linewidth=linewidth)
ax.fill_between(range(len(m_nom)), m_nom - 1.96*sem_nom,
                m_nom + 1.96*sem_nom, alpha=alpha,
                facecolor=c[0], edgecolor=c[0])
handle = ax.plot(range(len(m_prior)), m_prior, color=c[1],
                 linestyle=linestyle[1], marker='o', label=legend[1],
                 linewidth=linewidth)
ax.fill_between(range(len(m_prior)), m_prior - 1.96*sem_prior,
                m_prior + 1.96*sem_prior, alpha=alpha,
                facecolor=c[1], edgecolor=c[1])
# ax.errorbar(range(len(m_prior)), m_prior, yerr=1.96*sem_prior,
#                 linestyle='None', marker='None', color=c[1], elinewidth=2)
# ax.errorbar(range(len(m_nom)), m_nom, yerr=1.96*sem_nom,
#                 linestyle='None', marker='None', color=c[0], elinewidth=2)

ax.legend([legend[0], legend[1]],
          loc='upper right', fancybox=True, fontsize=legend_fontsize)

# p1 = plt.Rectangle((0, 0), 1, 1, fc=c[0], ec=c[0], alpha=alpha)
# p2 = plt.Rectangle((0, 0), 1, 1, fc=c[1], ec=c[1], alpha=alpha)
# ax.legend([p1, p2], [legend[0], legend[1]],
#           loc='upper right', fancybox=True, fontsize=legend_fontsize)
#

#ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
ax.set_ylabel('Cost', size=axis_label_fontsize)
ax.set_xlabel('Episode', size=axis_label_fontsize)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
plt.xlim(xmax=5.5)

plt.savefig(path + '.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')
