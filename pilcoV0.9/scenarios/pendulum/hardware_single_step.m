function [state, checksum] = hardware_single_step(s, u, attCmd)

    motor_min = 0.25;
    motor_max = 0.75; %0.55;
    % u is -2.5 to 2.5
    m1 = motor_min - u/2.5*(motor_max-motor_min);
    m2 = motor_min + u/2.5*(motor_max-motor_min);
    m1 = sat(m1, motor_max, motor_min);
    m2 = sat(m2, motor_max, motor_min);

    %% construct message
    cmd_data = [255, 254, 10, 3, m1*255.0, m2*255.0, attCmd];
    checksum = create_checksum(uint64(cmd_data(3:end)));
    cmd_data(end+1) = checksum;
%         verify_checksum(cmd_data(3:7),checksum)
    cmd_data = uint8(cmd_data);
    fwrite(s, cmd_data);
    
    while (s.BytesAvailable<11)
    end
    
    % totally hacky way to read the messages -- will only work with the
    % PEND message type
    data = fread(s, s.BytesAvailable);
    if verify_checksum(data(3:10), data(end))
        theta = double(typecast(uint8([data(5) data(6)]), 'int16'))/10000.0; % angle in rad
%         theta_meas = double(typecast(uint8([data(7) data(8)]), 'int16'))/10000.0; % angle in rad
        theta_dot = double(typecast(uint8([data(9) data(10)]), 'int16'))/1000.0; % rate in rad/s
        display(theta_dot);

        state = [theta_dot; theta];
        checksum = 1;
    else
        state = [0;0];
        checksum = 0;
        display('Warning!!! Bad Checksum!!!');
    end
        
end

function out = sat(val, hi, lo)
    if val > hi
        val = hi;
    end
    if val < lo
        val = lo;
    end
    out = val;
end