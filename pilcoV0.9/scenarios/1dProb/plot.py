#!/usr/bin/env python

###################################################
# plot.py -- Load mat files and create pretty plots
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 5 June 2015.
###################################################

import matplotlib.pyplot as plt
import scipy.io
import sys
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
import matplotlib as mpl

axis_tick_fontsize = 21
mpl.rcParams['xtick.labelsize'] = axis_tick_fontsize
mpl.rcParams['ytick.labelsize'] = axis_tick_fontsize

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)


path_main = '/home/mark/gaussian-process/pilcoV0.9/scenarios/1dProb/gp_data_'
H = 60

alpha = 0.15
linestyle = ['-', '--', '-.', ':']
legend = [r'Mean', r'Existing Data', r'New Data', r'$p_{rw}$']
legend_fontsize = 23
axis_label_fontsize = 23
linewidth = 1.5
annotation_fontsize = 23

# comparison plot
a = 3.0
b_sim = 5.0
b_rw = 1.25
u = np.linspace(-1.2, 1.2, 200)
x_sim = np.zeros(len(u))
x_rw = np.zeros(len(u))
for i in range(len(u)):
    if u[i] < 0:
        x_sim[i] = -b_sim*u[i]**2
        x_rw[i] = -b_rw*u[i]**2
    else:
        x_sim[i] = -a*u[i]**2
        x_rw[i] = -a*u[i]**2

fig, ax = plt.subplots()

ax.plot(u, x_sim*0.05, color=sim_dark, linewidth=3)
ax.plot(u, x_rw*0.05, color=rw_dark, linewidth=3)

ax.set_xlim([-1.2, 1.2])
ax.set_ylim([-.4, 0.2])

#ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
ax.set_ylabel('Targets ($\dot{x}$)', size=axis_label_fontsize)
ax.set_xlabel('Inputs ($u$)', size=axis_label_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

ax.annotate(r'Simulator', xy=(-0.8, -0.2),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=legend_fontsize, color=sim_dark)
ax.annotate(r'Real World', xy=(-1.0, 0),  xycoords='data',
            xytext=(0, 0), textcoords='offset points',
            fontsize=legend_fontsize, color=rw_dark)

plt.savefig(path_main + 'both.pdf',
            format='pdf', transparent=True, bbox_inches='tight')

# for i1 in range(1, 5):
# for i1 in range(3, 4):
for i1 in range(1, 1):
    i2 = 0
    while 1:
        i2 += 1
        print 'i1 = ' + str(i1) + ', i2 = ' + str(i2)
        try:
            path = path_main + str(i1) + '_' + str(i2)
            data = scipy.io.loadmat(path)
            x = np.transpose(np.array(data['x']))
            var = np.transpose(np.array(data['var']))
            m = np.transpose(np.array(data['m']))
            p_rw = np.transpose(np.array(data['p_rw']))
            inputs = np.transpose(np.array(data['inputs']))
            targets = np.transpose(np.array(data['targets']))
            cost = np.transpose(np.array(data['cost_array']))
            xx = np.transpose(np.array(data['xx']))
            uu = np.transpose(np.array(data['uu']))

            x = x[0]
            var = var[0]
            m = m[0]
            p_rw = p_rw[0]
            inputs = inputs[0]
            targets = targets[0]
            cost = cost[0]
            xx = xx[0]
            uu = uu[0]

            inputs_new = inputs[H*(i2-1):H*i2]
            targets_new = targets[H*(i2-1):H*i2]

            total_costs = []
            for j in range(1, i2+1):
                total_cost = np.sum(cost[H*(j-1):H*j])
                print total_cost
                total_costs.append(total_cost)

            c = ['k', 'b', 'g', 'm']
            fig, ax = plt.subplots()

            v1 = m+var
            v2 = m-var

            lns1 = ax.plot(x, m, color=c[0],
                             linestyle=linestyle[0], label=legend[0],
                             linewidth=linewidth)
            ax.fill_between(x, v1,
                            v2, alpha=alpha,
                            facecolor=c[0], edgecolor=c[0])

            if i1 == 1 or i1 == 3:
                env = 'Simulator'
                color_new = sim_dark
                color_old = sim_light
            else:
                env = 'Real World'
                color_new = rw_dark
                color_old = rw_light

            if i1 == 4:
                print inputs
                print inputs_new
                print targets
                print targets_new

            lns2 = ax.plot(inputs, targets, color=color_old, linestyle='None',
                    marker='.', markersize=12, label=legend[1])
            lns3 = ax.plot(inputs_new, targets_new, color=color_new, linestyle='None',
                    marker='.', markersize=12, label=legend[2])
            lns = lns1+lns2+lns3

            if i1 == 3:
                ax2 = ax.twinx()
                ax2.set_yticks([0])
                lns4 = ax2.plot(x, p_rw, color=dark_yellow,
                                linestyle=linestyle[0], label=legend[3],
                                linewidth=linewidth)
                #ax2.set_ylabel(r'$p_{rw}$', size=axis_label_fontsize, color=grey)
                #ax2.yaxis.set_label_coords(1.02, 0.2)
                for tl in ax2.get_yticklabels():
                    tl.set_color(axes_ticks_color)
                ax2.set_ylim([-0.1, 3])
                lns += lns4
                ax2.annotate(r'$p_{rw}$', xy=(0.1, 0.4),  xycoords='data',
                             xytext=(0, 0), textcoords='offset points',
                             fontsize=legend_fontsize, color=axes_ticks_color)
                ax2.annotate(r'$1$', xy=(0.04, 0.92),  xycoords='data',
                             xytext=(0, 0), textcoords='offset points',
                             fontsize=legend_fontsize, color=axes_ticks_color)

            # added these three lines
            labs = [l.get_label() for l in lns]

            #if (i1 == 1 and i2 == 1) or (i1 == 2 and i2 == 1):
            ax.legend(lns, labs,
                      loc='best', fancybox=True,
                      fontsize=legend_fontsize)

            cost_str = '%.1f' % total_cost
            ax.annotate(env+'\nCost = '+cost_str, xy=(-1.0, .1),
                        xycoords='data', xytext=(0, 0),
                        textcoords='offset points',
                        bbox=dict(boxstyle="round", fc="1.0",
                                  edgecolor=axes_ticks_color),
                        fontsize=legend_fontsize)

            ax.set_xlim([-1.2, 1.2])
            ax.set_ylim([-.4, 0.2])

            #ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
            ax.set_ylabel('Targets ($\dot{x}$)', size=axis_label_fontsize)
            ax.set_xlabel('Inputs ($u$)', size=axis_label_fontsize)

                    # Hide the right and top spines
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            # Only show ticks on the left and bottom spines
            ax.yaxis.set_ticks_position('left')
            ax.xaxis.set_ticks_position('bottom')

            plt.savefig(path + '.pdf',
                        format='pdf', transparent=True, bbox_inches='tight')

            # plot xx and uu vs t
            fig, axarr = plt.subplots(2, sharex=True)
            t = np.linspace(0, 3, 60)

            axarr[0].plot(t, xx, 'k', linewidth=3)
            axarr[0].set_ylabel(r'$x$', size=33)
            axarr[1].set_ylabel(r'$u$', size=33)
            axarr[1].plot(t, uu, 'k', linewidth=3)
            axarr[1].set_xlabel(r'$t~(s)$', size=33)
            axarr[1].set_ylim([-1, 1])
            axarr[0].set_ylim([-1, 3.1])

            for ax in axarr:
                ax.get_yaxis().set_label_coords(-0.07, 0.6)

                for tick in ax.xaxis.get_major_ticks():
                    tick.label.set_fontsize(axis_tick_fontsize)
                for tick in ax.yaxis.get_major_ticks():
                    tick.label.set_fontsize(axis_tick_fontsize)

                # Hide the right and top spines
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                # Only show ticks on the left and bottom spines
                ax.yaxis.set_ticks_position('left')
                ax.xaxis.set_ticks_position('bottom')

                # Find at most 101 ticks on the y-axis at 'nice' locations
                max_yticks = 4
                yloc = plt.MaxNLocator(max_yticks)
                ax.yaxis.set_major_locator(yloc)

            plt.savefig(path + '_data.pdf',
                        format='pdf', transparent=True, bbox_inches='tight',
                        dpi=1000)



        except:
           print "Unexpected error:", sys.exc_info()[0]
           break


# plot cost
fig, ax = plt.subplots()
x = np.linspace(-1, 3, 100)
sigma = 0.25
c = 1-np.exp(-x**2/(2*sigma**2))

ax.plot(x, c, 'k', linewidth=3)
ax.set_ylabel(r'$c(x)$', size=33)
ax.set_xlabel(r'$x$', size=33)
ax.set_ylim([-0.1, 1.1])
ax.set_xlim([-1, 3])
ax.set_aspect(1.3)

# ax.get_yaxis().set_label_coords(-0.07, 0.6)

for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(axis_tick_fontsize)

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

# Find at most 101 ticks on the y-axis at 'nice' locations
max_yticks = 6
yloc = plt.MaxNLocator(max_yticks)
ax.yaxis.set_major_locator(yloc)

plt.savefig(path_main + '_cost.pdf',
            format='pdf', transparent=True, bbox_inches='tight',
            dpi=1000)
