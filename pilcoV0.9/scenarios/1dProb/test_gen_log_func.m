clc; clear all;
t = 0:0.001:0.1;
[y, dy] = gen_logistic_function(t);

figure(10); clf;
plot(t,y)
hold all
plot(t,dy)