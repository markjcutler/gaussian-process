function plot_policy(policy)

state = -1:0.1:3;
u = zeros(size(state));
for i=1:length(state)
    u(i) = policy.fcn(policy,state(i),0);
end

figure(12); clf;
plot(state,u)