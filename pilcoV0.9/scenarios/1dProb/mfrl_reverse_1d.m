% mfrl_reverse_1d.m

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
% rng(1651)
rng('shuffle')

% first, learn in sim until convergence (possibly with gpops bootstrap)
global_iter = 1;
learn1d_fun(0, 0, 0, global_iter); global_iter = global_iter+1;

% next, learn in real world using sim as prior
learn1d_fun('1d_2_H60.mat', 0, 0, global_iter); global_iter = global_iter+1;

% learn in sim again using real-world data
learn1d_fun(0, '1d_2_H60.mat', 0, global_iter); global_iter = global_iter+1;

% finally, learn in real world again with updated simulation prior
learn1d_fun('1d_2_H60.mat', 0, 0, global_iter); global_iter = global_iter+1;


% augment inputs and outputs of last data files for plotting purposes
clear all
load('gp_data_2_2.mat');
targets_old = targets;
inputs_old = inputs;
load('gp_data_4_1.mat');
inputs = [inputs; inputs_old];
targets = [targets; targets_old];
save('gp_data_4_1.mat');
load('gp_data_4_2.mat');
inputs = [inputs; inputs_old];
targets = [targets; targets_old];
save('gp_data_4_2.mat');
