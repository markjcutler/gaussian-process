%% pendubot_learn.m
% *Summary:* Script to learn a controller for the pendubot
% swingup (a double pendulum with the inner joint actuated)
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls
% # Controlled learning (train dynamics model, policy learning, policy
% application)

%% Code

% 1. Initialization
% clear all; close all;

clc;
temporaryBreakpointData=dbstatus('-completenames');
clear functions;
dbstop(temporaryBreakpointData);
clear global;
clear variables;

rng('default')
% rng(1651)
rng('shuffle')

use_prior = 1;
use_hw = 0;
use_gpops = 0;

extra_plots = 1;

settings_1d;            % load scenario-specific settings
basename = '1d_';       % filename used for saving data


if use_gpops
    
    % load saved data
    load('training.mat');
    
    %% use kmeans clustering to find nc rbf centers
    [idx, c] = kmeans([input',target'], nc);

    % 4. Set up the policy structure                                                              % torque
    policy.p.inputs = c(:,1); % init. location of                           % basis functions
    policy.p.targets = c(:,2);    % init. policy targets 
end

%% load and use prior information
if use_prior
    prior_info = load('sim_prior2_d.mat');
    
    % set policy to one from prior information
%     policy.p.inputs = prior_info.policy.p.inputs;
%     policy.p.targets = prior_info.policy.p.targets;
%     policy.p.hyp = prior_info.policy.p.hyp;

    policy.p.w = prior_info.policy.p.w;
    policy.p.b = prior_info.policy.p.b;

    % set prior gp as prior in dynmodel struct
        dynmodel.prior = prior_info.dynmodel;
end

%% load and use hardware information
if use_hw
    hw_info = load('rw3_d.mat');
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
    
end


% return


% 2. Initial J random rollouts
for jj = 1:J
    if use_prior || use_gpops %isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
    %   if plotting.verbosity > 0;      % visualization of trajectory
    %     if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    %     draw_rollout_1d;
    %   end
end

% x
% return

mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
    plot_policy(policy);
    trainDynModel;   % train (GP) dynamics model
    
    % plot dynamics model
    figure(5);clf;
    u_prime = (-1.5:0.01:1.5)';
    mean = zeros(size(u_prime));
    var = zeros(size(u_prime));
    inoutvar = zeros(size(u_prime));
    for kk=1:length(u_prime)
        [mean(kk), var(kk), inoutvar(kk)] = gp0(dynmodel, u_prime(kk), S0);
        %       var(kk)
    end
    if isfield(dynmodel,'prior')
        for i=1:length(u_prime)
            prior_mean = dynmodel.prior.fcn(dynmodel.prior, u_prime(i), zeros(D));
            mean(i) = mean(i) - prior_mean;
        end
    end
    f = [mean+2*sqrt(var + exp(2*dynmodel.hyp(3))); flipdim(mean-2*sqrt(var + exp(2*dynmodel.hyp(3))),1)];
    fill([u_prime; flipdim(u_prime,1)], f, [7 7 7]/8)
    hold on; plot(u_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs, dynmodel.targets, '+', 'MarkerSize', 12)
    grid on
    xlabel('input, x')
    ylabel('output, y')
    drawnow;
    %     axis([-1.9 1.9 -0.9 3.9])
    % return;
    
    if extra_plots
        
        
        % plot the first data point covariance

        % get usuable versions of hyperparameters
        u_prime = (-1.5:0.01:1.5)';
        log_length_scales = dynmodel.gpmodel_rw.hyp(1:D,:);
        length_scales_squared = exp(2*log_length_scales);
        log_signal_sigma = dynmodel.gpmodel_rw.hyp(D+1,:);
        signal_var = exp(2*log_signal_sigma);
        noise_var = exp(2*dynmodel.gpmodel_rw.hyp(D+2,:));

        figure(14); clf;
        hold all
        cov_total = 0;
        for jj=1:length(dynmodel.gpmodel_rw.inputs)
            cov = zeros(size(u_prime));
            for i=1:length(u_prime)
                cov(i) = cov(i) + signal_var*exp(1/(-2*length_scales_squared)*...
                    (u_prime(i) - dynmodel.gpmodel_rw.inputs(jj))^2);
            end
            cov_total = cov_total + cov;
            plot(u_prime, cov/max(cov))
        end
        plot(u_prime, cov_total, 'LineWidth', 4)
        
        
        % plot dynamics model
        figure(6);clf;
        u_prime = (-1.5:0.01:1.5)';
        mean = zeros(size(u_prime));
        var = zeros(size(u_prime));
        dvardm = zeros(size(u_prime));
        dvards = zeros(size(u_prime));
        inoutvar = zeros(size(u_prime));
        p_rw = zeros(size(u_prime));
        for kk=1:length(u_prime)
            [mean(kk), var(kk), inoutvar(kk), ~, dvardm(kk), ~, ~, ...
                dvards(kk), ~] = gp0d(dynmodel.gpmodel_rw, u_prime(kk), S0);
            
            [n, D] = size(dynmodel.gpmodel_rw.inputs);    % number of examples and dimension of inputs
            
            % get usuable versions of hyperparameters
            log_signal_sigma = dynmodel.gpmodel_rw.hyp(D+1,:);
            signal_var = exp(2*log_signal_sigma);
            %     noise_var = exp(2*gpmodel.gpmodel_rw.hyp(D+2,:));
            
            
            % get the variance at the mean
            p_rw(kk) = weight_fnc2(dynmodel, u_prime(kk), S0);
            
            %       var(kk)
        end
        if isfield(dynmodel.gpmodel_rw,'prior')
            for i=1:length(u_prime)
                prior_mean = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, u_prime(i), zeros(D));
                mean(i) = mean(i) - prior_mean;
            end
        end
        f = [mean+2*sqrt(var + exp(2*dynmodel.gpmodel_rw.hyp(3))); flipdim(mean-2*sqrt(var + exp(2*dynmodel.gpmodel_rw.hyp(3))),1)];
        fill([u_prime; flipdim(u_prime,1)], f, [7 7 7]/8)
        hold on; plot(u_prime, mean, 'LineWidth', 2); plot(dynmodel.gpmodel_rw.inputs, dynmodel.gpmodel_rw.targets, '+', 'MarkerSize', 12)
        grid on
        xlabel('input, x')
        ylabel('output, y')
        drawnow;
        
        %%
        figure(7);clf
        plot(u_prime, p_rw)
        hold all
        plot(u_prime, 20*abs(dvardm) + abs(var)/abs(signal_var))
        plot(u_prime, dvards)
        plot(u_prime, 10*abs(var)/abs(signal_var))
        legend('prw','dvardm','dvards','var/signalvar')
        axis([-inf, inf, -inf 1.1])
        %     return
        
        
        % plot dynamics model
        figure(8);clf;
        u_prime = (-1.5:0.01:1.5)';
        mean = zeros(size(u_prime));
        var = zeros(size(u_prime));
        inoutvar = zeros(size(u_prime));
        dynmodel = rmfield(dynmodel,'gpmodel_rw');
        
        for kk=1:length(u_prime)
            [mean(kk), var(kk), inoutvar(kk)] = gp0(dynmodel, u_prime(kk), S0);
        end
        f = [mean+2*sqrt(var + exp(2*dynmodel.hyp(3))); flipdim(mean-2*sqrt(var + exp(2*dynmodel.hyp(3))),1)];
        fill([u_prime; flipdim(u_prime,1)], f, [7 7 7]/8)
        hold on; plot(u_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs, dynmodel.targets, '+', 'MarkerSize', 12)
        grid on
        xlabel('input, x')
        ylabel('output, y')
        drawnow;
        
        t = 0:0.001:0.1;
        [y, dy] = gen_logistic_function(t);
        
        figure(10); clf;
        plot(t,y)
        drawnow;
        %     hold all
        %     plot(t,dy)
        
        return;
    end
    
    learnPolicy;     % learn policy
    applyController; % apply controller to system
    disp(['controlled trial # ' num2str(j)]);
    %   if plotting.verbosity > 0;      % visualization of trajectory
    %     if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    %     draw_rollout_1d;
    %   end
end