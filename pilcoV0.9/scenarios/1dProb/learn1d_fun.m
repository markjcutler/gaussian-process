function learn1d_fun(prior, hw, gpops, global_iter)

rng('default')
rng(1651)
% rng('shuffle')

settings_1d;            % load scenario-specific settings
basename = '1d_';       % filename used for saving data

use_prior = 0;
use_hw = 0;
use_gpops = 0;

if prior ~= 0
    use_prior = 1;
    plant.params.c = 0.25;
end
if hw ~= 0
    use_hw = 1;
    plant.params.c = 1;
end
if gpops ~= 0
    use_gpops = 1;
    plant.params.c = 1;
end




if use_gpops
    
    % load saved data
    load(gpops);
    
    %% use kmeans clustering to find nc rbf centers
    [idx, c] = kmeans([input',target'], nc);

    % 4. Set up the policy structure                                                              % torque
    policy.p.inputs = c(:,1); % init. location of                           % basis functions
    policy.p.targets = c(:,2);    % init. policy targets 
end

%% load and use prior information
if use_prior
    prior_info = load(prior);
    
    % set policy to one from prior information
%     policy.p.inputs = prior_info.policy.p.inputs;
%     policy.p.targets = prior_info.policy.p.targets;
%     policy.p.hyp = prior_info.policy.p.hyp;

    policy.p.w = prior_info.policy.p.w;
    policy.p.b = prior_info.policy.p.b;

    % set prior gp as prior in dynmodel struct
        dynmodel.prior = prior_info.dynmodel;
end

%% load and use hardware information
if use_hw
    hw_info = load(hw);
    
    % set gp inforation into dynmodel struct
    dynmodel.gpmodel_rw = hw_info.dynmodel;
    
    % reconstruct the 'correct inputs' based on the prior
    if isfield(dynmodel.gpmodel_rw,'prior')
        [n, D] = size(dynmodel.gpmodel_rw.prior.inputs);
        % prior gp mean
        prior_mean = zeros(size(dynmodel.gpmodel_rw.targets(:,difi)));
        for i=1:length(dynmodel.gpmodel_rw.targets)
            prior_mean(i,:) = dynmodel.gpmodel_rw.prior.fcn(dynmodel.gpmodel_rw.prior, dynmodel.gpmodel_rw.inputs(i,:)', zeros(D));
        end
        dynmodel.gpmodel_rw.targets(:,difi) = dynmodel.gpmodel_rw.targets(:,difi) + prior_mean;
        dynmodel.gpmodel_rw.targets(:,plant.angi) = wrapToPi(dynmodel.gpmodel_rw.targets(:,plant.angi));
        
        dynmodel.gpmodel_rw = rmfield(dynmodel.gpmodel_rw,'prior');
        
    end
    
end

% 2. Initial J random rollouts
for jj = 1:J
    if use_prior || use_gpops %isfield(dynmodel,'prior')
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), policy, H, plant, cost);
    else
        [xx, yy, realCost{jj}, latent{jj}] = ...
            rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
    end
    x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
end


mu0Sim(odei,:) = mu0; S0Sim(odei,odei) = S0;
mu0Sim = mu0Sim(dyno); S0Sim = S0Sim(dyno,dyno);

% 3. Controlled learning (N iterations)
for j = 1:N
    plot_policy(policy);
    trainDynModel;   % train (GP) dynamics model
    
    % plot dynamics model
    figure(5);clf;
    u_prime = (-1.5:0.01:1.5)';
    mean = zeros(size(u_prime));
    var = zeros(size(u_prime));
    p_rw = zeros(size(u_prime));
    inoutvar = zeros(size(u_prime));
    for kk=1:length(u_prime)
        [mean(kk), var(kk), inoutvar(kk), p_rw(kk)] = gp0(dynmodel, u_prime(kk), S0);
        %       var(kk)
    end
    targets = dynmodel.targets;
%     mean2 = mean;
    if isfield(dynmodel,'prior')
        for i=1:length(u_prime)
            prior_mean = dynmodel.prior.fcn(dynmodel.prior, u_prime(i), zeros(D));
            if isfield(dynmodel.prior,'gpmodel_rw')
                mean(i) = prior_mean;
            end
        end
        for i=1:length(dynmodel.targets)
            prior_mean = dynmodel.prior.fcn(dynmodel.prior, dynmodel.inputs(i,:)', zeros(D));
            targets(i) = targets(i) + prior_mean;
        end
    end
    f = [mean+2*sqrt(var + exp(2*dynmodel.hyp(3))); flipdim(mean-2*sqrt(var + exp(2*dynmodel.hyp(3))),1)];
    var = 2*sqrt(var + exp(2*dynmodel.hyp(3)));
    fill([u_prime; flipdim(u_prime,1)], f, [7 7 7]/8)
    hold on; plot(u_prime, mean, 'LineWidth', 2); plot(dynmodel.inputs, targets, '+', 'MarkerSize', 12)
%     hold all
%     plot(u_prime, mean2, 'LineWidth', 2);
    grid on
    xlabel('input, x')
    ylabel('output, y')
    drawnow;
    %     axis([-1.9 1.9 -0.9 3.9])
    % return;

    
    save_data(global_iter,j,u_prime,var,mean,dynmodel.inputs,targets,y,cost,p_rw,xx)


    rng('shuffle')
    
%     return;
    
    
    learnPolicy;     % learn policy
    
    policy.p
    
    applyController; % apply controller to system
    disp(['controlled trial # ' num2str(j)]);

end

function save_data(i,j,x,var,m,inputs,targets,state,cost,p_rw,xx)

cost_array = zeros(size(state));
n = size(state,2);
for ii=1:size(state,1)
    cost_array(ii,:) = cost.fcn(cost,state(ii,:),zeros(n));
end
uu = xx(:,2);
xx = xx(:,1);

save(strcat('gp_data_',num2str(i),'_',num2str(j)','.mat'),...
    'x','var','m','inputs','targets','cost_array','p_rw','xx','uu');