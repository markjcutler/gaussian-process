%% dynamics_pendubot.m
% *Summary:* Implements ths ODE for simulating the Pendubot 
% dynamics, where an input torque f can be applied to the inner link 
%
%    function dz = dynamics_pendubot(t,z,f)
%
%
% *Input arguments:*
%
%		t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f     (optional): torque f(t) applied to inner pendulum
%
% *Output arguments:*
%   
%   dz    if 3 input arguments:      state derivative wrt time
%         if only 2 input arguments: total mechanical energy
%
%   Note: It is assumed that the state variables are of the following order:
%         dtheta1:  [rad/s] angular velocity of inner pendulum
%         dtheta2:  [rad/s] angular velocity of outer pendulum
%         theta1:   [rad]   angle of inner pendulum
%         theta2:   [rad]   angle of outer pendulum
%
% A detailed derivation of the dynamics can be found in:
%
% M.P. Deisenroth: 
% Efficient Reinforcement Learning Using Gaussian Processes, Appendix C, 
% KIT Scientific Publishing, 2010.
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-08

function dz = dynamics_1d(t,z,f, params)
%% Code
% dz = -5*f(t)^2;
dz = -params.a*f(t)^2;


if f(t) > 0
    dz = params.b*dz; %0.6*dz;
end
if f(t) < 0
    dz = params.c*dz; %0.25*dz;
end
