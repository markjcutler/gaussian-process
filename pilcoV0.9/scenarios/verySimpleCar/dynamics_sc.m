%% dynamics_cp.m
% *Summary:* Implements ths ODE for simulating car dynamics.
%
%    function dz = dynamics_cp(t, z, f)
%
%
% *Input arguments:*
%
%	t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f1,f2 inputs ([steering angle, speed])
%
% *Output arguments:*
%
%   dz    state derivative wrt time
%
%
% Note: It is assumed that the state variables are of the following order:
%       x:       [m]   x position
%
%
% A detailed derivation of the dynamics can be found in:
% 
% http://planning.cs.uiuc.edu/node658.html
%

% function dz = dynamics_sc(t,z,f1,f2)
function dz = dynamics_sc(t,z,f1)

% state
% x = z(1);

% action
vel = f1(t);

% Calculate derivatives
dz = vel;
