% clc; clear all;


rd = '../'; 
addpath([rd 'util']);

% [M, S, V] = gp_uncertain_md(logh, input, target, m, s^2)

% *Input arguments:*
%
%   gpmodel    GP model struct
%     hyp      log-hyper-parameters                                  [D+2 x  E ]
%     inputs   training inputs                                       [ n  x  D ]
%     targets  training targets                                      [ n  x  E ]
%     nigp     (optional) individual noise variance terms            [ n  x  E ]
%   m          mean of the test distribution                         [ D  x  1 ]
%   s          covariance matrix of the test distribution            [ D  x  D ]

sigma_f = 1;
sigma_w = 0.4;
l = 2.5;

% prior:
input = linspace(-12, 12, 30)';
target = sin(0.5*input) + input/4;
gpmodel.prior.hyp = [log(l), log(sigma_f), log(sigma_w)]';
gpmodel.prior.inputs = input;
gpmodel.prior.targets = target;
gpmodel.prior.fnc = @gp1d;


input = linspace(-4.5, 4.5, 5)';
target = sin(0.5*input) + input/4;
m = -6.0;
s = 1.5;
logh = [log(l), log(sigma_f), log(sigma_w)];


gpmodel.hyp = logh';
gpmodel.inputs = input;
gpmodel.targets = target;
if isfield(gpmodel, 'prior')
    for i=1:length(gpmodel.inputs)
        prior_mean = gpmodel.prior.fnc(gpmodel.prior, gpmodel.inputs(i,:), zeros(size(gpmodel.inputs(i,:))));
        gpmodel.targets(i,:) = gpmodel.targets(i,:) - prior_mean;
    end
end

[M1, S1, V1] = gp0(gpmodel, m, s^2)
[M, S, V, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d(gpmodel, m, s^2)
% [M2, S2, V2] = Copy_of_gp0(gpmodel, m, s^2);
err = [M1, S1, V1] - [M, S, V]