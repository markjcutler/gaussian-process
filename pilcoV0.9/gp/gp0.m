%% gp0.m
% Combine the outputs from two separate calls to gp0_single.m using the
% proper weighting

function [M, S, V, p_rw] = gp0(gpmodel, m, s)
%% Code

if isfield(gpmodel,'gpmodel_rw')
    %p_rw = gpmodel.p_rw;
%     p_rw is proportional to the variance at m
% right now just using a saturated linear function for calculating the
% weight parameter.  If variance is close to sigma_n, then p_rw -> 1 (near
% perfect measurement of state).  If variance is close to sigma_f, then
% p_rw -> 0 (essentially no information about the underlying function)

    p_rw = weight_fnc2(gpmodel, m, s);

    [M_rw, S_rw, V_rw] = gp0_single(gpmodel.gpmodel_rw, m, s);
    [M_sim, S_sim, V_sim] = gp0_single(gpmodel, m, s);
    
    M = M_sim + p_rw*(M_rw - M_sim);
    V = V_sim + p_rw*(V_rw - V_sim);
    tmp = (M_sim - M)*(M_sim - M)' + S_sim;
    S = tmp + p_rw*((M_rw  - M)*(M_rw  - M)' + S_rw - tmp);

else
    p_rw = 0;
    [M, S, V] = gp0_single(gpmodel, m, s);
end
