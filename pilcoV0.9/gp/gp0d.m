%% gp0d.m
% Use two gp's (real world and sim)

function [M, S, V, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d(gpmodel, m, s)
%% Code 

% If no derivatives required, call gp0
if nargout < 4; [M, S, V] = gp0(gpmodel, m, s); return; end

if isfield(gpmodel,'gpmodel_rw')
    [~, D] = size(gpmodel.gpmodel_rw.inputs);    % number of examples and dimension of inputs
    E = size(gpmodel.targets,2);
    
    [p_rw, dpdm, dpds] = weight_fnc2(gpmodel, m, s);
%     p_rw

    [M_rw, S_rw, V_rw, dMdm_rw, dSdm_rw, dVdm_rw, dMds_rw, dSds_rw, ...
        dVds_rw] = gp0d_single(gpmodel.gpmodel_rw, m, s);
    [M_sim, S_sim, V_sim, dMdm_sim, dSdm_sim, dVdm_sim, dMds_sim, ...
        dSds_sim, dVds_sim] = gp0d_single(gpmodel, m, s);

    
   

    % unvectorize derivatives
    dMds_rw = reshape(dMds_rw, [E D D]);
    dSds_rw = reshape(dSds_rw, [E E D D]);
    dVds_rw = reshape(dVds_rw, [D E D D]);
    dSdm_rw = reshape(dSdm_rw, [E E D]);
    dVdm_rw = reshape(dVdm_rw, [D E D]);

    dMds_sim = reshape(dMds_sim, [E D D]);
    dSds_sim = reshape(dSds_sim, [E E D D]);
    dVds_sim = reshape(dVds_sim, [D E D D]);
    dSdm_sim = reshape(dSdm_sim, [E E D]);
    dVdm_sim = reshape(dVdm_sim, [D E D]);
    
    
    
    % combine the outputs
    M = M_sim + p_rw*(M_rw - M_sim);
    V = V_sim + p_rw*(V_rw - V_sim);
    sim_center = (M_sim - M)*(M_sim - M)' + S_sim;
    rw_center = (M_rw - M)*(M_rw - M)' + S_rw;
    S = sim_center + p_rw*(rw_center - sim_center);
    dMdm = dMdm_sim + p_rw*(dMdm_rw - dMdm_sim) + (M_rw - M_sim)*dpdm';
    tmp = zeros([E D D]);
    for i=1:E
        tmp(i,:,:) = (M_rw(i)-M_sim(i))*dpds;
    end
    dMds = dMds_sim + p_rw*(dMds_rw - dMds_sim) + tmp;
    tmp = zeros([D E D]);
    for i=1:D
        tmp(:,:,i) = (V_rw - V_sim)*dpdm(i);
    end
    dVdm = dVdm_sim + p_rw*(dVdm_rw - dVdm_sim) + tmp;
    tmp =zeros([D E D D]);
    for i=1:D
        for j=1:E
            tmp(i,j,:,:) = (V_rw(i,j) - V_sim(i,j))*dpds;
        end
    end
    dVds = dVds_sim + p_rw*(dVds_rw - dVds_sim) + tmp;
    
    dSdm = zeros([E E D]);
    dSds = zeros([E E D D]);
    for i=1:E
            
        for j=1:i
            
            tmp_sim = zeros([1,1,D]);
            tmp_sim(1,:,:) = (M_sim(i) - M(i))*(dMdm_sim(j,:)-dMdm(j,:)) + ...
                (M_sim(j) - M(j))*(dMdm_sim(i,:)-dMdm(i,:));
            tmp_sim = tmp_sim + dSdm_sim(i,j,:);
            
            tmp_rw = zeros([1,1,D]);
            tmp_rw(1,:,:) = (M_rw(i) - M(i))*(dMdm_rw(j,:)-dMdm(j,:)) + ...
                (M_rw(j) - M(j))*(dMdm_rw(i,:)-dMdm(i,:));
            tmp_rw = tmp_rw + dSdm_rw(i,j,:);
            tmp = zeros(1,1,D);
            tmp(1,:,:) = (rw_center(i,j) - sim_center(i,j))*dpdm';
            dSdm(i, j, :) = tmp_sim + p_rw*(tmp_rw - tmp_sim) + tmp;
            
            tmp_sim = (M_sim(i) - M(i))*reshape(dMds_sim(j,:,:) - ...
                dMds(j,:,:), D, D) + (M_sim(j) - M(j))*reshape(dMds_sim(i,:,:) - ...
                dMds(i,:,:), D, D) + reshape(dSds_sim(i,j,:,:),D,D);
            tmp_rw = (M_rw(i) - M(i))*reshape(dMds_rw(j,:,:) - ...
                dMds(j,:,:),D,D) + (M_rw(j) - M(j))*reshape(dMds_rw(i,:,:) - ...
                dMds(i,:,:),D,D) + reshape(dSds_rw(i,j,:,:),D,D);
            dSds(i,j,:,:) = tmp_sim + p_rw*(tmp_rw - tmp_sim) + (rw_center(i,j) - sim_center(i,j))*dpds;
            if i ~= j
                dSdm(j,i,:) = dSdm(i,j,:);
                dSds(j,i,:,:) = dSds(i,j,:,:);
            end
        end
    end
    
%     tmp = 2*(M_sim - M)*(dMds_sim - dMds) + dSds_sim;
%     dSds = tmp + p_rw*(2*(M_rw - M)*(dMds_rw - dMds) + dSds_rw - tmp);
   
    
    % vectorize derivatives
    dMds = reshape(dMds,[E D*D]);
    dSds = reshape(dSds,[E*E D*D]); dSdm = reshape(dSdm,[E*E D]);
    dVds = reshape(dVds,[D*E D*D]); dVdm = reshape(dVdm,[D*E D]);
    
    
else
    [M, S, V, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d_single(gpmodel, m, s);
end