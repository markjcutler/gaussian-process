function [y, dydt] = gen_logistic_function(t)

% Implements the generalized logistic function
% See here for details: http://en.wikipedia.org/wiki/Generalised_logistic_function

A = 0;
K = 1;
B = 400;
v = 1.5;
Q = v; %.5;
M = 0.02;

y = A + (K-A)./((1 + Q*exp(B*(t-M))).^(1/v));
if nargout > 1
    dydt = (B*Q*(A-K)*exp(B*(t-M)).*(Q*exp(B*(t-M))+1).^(-(v+1)/v))/v;
    for i=1:length(t)
        if t(i) > 1 || t(i) < -1
            dydt(i) = 0;
        end
    end
end