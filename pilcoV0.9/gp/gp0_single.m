%% gp0.m
% *Summary:* Compute joint predictions for multiple GPs with uncertain inputs.
% If gpmodel.nigp exists, individial noise contributions are added.
% Predictive variances contain uncertainty about the function, but no noise.
%
%   function [M, S, V] = gp0(gpmodel, m, s)
%
% *Input arguments:*
%
%   gpmodel    GP model struct
%     hyp      log-hyper-parameters                                  [D+2 x  E ]
%     inputs   training inputs                                       [ n  x  D ]
%     targets  training targets                                      [ n  x  E ]
%     nigp     (optional) individual noise variance terms            [ n  x  E ]
%   m          mean of the test distribution                         [ D  x  1 ]
%   s          covariance matrix of the test distribution            [ D  x  D ]
%
% *Output arguments:*
%
%   M          mean of pred. distribution                            [ E  x  1 ]
%   S          covariance of the pred. distribution                  [ E  x  E ]
%   V          inv(s) times covariance between input and output      [ D  x  E ]
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-05-24
%
%% High-Level Steps
% # If necessary, compute kernel matrix and cache it
% # Compute predicted mean and inv(s) times input-output covariance
% # Compute predictive covariance matrix, non-central moments
% # Centralize moments

function [M, S, V] = gp0_single(gpmodel, m, s)
%% Code

prior_exisits = false;
if isfield(gpmodel,'prior')
    prior_exisits = true;
end
% add prior mean and input-output variance
% if prior_exisits
%     [M_prior, ~, V_prior] = gpmodel.prior.fnc(gpmodel.prior, m, s);
% end

persistent K iK beta oldX oldn K_prior beta_prior oldX_prior oldn_prior;
[n, D] = size(gpmodel.inputs);    % number of examples and dimension of inputs
E = size(gpmodel.targets,2);                               % number of outputs

% get usuable versions of hyperparameters
log_length_scales = gpmodel.hyp(1:D,:);
log_signal_sigma = gpmodel.hyp(D+1,:);
signal_var = exp(2*log_signal_sigma);
noise_var = exp(2*gpmodel.hyp(D+2,:));

% get usable version of hyperparameters for the prior
if prior_exisits
    [p, ~] = size(gpmodel.prior.targets);
    log_length_scales_prior = gpmodel.prior.hyp(1:D,:);
    log_signal_sigma_prior = gpmodel.prior.hyp(D+1,:);
    length_scales_2_prior = exp(2*log_length_scales_prior);
    signal_var_prior = exp(2*log_signal_sigma_prior);
    noise_var_prior = exp(2*gpmodel.prior.hyp(D+2,:));
end

% 1) if necessary: re-compute cached variables
if numel(gpmodel.hyp) ~= numel(oldX) || isempty(iK) || sum(any(gpmodel.hyp ~= oldX)) || n ~= oldn
  oldX = gpmodel.hyp; oldn = n;
  iK = zeros(n,n,E);  K = iK; beta = zeros(n,E);
  
  for i=1:E                                              % compute K and inv(K)
    v = bsxfun(@rdivide,gpmodel.inputs,exp(log_length_scales(:,i)'));
    K(:,:,i) = signal_var(i)*exp(-maha(v,v)/2);
    if isfield(gpmodel,'nigp')
      L = chol(K(:,:,i) + noise_var(i)*eye(n) + diag(gpmodel.nigp(:,i)))';
    else        
      L = chol(K(:,:,i) + noise_var(i)*eye(n))';
    end
    iK(:,:,i) = L'\(L\eye(n));
    beta(:,i) = L'\(L\gpmodel.targets(:,i));
  end
end

if prior_exisits
    % 1) if necessary: re-compute cached variables
    if numel(gpmodel.prior.hyp) ~= numel(oldX_prior) || ...
            sum(any(gpmodel.prior.hyp ~= oldX_prior)) || p ~= oldn_prior
      oldX_prior = gpmodel.prior.hyp; oldn_prior = p;
      K_prior = zeros(p,p,E); beta_prior = zeros(p,E);

      for i=1:E                                              % compute K and inv(K)
        v = bsxfun(@rdivide,gpmodel.prior.inputs,exp(log_length_scales_prior(:,i)'));
        K_prior(:,:,i) = signal_var_prior(i)*exp(-maha(v,v)/2);
        if isfield(gpmodel.prior,'nigp')
          L = chol(K_prior(:,:,i) + noise_var_prior(i)*eye(p) + diag(gpmodel.prior.nigp(:,i)))';
        else        
          L = chol(K_prior(:,:,i) + noise_var_prior(i)*eye(p))';
        end
        beta_prior(:,i) = L'\(L\gpmodel.prior.targets(:,i));
      end
    end
end

k = zeros(n,E); M = zeros(E,1); V = zeros(D,E); S = zeros(E);

v = bsxfun(@minus,gpmodel.inputs,m'); % centralize inputs, nxD
if prior_exisits
    v_prior = bsxfun(@minus,gpmodel.prior.inputs,m'); % centralize inputs
    k_prior = zeros(p,E);
    M_prior = zeros(E,1); V_prior = zeros(D,E);
end

% 2) compute predicted mean and inv(s) times input-output covariance
for i=1:E
    iLsqrt = diag(exp(-log_length_scales(:,i))); % inverse length scales
    iv = v*iLsqrt;
    B = iLsqrt*s*iLsqrt+eye(D); 
    t = iv/B;
    l = exp(-sum(iv.*t,2)/2); lb = l.*beta(:,i);
    tL = t*iLsqrt;
    c = signal_var(i)/sqrt(det(B));

    M(i) = sum(lb)*c;         % predicted mean
    V(:,i) = tL'*lb*c;       % inv(s) times input-output covariance
    k(:,i) = 2*log_signal_sigma(i)-sum(iv.*iv,2)/2;
  
    if prior_exisits
        iLsqrt = diag(exp(-log_length_scales_prior(:,i))); % inverse length-scales
        iv = v_prior*iLsqrt;
        B = iLsqrt*s*iLsqrt+eye(D); 
        t_prior = iv/B;
        l = exp(-sum(iv.*t_prior,2)/2); lb = l.*beta_prior(:,i);
        tL = t_prior*iLsqrt;
        c = signal_var_prior(i)/sqrt(det(B));

        M_prior(i) = sum(lb)*c;         % predicted mean
        V_prior(:,i) = tL'*lb*c;       % inv(s) times input-output covariance
        k_prior(:,i) = 2*log_signal_sigma_prior(i)-sum(iv.*iv,2)/2;
    end
end

ilength_scales_2 = exp(-2*log_length_scales);
inpiell2 = bsxfun(@times,v,permute(ilength_scales_2,[3,1,2])); % N-by-D-by-E
if prior_exisits
    ilength_scales_2_prior = exp(-2*log_length_scales_prior);
    inpiell2_prior = bsxfun(@times,v_prior,permute(ilength_scales_2_prior,[3,1,2])); % N-by-D-by-E
end

% 3) compute predictive covariance matrix, non-central moments
for i=1:E
    ii = inpiell2(:,:,i); % nxD, (x-mu)*inv(Lambda_a)
    if prior_exisits
        ii_prior = bsxfun(@rdivide,v_prior,length_scales_2_prior(:,i)'); % lambda_a^-1*(x_i - mu)
    end
  
  for j=1:i
    R = s*diag(ilength_scales_2(:,i)+ilength_scales_2(:,j))+eye(D); % DxD
    t = 1/sqrt(det(R)); % scalar
    ij = inpiell2(:,:,j); % nxD, (x-mu)*inv(Lambda_b)
    Qij = exp(bsxfun(@plus,k(:,i),k(:,j)')+maha(ii,-ij,R\s/2)); % nxn
	S(i,j) = t*beta(:,i)'*Qij*beta(:,j); % scalar, S is ExE
    if i==j
	  iKQij = iK(:,:,i).*Qij; 
      s1iKL = sum(iKQij,1); 
      S(i,j) = S(i,j) - t*sum(s1iKL);
    end

    if prior_exisits
        % covariance additional terms due to prior
        % sigma_ab^2 = beta_a*Q*beta_b + alpha_a*P*alpha_b +
        % alpha_a*Q_hat*beta_b + alpha_b*Q_hat*beta_a - (mean + mean_prior)^2 +
        % delta_ab(sigma_f^2 - tr((K_a + sigma_w^2)^-1*Q)
        R = s*diag(exp(-2*log_length_scales_prior(:,i))+exp(-2*log_length_scales_prior(:,j)))+eye(D); 
        iT = R\s;
        ij_prior = inpiell2_prior(:,:,j); % lambda_b^-1*(x_i - mu)
        Pij = 1/sqrt(det(R))*exp(bsxfun(@plus,k_prior(:,i),k_prior(:,j)')+maha(ii_prior,-ij_prior,1/2*iT));
        S(i,j) = S(i,j) + beta_prior(:,i)'*Pij*beta_prior(:,j);
        
        R_hat = s*diag(exp(-2*log_length_scales_prior(:,i))+exp(-2*log_length_scales(:,j)))+eye(D);
        iT_hat = R_hat\s;
        Qij_hat = 1/sqrt(det(R_hat))*exp(bsxfun(@plus,k_prior(:,i),k(:,j)')+maha(ii_prior,-ij,1/2*iT_hat));
        S(i,j) = S(i,j) + beta_prior(:,i)'*Qij_hat*beta(:,j); %last two terms are different
        R_hat = s*diag(exp(-2*log_length_scales(:,i))+exp(-2*log_length_scales_prior(:,j)))+eye(D);
        iT_hat = R_hat\s;
        Qij_hat2 = 1/sqrt(det(R_hat))*exp(bsxfun(@plus,k(:,i),k_prior(:,j)')+maha(ii,-ij_prior,1/2*iT_hat));
        S(i,j) = S(i,j) + beta(:,i)'*Qij_hat2*beta_prior(:,j);
    end

    % S is symmetric so no need to separately calc S(j,i)
    S(j,i) = S(i,j);
  end
  
  S(i,i) = S(i,i) + signal_var(i);
end

% add prior mean and input-output variance
if prior_exisits
    M = M + M_prior;
    V = V + V_prior;
end

% 4) centralize moments
S = S - M*M';

% DEBUG -- add noise variance here
% S = S + noise_var(i);
