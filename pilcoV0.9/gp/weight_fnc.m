function [p, dpdm, dpds] = weight_fnc(gpmodel,m,s)
[~, D] = size(gpmodel.gpmodel_rw.inputs);    % number of examples and dimension of inputs
E = size(gpmodel.gpmodel_rw.targets,2);

% get usuable versions of hyperparameters
log_signal_sigma = gpmodel.gpmodel_rw.hyp(D+1,:);
signal_var = exp(2*log_signal_sigma);
%     noise_var = exp(2*gpmodel.gpmodel_rw.hyp(D+2,:));
    
if nargout == 1
    [~, var, ~,] = gp0d(gpmodel.gpmodel_rw, m, s);


    nvar = norm(var, 'fro');
    nsvar = norm(signal_var);

    p = gen_logistic_function(nvar/nsvar);
else

    % get the variance at the mean
    [~, var, ~, ~, dvardm, ~, ~, dvards, ~] = gp0d(gpmodel.gpmodel_rw, m, s);

    dvardm = reshape(dvardm, [E E D]);
    dvards = reshape(dvards, [E E D D]);

    nvar = norm(var, 'fro');
    nsvar = norm(signal_var);

    [p, dp_rwdvar] = gen_logistic_function(nvar/nsvar);

    dnvar2dvar = var; % really 2*var, but also then divide by 2*nvar
    dp_rwdvar = 1/nsvar*dp_rwdvar;
    dp_rwdm = dp_rwdvar/nvar;
    dpdm = zeros(D,1);
    dpds = zeros(D,D);
    for i=1:D
        tmp_mat = sum(sum(dnvar2dvar.*dvardm(:,:,i)));
        dpdm(i) = dp_rwdm*tmp_mat;
        for j=1:i
            tmp_mat = sum(sum(dnvar2dvar.*dvards(:,:,i,j)));
            dpds(i,j) = dp_rwdm*tmp_mat;
            if i ~= j
                dpds(j,i) = dpds(i,j);
            end
        end
    end
end
