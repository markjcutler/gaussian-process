%% gp0d.m
% *Summary:* Compute joint predictions and derivatives for multiple GPs
% with uncertain inputs. Predictive variances contain uncertainty about the 
% function, but no noise.
% If gpmodel.nigp exists, individial noise contributions are added.
% 
%
%   function [M, S, V, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d(gpmodel, m, s)
%
% *Input arguments:*
%
%   gpmodel    GP model struct
%     hyp      log-hyper-parameters                                  [D+2 x  E ]
%     inputs   training inputs                                       [ n  x  D ]
%     targets  training targets                                      [ n  x  E ]
%     nigp     (optional) individual noise variance terms            [ n  x  E ]
%   m          mean of the test distribution                         [ D  x  1 ]
%   s          covariance matrix of the test distribution            [ D  x  D ]
%
% *Output arguments:*
%
%   M          mean of pred. distribution                            [ E  x  1 ]
%   S          covariance of the pred. distribution                  [ E  x  E ]
%   V          inv(s) times covariance between input and output      [ D  x  E ]
%   dMdm       output mean by input mean                             [ E  x  D ]
%   dSdm       output covariance by input mean                       [E*E x  D ]
%   dVdm       inv(s)*input-output covariance by input mean          [D*E x  D ]
%   dMds       ouput mean by input covariance                        [ E  x D*D]
%   dSds       output covariance by input covariance                 [E*E x D*D]
%   dVds       inv(s)*input-output covariance by input covariance    [D*E x D*D]
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-05-24
%
%% High-Level Steps
% # If necessary, compute kernel matrix and cache it
% # Compute predicted mean and inv(s) times input-output covariance
% # Compute predictive covariance matrix, non-central moments
% # Centralize moments
% # Vectorize derivatives

function [M, S, V, dMdm, dSdm, dVdm, dMds, dSds, dVds] = gp0d_single(gpmodel, m, s)
%% Code 

% If no derivatives required, call gp0
if nargout < 4; [M, S, V] = gp0(gpmodel, m, s); return; end

% add prior mean and input-output variance
prior_exisits = false;
if isfield(gpmodel,'prior')
    prior_exisits = true;
end

persistent K iK beta oldX oldn K_prior beta_prior oldX_prior oldn_prior;
[n, D] = size(gpmodel.inputs);    % number of examples and dimension of inputs
E = size(gpmodel.targets,2);                               % number of outputs
X = gpmodel.hyp;                              % short hand for hyperparameters

% get usable version of hyperparameters for the prior
if prior_exisits
    [p, ~] = size(gpmodel.prior.targets);
    X_prior = gpmodel.prior.hyp;                              % short hand for hyperparameters
end

% 1) if necessary: re-compute cached variables
if numel(X) ~= numel(oldX) || isempty(iK) || sum(any(X ~= oldX)) || n ~= oldn
  oldX = X; oldn = n;
  iK = zeros(n,n,E);  K = iK; beta = zeros(n,E);
  
  for i=1:E                                              % compute K and inv(K)
    inp = bsxfun(@rdivide,gpmodel.inputs,exp(X(1:D,i)'));
    K(:,:,i) = exp(2*X(D+1,i)-maha(inp,inp)/2);
    if isfield(gpmodel,'nigp')
      L = chol(K(:,:,i) + exp(2*X(D+2,i))*eye(n) + diag(gpmodel.nigp(:,i)))';
    else
      L = chol(K(:,:,i) + exp(2*X(D+2,i))*eye(n))';
    end
    iK(:,:,i) = L'\(L\eye(n));
    beta(:,i) = L'\(L\gpmodel.targets(:,i));
  end
end

if prior_exisits
    % 1) if necessary: re-compute cached variables
    if numel(X_prior) ~= numel(oldX_prior) || ...
            sum(any(X_prior ~= oldX_prior)) || p ~= oldn_prior
      oldX_prior = X_prior; oldn_prior = p;
      K_prior = zeros(p,p,E); beta_prior = zeros(p,E);

      for i=1:E                                              % compute K and inv(K)
        v = bsxfun(@rdivide,gpmodel.prior.inputs,exp(X_prior(1:D,i)'));
        K_prior(:,:,i) = exp(2*X_prior(D+1,i)-maha(v,v)/2);
        if isfield(gpmodel.prior,'nigp')
          L = chol(K_prior(:,:,i) + exp(2*X_prior(D+2,i))*eye(p) + diag(gpmodel.prior.nigp(:,i)))';
        else        
          L = chol(K_prior(:,:,i) + exp(2*X_prior(D+2,i))*eye(p))';
        end
        beta_prior(:,i) = L'\(L\gpmodel.prior.targets(:,i));
      end
    end
end

k = zeros(n,E); M = zeros(E,1); V = zeros(D,E); S = zeros(E);      % initialize
dMds = zeros(E,D,D); dSdm = zeros(E,E,D);
dSds = zeros(E,E,D,D); dVds = zeros(D,E,D,D); T = zeros(D);

inp = bsxfun(@minus,gpmodel.inputs,m'); % centralize inputs, nxD
if prior_exisits
    T_prior = zeros(D);
    T12 = zeros(D);
    T21 = zeros(D);
    v_prior = bsxfun(@minus,gpmodel.prior.inputs,m'); % centralize inputs
    k_prior = zeros(p,E);
    M_prior = zeros(E,1); V_prior = zeros(D,E);
    dMds_prior = zeros(E,D,D);
    dVds_prior = zeros(D,E,D,D);
end

% 2) compute predicted mean and inv(s) times input-output covariance
for i=1:E
  iL = diag(exp(-X(1:D,i))); % inverse length scales
  in = inp*iL;
  B = iL*s*iL+eye(D); LiBL = iL/B*iL;
  t = in/B;
  l = exp(-sum(in.*t,2)/2); lb = l.*beta(:,i);
  tL = t*iL;
  tlb = bsxfun(@times,tL,lb);
  c = exp(2*X(D+1,i))/sqrt(det(B));
  M(i) = c*sum(lb);
  V(:,i) = tL'*lb*c;                     % inv(s) times input-output covariance
  dMds(i,:,:) = c*tL'*tlb/2 - LiBL*M(i)/2;
  for d = 1:D
    dVds(d,i,:,:) = c*bsxfun(@times,tL,tL(:,d))'*tlb/2 - LiBL*V(d,i)/2 ...
      - (V(:,i)*LiBL(d,:) + LiBL(:,d)*V(:,i)')/2;
  end
  k(:,i) = 2*X(D+1,i)-sum(in.*in,2)/2;
    
    if prior_exisits
        iL = diag(exp(-X_prior(1:D,i))); % inverse length-scales
        in = v_prior*iL;
        B = iL*s*iL+eye(D);
        LiBL = iL/B*iL; % DxD
        t_prior = in/B;
        l = exp(-sum(in.*t_prior,2)/2); 
        lb = l.*beta_prior(:,i);
        tL = t_prior*iL;
        tlb = bsxfun(@times,tL,lb); % pxD 
        c = exp(2*X_prior(D+1,i))/sqrt(det(B));
        M_prior(i) = c*sum(lb);         % predicted mean
        V_prior(:,i) = tL'*lb*c;       % inv(s) times input-output covariance
        dMds_prior(i,:,:) = c*tL'*tlb/2 - LiBL*M_prior(i)/2; % DxD
        for d = 1:D
            dVds_prior(d,i,:,:) = c*bsxfun(@times,tL,tL(:,d))'*tlb/2 - LiBL*V_prior(d,i)/2 ...
              - (V_prior(:,i)*LiBL(d,:) + LiBL(:,d)*V_prior(:,i)')/2; % DxD
        end
        k_prior(:,i) = 2*X_prior(D+1,i)-sum(in.*in,2)/2;
    end
end
dMdm = V'; dVdm = 2*permute(dMds,[2 1 3]);                  % derivatives wrt m

iell2 = exp(-2*gpmodel.hyp(1:D,:));
inpiell2 = bsxfun(@times,inp,permute(iell2,[3,1,2])); % N-by-D-by-E
if prior_exisits
    
    dMdm_prior = V_prior'; 
    dVdm_prior = 2*permute(dMds_prior,[2 1 3]);                  % derivatives wrt m
    
    iell2_prior = exp(-2*gpmodel.prior.hyp(1:D,:));
    inpiell2_prior = bsxfun(@times,v_prior,permute(iell2_prior,[3,1,2])); % N-by-D-by-E
end

% 3) compute predictive covariance matrix, non-central moments
for i=1:E
    ii = inpiell2(:,:,i); % nxD, (x-mu)*inv(Lambda_a)
    if prior_exisits
        ii_prior = inpiell2_prior(:,:,i); % lambda_a^-1*(x_i - mu)
    end
  
  for j=1:i
    R = s*diag(iell2(:,i)+iell2(:,j))+eye(D); 
    t = 1/sqrt(det(R));
    ij = inpiell2(:,:,j);
    L = exp(bsxfun(@plus,k(:,i),k(:,j)')+maha(ii,-ij,R\s/2));
    if i==j
      iKL = iK(:,:,i).*L; 
      s1iKL = sum(iKL,1); 
      s2iKL = sum(iKL,2);
      S(i,j) = t*(beta(:,i)'*L*beta(:,i) - sum(s1iKL));
      zi = ii/R;
      bibLi = L'*beta(:,i).*beta(:,i); cbLi = L'*bsxfun(@times, beta(:,i), zi);
      r = (bibLi'*zi*2 - (s2iKL' + s1iKL)*zi)*t;
      for d = 1:D
        T(d,1:d) = 2*(zi(:,1:d)'*(zi(:,d).*bibLi) + ...
          cbLi(:,1:d)'*(zi(:,d).*beta(:,i)) - zi(:,1:d)'*(zi(:,d).*s2iKL) ...
          - zi(:,1:d)'*(iKL*zi(:,d)));
        T(1:d,d) = T(d,1:d)';
      end
    else
      zi = ii/R; zj = ij/R;
      S(i,j) = beta(:,i)'*L*beta(:,j)*t; 
      S(j,i) = S(i,j);
      
      bibLj = L*beta(:,j).*beta(:,i); 
      bjbLi = L'*beta(:,i).*beta(:,j);
      cbLi = L'*bsxfun(@times, beta(:,i), zi);
      cbLj = L*bsxfun(@times, beta(:,j), zj);
      
      r = (bibLj'*zi+bjbLi'*zj)*t;
      for d = 1:D
        T(d,1:d) = zi(:,1:d)'*(zi(:,d).*bibLj) + ...
          cbLi(:,1:d)'*(zj(:,d).*beta(:,j)) + zj(:,1:d)'*(zj(:,d).*bjbLi) + ...
          cbLj(:,1:d)'*(zi(:,d).*beta(:,i));
        T(1:d,d) = T(d,1:d)';
      end
    end

    Sij1 = S(i,j);
    if prior_exisits
        % covariance additional terms due to prior
        % sigma_ab^2 = beta_a*Q*beta_b + alpha_a*P*alpha_b +
        % alpha_a*Q_hat*beta_b + alpha_b*Q_hat*beta_a - (mean + mean_prior)^2 +
        % delta_ab(sigma_f^2 - tr((K_a + sigma_w^2)^-1*Q)
        R_prior = s*diag(iell2_prior(:,i)+iell2_prior(:,j))+eye(D); 
        t_prior = 1/sqrt(det(R_prior));
        iT_prior = R_prior\s;
        ij_prior = inpiell2_prior(:,:,j); % lambda_b^-1*(x_i - mu)
        Pij = exp(bsxfun(@plus,k_prior(:,i),k_prior(:,j)')+maha(ii_prior,-ij_prior,1/2*iT_prior));
        Sij2 = t_prior*beta_prior(:,i)'*Pij*beta_prior(:,j);
        S(i,j) = S(i,j) + Sij2;
        
        R_hat = s*diag(iell2_prior(:,i)+iell2(:,j))+eye(D);
        t_hat = 1/sqrt(det(R_hat));
        iT_hat = R_hat\s;
        Qij_hat = exp(bsxfun(@plus,k_prior(:,i),k(:,j)')+maha(ii_prior,-ij,1/2*iT_hat));
        Sij3 = t_hat*beta_prior(:,i)'*Qij_hat*beta(:,j);
        S(i,j) = S(i,j) + Sij3;
        
        R_hat2 = s*diag(iell2(:,i)+iell2_prior(:,j))+eye(D);
        t_hat2 = 1/sqrt(det(R_hat2));
        iT_hat2 = R_hat2\s;
        Qij_hat2 = exp(bsxfun(@plus,k(:,i),k_prior(:,j)')+maha(ii,-ij_prior,1/2*iT_hat2));
        Sij4 = t_hat2*beta(:,i)'*Qij_hat2*beta_prior(:,j);
        S(i,j) = S(i,j) + Sij4;
%         if max(max(abs(exp(bsxfun(@plus,k_prior(:,i),k(:,j)'))-exp(bsxfun(@plus,k(:,i),k_prior(:,j)'))'))) > 0
%             display('error');
%         end
        
        % get terms for dSdm and dSds
        zi_prior = ii_prior/R_prior;
        zj_prior = ij_prior/R_prior;
        bibLj = Pij*beta_prior(:,j).*beta_prior(:,i);
        bjbLi= Pij'*beta_prior(:,i).*beta_prior(:,j);
        cbLi = Pij'*bsxfun(@times, beta_prior(:,i), zi_prior);
        cbLj = Pij*bsxfun(@times, beta_prior(:,j), zj_prior);
        alpha_dR_alpha = (bibLj'*zi_prior+bjbLi'*zj_prior)*t_prior; %% **verified**

        for d = 1:D
            T_prior(d,1:d) = zi_prior(:,1:d)'*(zi_prior(:,d).*bibLj) + ...
              cbLi(:,1:d)'*(zj_prior(:,d).*beta_prior(:,j)) + zj_prior(:,1:d)'*(zj_prior(:,d).*bjbLi) + ...
              cbLj(:,1:d)'*(zi_prior(:,d).*beta_prior(:,i));
            T_prior(1:d,d) = T_prior(d,1:d)';
        end
        
        zi_prior = ii_prior/R_hat;
        zj = ij/R_hat;
        bibLj = Qij_hat*beta(:,j).*beta_prior(:,i);
        bjbLi = Qij_hat'*beta_prior(:,i).*beta(:,j);
        cbLi = Qij_hat'*bsxfun(@times, beta_prior(:,i), zi_prior);
        cbLj = Qij_hat*bsxfun(@times, beta(:,j), zj);
        alpha_a_dQ_hat_beta_b = (bibLj'*zi_prior + bjbLi'*zj)*t_hat;

        for d = 1:D
            T12(d,1:d) = zi_prior(:,1:d)'*(zi_prior(:,d).*bibLj) + ...
              cbLi(:,1:d)'*(zj(:,d).*beta(:,j)) + zj(:,1:d)'*(zj(:,d).*bjbLi) + ...
              cbLj(:,1:d)'*(zi_prior(:,d).*beta_prior(:,i));
            T12(1:d,d) = T12(d,1:d)';
        end

        zi = ii/R_hat2;
        zj_prior = ij_prior/R_hat2;
        bibLj = Qij_hat2*beta_prior(:,j).*beta(:,i);
        bjbLi = Qij_hat2'*beta(:,i).*beta_prior(:,j);
        cbLi = Qij_hat2'*bsxfun(@times, beta(:,i), zi);
        cbLj = Qij_hat2*bsxfun(@times, beta_prior(:,j), zj_prior);
        beta_a_dQ_hat_alpha_b = (bibLj'*zi + bjbLi'*zj_prior)*t_hat2;
        
        for d = 1:D
            T21(d,1:d) = zi(:,1:d)'*(zi(:,d).*bibLj) + ...
              cbLi(:,1:d)'*(zj_prior(:,d).*beta_prior(:,j)) + ...
              zj_prior(:,1:d)'*(zj_prior(:,d).*bjbLi) + ...
              cbLj(:,1:d)'*(zi(:,d).*beta(:,i));
            T21(1:d,d) = T21(d,1:d)';
        end
        
    end
    
    
    % S is symmetric so no need to separately calc S(j,i)
    S(j,i) = S(i,j);
    
    dSdm(i,j,:) = r - M(i)*dMdm(j,:)-M(j)*dMdm(i,:); % 1xD
    dSdm(j,i,:) = dSdm(i,j,:);
    T = (t*T-Sij1*diag(iell2(:,i)+iell2(:,j))/R)/2;
    T = T - reshape(M(i)*dMds(j,:,:) + M(j)*dMds(i,:,:),D,D);
    if prior_exisits
        r_extra = alpha_dR_alpha + alpha_a_dQ_hat_beta_b + beta_a_dQ_hat_alpha_b;
        dSdm(i,j,:) = r + r_extra - (M(i)*dMdm(j,:)+M(j)*dMdm(i,:)+...
            M_prior(i)*dMdm_prior(j,:) + M_prior(j)*dMdm_prior(i,:) +...
            M(i)*dMdm_prior(j,:) + M_prior(j)*dMdm(i,:) + ...
            M(j)*dMdm_prior(i,:) + M_prior(i)*dMdm(j,:));
        T_prior = (t_prior*T_prior-Sij2*diag(iell2_prior(:,i)+...
            iell2_prior(:,j))/R_prior)/2;
        T12 = (t_hat*T12-Sij3*diag(iell2_prior(:,i)+...
            iell2(:,j))/R_hat)/2;
        T21 = (t_hat2*T21-Sij4*diag(iell2(:,i)+...
            iell2_prior(:,j))/R_hat2)/2;
        T = T + T_prior + T12 + T21 -...
            reshape(M_prior(i)*dMds_prior(j,:,:) + M_prior(j)*dMds_prior(i,:,:) +...
            M(i)*dMds_prior(j,:,:) + M_prior(j)*dMds(i,:,:) + ...
            M(j)*dMds_prior(i,:,:) + M_prior(i)*dMds(j,:,:),D,D);
		dSdm(j,i,:) = dSdm(i,j,:);
    end
    dSds(i,j,:,:) = T; 
    dSds(j,i,:,:) = T;
  end
  
  S(i,i) = S(i,i) + exp(2*X(D+1,i));
end

% add prior mean and input-output variance
if prior_exisits
    M = M + M_prior;
    V = V + V_prior;
end

% 4) centralize moments
S = S - M*M';

if prior_exisits
    dMdm = dMdm + dMdm_prior;
    dMds = dMds + dMds_prior;
    dVdm = dVdm + dVdm_prior;
    dVds = dVds + dVds_prior;
end

% 5) vectorize derivatives
dMds = reshape(dMds,[E D*D]);
dSds = reshape(dSds,[E*E D*D]); dSdm = reshape(dSdm,[E*E D]);
dVds = reshape(dVds,[D*E D*D]); dVdm = reshape(dVdm,[D*E D]);
