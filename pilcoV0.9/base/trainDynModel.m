%% trainDynModel.m
% *Summary:* Script to learn the dynamics model
%
% Copyright (C) 2008-2013 by 
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modification: 2013-05-20
%
%% High-Level Steps
% # Extract states and controls from x-matrix
% # Define the training inputs and targets of the GP
% # Train the GP 

%% Code


% 1. Train GP dynamics model
Du = length(policy.maxU); Da = length(plant.angi); % no. of ctrl and angles
xaug = [x(:,dyno) x(:,end-Du-2*Da+1:end-Du)];     % x augmented with angles
dynmodel.inputs = [xaug(:,dyni) x(:,end-Du+1:end)];     % use dyni and ctrl
dynmodel.targets = y(:,dyno);
dynmodel.targets(:,difi) = dynmodel.targets(:,difi) - x(:,dyno(difi)); % x(k+1) - x(k)
dynmodel.targets(:,plant.angi) = wrapToPi(dynmodel.targets(:,plant.angi));
[n, D] = size(dynmodel.inputs);


if isfield(dynmodel,'prior')
    
    % check if we already have real-world data, load here
    % WARNING- this code is not compatible with angles!
%     if isfield(dynmodel.prior,'gpmodel_rw')
%         dynmodel.inputs = [dynmodel.inputs; dynmodel.prior.gpmodel_rw.inputs];
%         dynmodel.targets = [dynmodel.targets; dynmodel.prior.gpmodel_rw.targets];
%     end
    
    % prior gp mean
    prior_mean = zeros(size(dynmodel.targets(:,difi)));
    for i=1:length(dynmodel.targets)
        prior_mean(i,:) = dynmodel.prior.fcn(dynmodel.prior, dynmodel.inputs(i,:)', zeros(D));
    end
    dynmodel.targets(:,difi) = dynmodel.targets(:,difi) - prior_mean;
    dynmodel.targets(:,plant.angi) = wrapToPi(dynmodel.targets(:,plant.angi));
end


dynmodel = dynmodel.train(dynmodel, plant, trainOpt);  %  train dynamics GP

% display some hyperparameters
Xh = dynmodel.hyp;     
% noise standard deviations
disp(['Learned noise std: ' num2str(exp(Xh(end,:)))]);
% signal-to-noise ratios (values > 500 can cause numerical problems)
disp(['SNRs             : ' num2str(exp(Xh(end-1,:)-Xh(end,:)))]);