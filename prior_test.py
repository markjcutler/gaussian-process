#!/usr/bin/env python

###################################################
# prior_test.py -- quick test to understand the role of priors
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday,  8 August 2014.
###################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
import matplotlib as mpl

axis_tick_fontsize = 21
mpl.rcParams['xtick.labelsize'] = axis_tick_fontsize
mpl.rcParams['ytick.labelsize'] = axis_tick_fontsize

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)

alpha = 0.15
linestyle = ['-', '--', '-.', ':']
legend = [r'Mean', r'Existing Data', r'New Data', r'$p_{rw}$']
legend_fontsize = 23
axis_label_fontsize = 23
linewidth = 1.5
annotation_fontsize = 23


def prior_mean_fun(x):
    return 1*(np.sin(0.8*x) + 0*x/4)


def mean_fun(x):
    return np.sin(0.8*x) + 0*x/4


def kernel(x1, x2):
    l = 2.5
    return np.exp(-0.5/l**2*np.linalg.norm(x1 - x2)**2)

# prior
num_func = 5
n_star = 300
x_star = np.linspace(-20, 20, n_star)
K_star = np.zeros((n_star, n_star))
for i in range(n_star):
    for j in range(n_star):
        K_star[i, j] = kernel(x_star[i], x_star[j])

f_star = np.random.multivariate_normal(prior_mean_fun(x_star), K_star,
                                       num_func)

# training points
x = np.array([-10, -2.5, 5, 7]) # np.linspace(-4.5, 4.5, 5) #np.array([-4, -2, 0, 1, 2])
n = len(x)
f = mean_fun(x)

K = np.zeros((n, n))
K12 = np.zeros((n, n_star))
K21 = np.zeros((n_star, n))
for i in range(n):
    for j in range(n):
        K[i, j] = kernel(x[i], x[j])
    for j in range(n_star):
        K12[i, j] = kernel(x[i], x_star[j])
for i in range(n_star):
    for j in range(n):
        K21[i, j] = kernel(x_star[i], x[j])

mean = prior_mean_fun(x_star) + np.dot(K21, np.dot(np.linalg.inv(K),
                                                   f - prior_mean_fun(x)))
cov = K_star - np.dot(K21, np.dot(np.linalg.inv(K), K12))
#print cov
f_post = np.random.multivariate_normal(mean, cov, num_func)

fig, axarr = plt.subplots(2, sharex=True)
axarr[0].fill_between(x_star, prior_mean_fun(x_star)-1.96*np.sqrt(K_star.diagonal()),
                   prior_mean_fun(x_star)+1.96*np.sqrt(K_star.diagonal()), facecolor='k',
                      edgecolor='k', alpha=alpha)
axarr[1].fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                   mean+1.96*np.sqrt(cov.diagonal()), facecolor='k',
                      edgecolor='k', alpha=alpha)

axarr[0].plot(x_star, prior_mean_fun(x_star), color='k', linewidth=2)
axarr[1].plot(x_star, mean_fun(x_star), color='k', linewidth=2, alpha=0.5)
axarr[1].plot(x_star, mean, color='k', linewidth=2)
for i in range(num_func):
    axarr[0].plot(x_star, f_star[i, :], 'k--', alpha=0.5)
    axarr[1].plot(x_star, f_post[i, :], 'k--', alpha=0.5)
    # for j in range(len(x_star)):
    #     if (f_star[i, j] >= -1.96*np.sqrt(K_star[j, j]) and
    #         f_star[i, j] <= 1.96*np.sqrt(K_star[j, j])):
    #         inside += 1
    #     else:
    #         outside += 1
    #     if (f_post[i, j] >= mean[j]-1.96*np.sqrt(cov[j, j]) and
    #         f_post[i, j] <= mean[j]+1.96*np.sqrt(cov[j, j])):
    #         inside += 1
    #     else:
    #         outside += 1

axarr[1].plot(x, f, color='k', linestyle='None',
                    marker='.', markersize=15)


for ax in axarr:
    ax.set_xlim([-20, 20])
    ax.set_ylim([-4, 4])

    #ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
    ax.set_ylabel('Targets', size=axis_label_fontsize)

            # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

axarr[0].annotate('Prior', xy=(10, 2),
                  xycoords='data', xytext=(0, 0),
                  textcoords='offset points',
                  bbox=dict(boxstyle="round", fc="1.0",
                            edgecolor=axes_ticks_color),
                  fontsize=legend_fontsize)
axarr[1].annotate('Posterior', xy=(10, 2),
                  xycoords='data', xytext=(0, 0),
                  textcoords='offset points',
                  bbox=dict(boxstyle="round", fc="1.0",
                            edgecolor=axes_ticks_color),
                  fontsize=legend_fontsize)
axarr[1].set_xlabel('Inputs', size=axis_label_fontsize)


plt.savefig('/home/mark/gaussian-process/prior.pdf',
            format='pdf', transparent=True, bbox_inches='tight')
