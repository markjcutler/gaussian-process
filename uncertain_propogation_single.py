#!/usr/bin/env python

###################################################
# uncertian_propogation_single.py -- Test whether analytical uncertain propogations match emperical results
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 28 August 2014.
###################################################

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt


def prior_mean_fun(x):
    return 0*(np.sin(0.5*x) + x/4)


def mean_fun(x):
    return np.sin(0.5*x) + x/4


def kernel(x1, x2, sigma_f, l, sigma=0):
    return sigma_f**2/np.sqrt(sigma**2/l**2 + 1)*np.exp(-0.5*(x1 - x2)**2/(l**2 + sigma**2))


def mapping_fun(x):
    return 0.5*x


def K_noise_inv(x, y, sigma_f, l, sigma_w):
    n = len(x)

    K = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            K[i, j] = kernel(x[i], x[j], sigma_f, l)

    return np.linalg.inv(K + sigma_w**2*np.identity(n))


def calc_beta(x, y, sigma_f, l, sigma_w):
    K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
    beta = np.dot(K_inv, y)
    return beta


def gp_post(x_star, x, y, sigma_f, l, sigma_w, comp_cov, cov=0):
    n_star = len(x_star)
    n = len(x)

    K12 = np.zeros((n, n_star))
    for i in range(n):
        for j in range(n_star):
            K12[i, j] = kernel(x[i], x_star[j], sigma_f, l, cov)

    K21 = K12.transpose()
    mean = prior_mean_fun(x_star) + np.dot(K21,
                                           np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w),
                                                  y - prior_mean_fun(x)))
    if comp_cov:
        K_star = np.zeros((n_star, n_star))
        for i in range(n_star):
            for j in range(n_star):
                K_star[i, j] = kernel(x_star[i], x_star[j], sigma_f, l, cov)
        cov = K_star - np.dot(K21, np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w), K12))

        return mean, cov
    else:
        return mean


## Hyperparameters
sigma_w = 0.4
sigma_f = 1.0
l = 2.5

# prior
num_func = 25
n_star = 300
x_star = np.linspace(-15, 15, n_star)

# training points
x = np.linspace(-4.5, 4.5, 5) #np.array([-4, -2, 0, 1, 2])
y = mean_fun(x)
mean, cov = gp_post(x_star, x, y, sigma_f, l, sigma_w, True)


def prop_through_gp(mu, sigma, n_star, num_samples, x, y, sigma_f, l, simga_w, x_uncertain=None):
    ##  TEST INPUT
    if x_uncertain is None:
        if sigma == 0:
            x_uncertain = np.array([mu])
            n_star = 1
        else:
            x_uncertain = np.random.normal(loc=mu, scale=sigma, size=n_star)

    # send uncertain test input through GP
    f_samples = np.zeros(n_star*num_samples)
    for i in range(n_star):
        f_mean, f_cov = gp_post(np.array([x_uncertain[i]]), x, y, sigma_f, l, sigma_w, True)
        f_samples[i*num_samples:i*num_samples+num_samples] = np.random.normal(loc=f_mean, scale=np.sqrt(f_cov), size=num_samples)
    # print f_samples
    f_epdf, bin_edges = np.histogram(f_samples, bins=100)
    y_fepdf = np.linspace(min(f_samples), max(f_samples), 100)
    mean = np.mean(f_samples)
    var = np.var(f_samples, ddof=1)

    return mean, var, x_uncertain, y_fepdf, f_epdf

##  TEST INPUT
n_star = 3000
mu = 0.75
sigma = 1.5
num_samples = 500

emean, evar, x_uncertain, y_fepdf, f_epdf = prop_through_gp(mu, sigma, n_star, num_samples, x, y, sigma_f, l, sigma_w)

epdf, bin_edges = np.histogram(x_uncertain, bins=100)
x_epdf = np.linspace(min(x_uncertain), max(x_uncertain), 100)

# certain mean and variance
certain_mean, certain_var = gp_post(np.array([mu]), x, y, sigma_f, l, sigma_w, True)
print "certain_mean: " + str(certain_mean)
print "certain_var: " + str(certain_var)


def closed_form_mean_var(mu, x, y, sigma_f, l, sigma_w, sigma):
    # compute moment-matched mean and covariance of propagated values
    mean = gp_post(np.array([mu]), x, y, sigma_f, l, sigma_w, False, sigma)

    # compute moment-matched variance
    beta = calc_beta(x, y, sigma_f, l, sigma_w)
    Q = np.zeros((len(beta), len(beta)))
    R = sigma**2*(1/l**2 + 1/l**2) + 1
    T = 1/l**2 + 1/l**2 + 1/sigma**2
    for i in range(len(beta)):
        for j in range(len(beta)):
            zij = 1/l**2*(x[i] - mu) + 1/l**2*(x[j] - mu)
            Q[i, j] = kernel(x[i], mu, sigma_f, l)*kernel(x[j], mu, sigma_f, l)/np.sqrt(R)*np.exp(0.5*zij**2/T)
    part2 = np.dot(beta.transpose(), np.dot(Q, beta))
    K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
    part1 = sigma_f**2 - np.trace(np.dot(K_inv, Q))# + sigma_w**2
    part3 = mean**2
    var = part1 + part2 - part3

    return mean[0], var[0]


def empirical_deriv(mu, x, y, sigma_f, l, simga_w, sigma):
    fdiff = 1e-2
    n_star = 5000
    num_samples = 500

    x_uncertain = np.random.normal(loc=mu, scale=sigma, size=n_star)
    x_uncertain1 = x_uncertain - fdiff
    x_uncertain2 = x_uncertain + fdiff

    m1, v1, x_uncertain, y_fepdf, f_epdf = prop_through_gp(mu-fdiff, sigma, n_star, num_samples, x, y, sigma_f, l, sigma_w, x_uncertain1)
    m2, v2, x_uncertain, y_fepdf, f_epdf = prop_through_gp(mu+fdiff, sigma, n_star, num_samples, x, y, sigma_f, l, sigma_w, x_uncertain2)

    dmdm = (m2-m1)/(2*fdiff)
    dsdm = (v2-v1)/(2*fdiff)
    return dmdm, dsdm


def closed_form_deriv(mu, x, y, sigma_f, l, sigma_w, sigma):
    fdiff = 1e-2
    m1, v1 = closed_form_mean_var(mu-fdiff, x, y, sigma_f, l, sigma_w, sigma)
    m2, v2 = closed_form_mean_var(mu+fdiff, x, y, sigma_f, l, sigma_w, sigma)
    dmdm = (m2-m1)/(2*fdiff)
    dsdm = (v2-v1)/(2*fdiff)
    return dmdm, dsdm

mean_comp, var_comp = closed_form_mean_var(mu, x, y, sigma_f, l, sigma_w, sigma)
dmdm, dsdm = closed_form_deriv(mu, x, y, sigma_f, l, sigma_w, sigma)
edmdm, edsdm = empirical_deriv(mu, x, y, sigma_f, l, sigma_w, sigma)

print "Computed mean: " + str(mean_comp)
print "Empirical mean: " + str(emean)
print "Computed var: " + str(var_comp)
print "Empirical var: " + str(evar)

print "Finite difference dmdm: " + str(dmdm)
print "Finite difference empirical dmdm: " + str(edmdm)

plt.clf()
ax00 = plt.subplot(221)
ax10 = plt.subplot(223, sharex=ax00)
ax01 = plt.subplot(222, sharey=ax00)
ax00.fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                   mean+1.96*np.sqrt(cov.diagonal()), facecolor='grey')
ax00.plot(x_star, mean)
ax10.plot(x_epdf, epdf/np.sum(epdf*(x_epdf[2]-x_epdf[1])))
ax10.plot(x_epdf, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-0.5*(x_epdf-mu)**2/sigma**2))
ax01.plot(f_epdf/np.sum(f_epdf*(y_fepdf[2]-y_fepdf[1])), y_fepdf)
ax01.plot(1/(np.sqrt(2*var_comp*np.pi))*np.exp(-0.5*(y_fepdf - mean_comp)**2/evar), y_fepdf)

#plt.show()
