%---------------------------------------------------%
% Simple Car Problem:                   %
%---------------------------------------------------%

clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
auxdata.L = 0.3;


%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; tf = 3;
x0 = 3.0; 
xf = 0;


%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 4;
xmax = 4; xmin = -4;
velmax = 1; velmin = -1;

%Integral parameters:
integralGuess = 50;
integralBoundLow = 0;
integralBoundUpp = 1000;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tf; 
bounds.phase.finaltime.upper = tf;
bounds.phase.initialstate.lower = x0; 
bounds.phase.initialstate.upper = x0; 
bounds.phase.state.lower = xmin; 
bounds.phase.state.upper = xmax;
bounds.phase.finalstate.lower = xmin; 
bounds.phase.finalstate.upper = xmax;
bounds.phase.control.lower = velmin; 
bounds.phase.control.upper = velmax;
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
guess.phase.time    = [t0; tfmax]; 
guess.phase.state   = [x0; 
                       xf];
guess.phase.control = [0; 0];
guess.phase.integral = integralGuess;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-6;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'SimpleCar';
setup.functions.continuous = @prob1d_Continuous;
setup.functions.endpoint = @prob1d_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    xlabel('time (s)')
    ylabel('x (m)')
    title(['cost: ' num2str(output.result.objective)])
    
subplot(2,1,2); hold on;
    plot(Time, Force(1,:),'k.','MarkerSize',mSize)
    plot(Time, Force(1,:));
    xlabel('time (s)')
    ylabel('dx (m/s)')

    xlabel('time (s)')
    
save('training2.mat','Time','State','Force');
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
