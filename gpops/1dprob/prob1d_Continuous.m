function output = prob1d_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

%Call continuous dynamics function:
Xd = Prob1d_Dynamics_Forced(X',F',P)';

%Get the cost integrand:
costIntegrand = X.^2; % position


%Pack up the output:
output.dynamics = Xd;
output.integrand = costIntegrand;

end