function Xd = Spherical_Pend_Dynamics_Forced(X,F,P_Dyn)
% Xd = Pendulum_Cart_Dynamics_Forced(X,F,P_Dyn)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [p,q,gx,gy,gz]
%   F = [2 x N] actuator force [torque, dir]
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [5 x N] derivative of the state matrix [dp,dq,dgx,dgy,dgz]
%
%

%Define Parameters
m = P_Dyn.massPend;    %Pendulum mass
bx = P_Dyn.frictionCoeffx; % coefficient of friction
by = P_Dyn.frictionCoeffy; % coefficient of friction
l = P_Dyn.lengthPendulum;    %Pendulum length
g = -P_Dyn.gravity;    %Gravity

%Define input states:
p = X(1,:);
q = X(2,:);
gx = X(3,:);
gy = X(4,:);
gz = X(5,:);

torque = F(1,:);
dir = F(2,:);

torquex = torque.*cos(dir);
torquey = torque.*sin(dir);

inertia = m*l^2/3;
gravity_torque_x = m*g*l/2*gx;
gravity_torque_y = m*g*l/2*gy;

dgx = -q.*gz;
dgy =  p.*gz;
dgz = q.*gx - p.*gy;

drag_x_body = bx*dgx;
drag_y_body = by*dgy;

dp = ( torquex + drag_y_body - gravity_torque_y ) / inertia;
dq = ( torquey - drag_x_body + gravity_torque_x ) / inertia;



%Express as a vector
Xd = [dp;dq;dgx;dgy;dgz];

end
