%---------------------------------------------------%
% Spherical Swing-Up Problem:                   %
%---------------------------------------------------%

clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
auxdata.massPend = 1;   %Mass of the pendulum
auxdata.frictionCoeffx = 0*0.01;   %friction x
auxdata.frictionCoeffy = 0*0.01;   %friction y
auxdata.lengthPendulum = 1;   %Set negative to have 0 be the unstable equilibrium
auxdata.gravity = 9.81;   %Length of the pendulum

%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; p0 = 0; q0 = 0; theta0 = pi; phi0 = 0;
gx0 = -sin(theta0);
gy0 = cos(theta0)*sin(phi0);
gz0 = cos(theta0)*cos(phi0);

pf = 0; qf = 0; thetaf = 0; phif = 0;
gxf = -sin(thetaf);
gyf = cos(thetaf)*sin(phif);
gzf = cos(thetaf)*cos(phif);

%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0;                   tfmax = 4;
pmin = -6*pi;      pmax = 6*pi;
qmin = -6*pi;      qmax = 6*pi;
gxmin = -1.1;       gxmax = 1.1;
gymin = -1.1;       gymax = 1.1;
gzmin = -1.1;       gzmax = 1.1;
fmin = -5;          fmax = 5;
dirmin = -pi/2;      dirmax = pi/2;

%Integral parameters:
integralGuess = 50;
integralBoundLow = 0;
integralBoundUpp = 1000;

%-------------------------------------------------------------------%
%--------------- Set Up Problem Using Data Provided Above ----------%
%-------------------------------------------------------------------%
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmin; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = [p0, q0, gx0, gy0, gz0]; 
bounds.phase.initialstate.upper = [p0, q0, gx0, gy0, gz0]; 
bounds.phase.state.lower = [pmin, qmin, gxmin, gymin, gzmin]; 
bounds.phase.state.upper = [pmax, qmax, gxmax, gymax, gzmax]; 
bounds.phase.finalstate.lower = [pf, qf, gxf, gyf, gzf]; 
bounds.phase.finalstate.upper = [pf, qf, gxf, gyf, gzf];
bounds.phase.control.lower = [fmin, dirmin]; 
bounds.phase.control.upper = [fmax, dirmax];
% bounds.phase.integral.lower = integralBoundLow;
% bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
% N = 100;
% tGuess = linspace(0,2,N).';
% pguess = 
guess.phase.time    = [t0; tfmax]; 
guess.phase.state   = [bounds.phase.initialstate.lower; bounds.phase.finalstate.lower];
guess.phase.control = [1 pi/2; 
                       1 pi/2];
guess.phase.integral = integralGuess;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-6;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'Spherical_SwingUp';
setup.functions.continuous = @sphericalPend_Continuous;
setup.functions.endpoint = @sphericalPend_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    xlabel('time (s)')
    ylabel('angle (rad)')
    
subplot(2,1,2); hold on;
    plot(Time, State(2,:),'k.','MarkerSize',mSize)
    plot(Time, State(2,:));
    xlabel('time (s)')
    ylabel('rate (rad/s)')
        
figure(3); clf; hold on
    plot(Time, Force,'k.','MarkerSize',mSize)
    plot(Time, Force);
    title(['cost: ' num2str(output.result.objective)])
    xlabel('time (s)')
    ylabel('torque (N-m)')
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
