function output = sphericalPend_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

%Call continuous dynamics function:
Xd = Spherical_Pend_Dynamics_Forced(X',F',P)';

%Get the cost integrand:   (force-squared for now)
% costIntegrand = F(:,1).^2; %ones(size(F)); %
% costIntegrand = X(:,1).^2; % anguluar position
costIntegrand = F(:,1).^2 + F(:,2).^2; % anguluar position + rate


%Pack up the output:
output.dynamics = Xd;
% output.integrand = costIntegrand;

end