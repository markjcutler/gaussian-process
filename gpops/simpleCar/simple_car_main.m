%---------------------------------------------------%
% Simple Car Problem:                   %
%---------------------------------------------------%

clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
auxdata.L = 0.3;


%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; 
x0 = 0; y0 = 0; psi0 = 0;
xf = 2; yf = 2; psif = -pi/2;


%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 4;
xmax = 6; xmin = -6;
ymax = 6; ymin = -6;
psimax = 3*pi; psimin = -3*pi;
deltamax = 30*pi/180; deltamin = -30*pi/180;
velmax = 2; velmin = 0*-2;

%Integral parameters:
integralGuess = 50;
integralBoundLow = 0;
integralBoundUpp = 1000;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmin; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = [x0, y0, psi0]; 
bounds.phase.initialstate.upper = [x0, y0, psi0]; 
bounds.phase.state.lower = [xmin, ymin, psimin]; 
bounds.phase.state.upper = [xmax, ymax, psimax];
bounds.phase.finalstate.lower = [xf, yf, psif]; 
bounds.phase.finalstate.upper = [xf, yf, psif];
bounds.phase.control.lower = [deltamin, velmin]; 
bounds.phase.control.upper = [deltamax, velmax];
% bounds.phase.integral.lower = integralBoundLow;
% bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
guess.phase.time    = [t0; tfmax]; 
guess.phase.state   = [x0, y0, psi0; 
                       xf, yf, 0];
guess.phase.control = [0 0; 2 0];
% guess.phase.integral = integralGuess;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-6;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'SimpleCar';
setup.functions.continuous = @simpleCar_Continuous;
setup.functions.endpoint = @simpleCar_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    xlabel('time (s)')
    ylabel('x (m)')
    
subplot(2,1,2); hold on;
    plot(Time, State(2,:),'k.','MarkerSize',mSize)
    plot(Time, State(2,:));
    xlabel('time (s)')
    ylabel('y (m)')
    
figure(2); clf;
mSize = 10;
hold on;
    plot(State(1,:),State(2,:),'k.','MarkerSize',mSize)
    plot(State(1,:), State(2,:));
    xlabel('x (m)')
    ylabel('y (m)')
  
        
figure(3); clf; hold on
    plot(Time, Force(1,:),'k.','MarkerSize',mSize)
    plot(Time, Force(1,:));
    plot(Time, Force(2,:),'k.','MarkerSize',mSize)
    plot(Time, Force(2,:));
    title(['cost: ' num2str(output.result.objective)])
    legend('vel', 'psi');
    xlabel('time (s)')
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
