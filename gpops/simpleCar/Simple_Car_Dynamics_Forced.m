function Xd = Simple_Car_Dynamics_Forced(X,F,P_Dyn)
% Xd = Pendulum_Cart_Dynamics_Forced(X,F,P_Dyn)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [p,q,gx,gy,gz]
%   F = [2 x N] actuator force [torque, dir]
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [5 x N] derivative of the state matrix [dp,dq,dgx,dgy,dgz]
%
%
% A detailed derivation of the dynamics can be found in:
% 
% http://planning.cs.uiuc.edu/node658.html
%

%Define input states:
% x = X(1,:);
% y = X(2,:);
psi = X(3,:);

delta = F(1,:);
vel = F(2,:);


% Settings for car
param.L = P_Dyn.L;

% Calculate derivatives
dx = vel.*cos(psi);
dy = vel.*sin(psi);
dpsi = vel./param.L.*tan(delta);

Xd = [dx;dy;dpsi];