%---------------------------------------------------%
% Simple Car Problem:                   %
%---------------------------------------------------%

clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
auxdata.B = 7;
auxdata.C = 1.6;
auxdata.D = 1.0;
auxdata.rF = 0.3;
auxdata.rR = auxdata.rF;
auxdata.lF = 1.1;
auxdata.lR = 1.59;
auxdata.m = 1450;
auxdata.Iz = 2741.9;
auxdata.IwF = 1.8;
auxdata.IwR = auxdata.IwF;
auxdata.h = 0.4;
auxdata.muW = 0;		% guess
auxdata.MaxTorque = 3000;    % guess
auxdata.MaxWheelAngle = 30*pi/180.0; % guess

% steady-state drift conditions
beta_ss = -43*pi/180; % rad
V_ss = 3.3879; % m/s
R_ss = 1.5; % m
omegaF_ss = 8.8479;
omegaR_ss = 32.624;
dpsi_ss = V_ss/R_ss;
Vx_ss = V_ss*cos(beta_ss);
Vy_ss = V_ss*sin(beta_ss);

auxdata.Vx_ss = Vx_ss;
auxdata.Vy_ss = Vy_ss;
auxdata.dpsi_ss = dpsi_ss;


%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; tf = 1.5;
Vx0 = 0.1; Vy0 = 0; dpsi0 = 0;
Vxf = Vx_ss; Vyf = Vy_ss; dpsif = 0.2*dpsi_ss;


%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 4;
Vxmax = 6; Vxmin = 0.05;
Vymax = 5; Vymin = -5;
dpsimax = 5; dpsimin = -5;
sFxmax = 1; sFxmin = -1;
sRxmax = 1; sRxmin = -1;
deltamax = auxdata.MaxWheelAngle; deltamin = -deltamax;

%Integral parameters:
integralGuess = 1e6;
integralBoundLow = 0;
integralBoundUpp = 1e7;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tf; 
bounds.phase.finaltime.upper = tf;
bounds.phase.initialstate.lower = [Vx0, Vy0, dpsi0]; 
bounds.phase.initialstate.upper =  [Vx0, Vy0, dpsi0]; 
bounds.phase.state.lower = [Vxmin, Vymin, dpsimin]; 
bounds.phase.state.upper = [Vxmax, Vymax, dpsimax];
% bounds.phase.finalstate.lower = [2.4, -2.9, 2.2]; 
% bounds.phase.finalstate.upper = [2.9, -2.3, 2.9];
bounds.phase.finalstate.lower = [1.0, Vymin, dpsimin]; 
bounds.phase.finalstate.upper = [1.0, Vymax, dpsimax];
bounds.phase.control.lower = [sFxmin, sRxmin, deltamin]; 
bounds.phase.control.upper = [sFxmax, sRxmax, deltamax];
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
guess.phase.time    = [t0; tfmax]; 
guess.phase.state   = [Vx0, Vy0, dpsi0; 
                       Vxf, 0,   0];
guess.phase.control = [0.1 0.1 0; 
                       0.1 0.1 0.3];
guess.phase.integral = integralGuess;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-3; %1e-6;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'SimpleCar';
setup.functions.continuous = @simpleCar_Continuous;
setup.functions.endpoint = @simpleCar_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(3,1,1)
hold on;
plot(Time, State(1,:),'k.','MarkerSize',mSize)
plot(Time, State(1,:));
ylabel('Vx (m/s)')

subplot(3,1,2)
hold on;
plot(Time, State(2,:),'k.','MarkerSize',mSize)
plot(Time, State(2,:));
ylabel('Vy (m/s)')

subplot(3,1,3)
hold on;
plot(Time, State(3,:),'k.','MarkerSize',mSize)
plot(Time, State(3,:));
ylabel('dpsi (rad/s)')
xlabel('time (s)')

figure(2); clf; hold all
plot(Time, Force(3,:)*180/pi,'k.','MarkerSize',mSize)
plot(Time, Force(3,:)*180/pi);
ylabel('delta (deg)');
xlabel('time (s)')
        
figure(3); clf; hold all
plot(Time, Force(1,:),'k.','MarkerSize',mSize)
plot(Time, Force(1,:));
plot(Time, Force(2,:),'k.','MarkerSize',mSize)
plot(Time, Force(2,:));
title(['cost: ' num2str(output.result.objective)])
legend('sFx', 'sRx');
xlabel('time (s)')
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
