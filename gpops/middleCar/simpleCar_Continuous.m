function output = simpleCar_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

%Call continuous dynamics function:
Xd = Simple_Car_Dynamics_Forced(X',F',P)';

%Get the cost integrand:   (force-squared for now)
% costIntegrand = F.^2; %ones(size(F)); %
% costIntegrand = X(:,1).^2; % anguluar position
% costIntegrand = X(:,1).^2 + X(:,2).^2; % anguluar position + rate

% costIntegrand = (X(:,1) - P.Vx_s s).^2;
% costIntegrand = (X(:,3) - P.dpsi_ss).^2;
% costIntegrand = (X(:,2) - P.Vy_ss).^2;

% costIntegrand = F(:,3).^2;
costIntegrand = F(:,1).^2 + F(:,2).^2;

% costIntegrand = (X(:,1) - P.Vx_ss).^2 + (X(:,3) - P.dpsi_ss).^2;

% costIntegrand = (X(:,1) - P.Vx_ss).^2 + (X(:,2) - P.Vy_ss).^2 + ...
%     (X(:,3) - P.dpsi_ss).^2;


%Pack up the output:
output.dynamics = Xd;
output.integrand = costIntegrand;
% max(costIntegrand)

end