function Xd = Simple_Car_Dynamics_Forced(X,F,P_Dyn)
% Xd = Pendulum_Cart_Dynamics_Forced(X,F,P_Dyn)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [p,q,gx,gy,gz]
%   F = [2 x N] actuator force [torque, dir]
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [5 x N] derivative of the state matrix [dp,dq,dgx,dgy,dgz]
%
%
% A detailed derivation of the dynamics can be found in:
% 
% http://planning.cs.uiuc.edu/node658.html
%

% %Define input states:
% % x = X(1,:);
% % y = X(2,:);
% psi = X(3,:);
% 
% delta = F(1,:);
% vel = F(2,:);
% 
% 
% % Settings for car
% param.L = P_Dyn.L;
% 
% % Calculate derivatives
% dx = vel.*cos(psi);
% dy = vel.*sin(psi);
% dpsi = vel./param.L.*tan(delta);
% 
% Xd = [dx;dy;dpsi];


%Define Parameters
param.B = P_Dyn.B;
param.C = P_Dyn.C;
param.D = P_Dyn.D;
param.rF = P_Dyn.rF;
param.rR = P_Dyn.rR;
param.lF = P_Dyn.lF;
param.lR = P_Dyn.lR;
param.m = P_Dyn.m;
param.Iz = P_Dyn.Iz;
param.IwF = P_Dyn.IwF;
param.IwR = P_Dyn.IwR;
param.h = P_Dyn.h;
param.muW = P_Dyn.muW;		% guess
param.MaxTorque = P_Dyn.MaxTorque;    % guess
param.MaxWheelAngle = P_Dyn.MaxWheelAngle; % guess

%Define input states:
Vx = X(1,:);
Vy = X(2,:);
dpsi = X(3,:);
% omega = X(4,:);

% Vx = X(1,:);
% Vy = zeros(size(Vx));
% dpsi = zeros(size(Vx));

s_Fx = F(1,:);
s_Rx = F(2,:);
delta = F(3,:);

% delta = zeros(size(s_Fx));

% Calculate component velocities
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2+Vy.^2);

V_Fx = V.*cos(beta - delta) + dpsi*param.lF.*sin(delta);
V_Fy = V.*sin(beta - delta) + dpsi*param.lF.*cos(delta);
V_Rx = V.*cos(beta);
V_Ry = V.*sin(beta) - dpsi*param.lR;

% Calculate friction coefficients
s_Fy = V_Fy./V_Fx;
s_F = sqrt(s_Fx.^2+s_Fy.^2);

s_Ry = V_Ry./V_Rx;
s_R = sqrt(s_Rx.^2+s_Ry.^2);

mu_Fx = zeros(size(s_F)); mu_Fy = zeros(size(s_F));
for i=1:length(mu_Fx)
    if (s_F(i) ~= 0)
%     if (abs(s_F(i)) > 0.0001)
        mu_Fx(i) = -s_Fx(i)/s_F(i)*MF(param.B, param.C, param.D, s_F(i));
        mu_Fy(i) = -s_Fy(i)/s_F(i)*MF(param.B, param.C, param.D, s_F(i));
    end
end

mu_Rx = zeros(size(s_R)); mu_Ry = zeros(size(s_R));
for i=1:length(mu_Rx)
    if (s_R(i) ~= 0)
%     if (abs(s_R(i)) > 0.0001)
        mu_Rx(i) = -s_Rx(i)/s_R(i)*MF(param.B, param.C, param.D, s_R(i));
        mu_Ry(i) = -s_Ry(i)/s_R(i)*MF(param.B, param.C, param.D, s_R(i));
    end
end


% Calculate friction forces
GRAVITY = 9.81;
num = param.lR*param.m*GRAVITY - param.h*param.m*GRAVITY*mu_Rx;
den = param.h*(mu_Fx.*cos(delta) - mu_Fy.*sin(delta) - mu_Rx);
f_Fz = num./(param.lF + param.lR + den);
f_Rz = param.m*GRAVITY - f_Fz;
% f_Fz = param.m*GRAVITY*param.lR./(param.lR + param.lF);
% f_Rz = param.m*GRAVITY*param.lF./(param.lR + param.lF);
f_Fx = mu_Fx.*f_Fz;
f_Fy = mu_Fy.*f_Fz;
f_Rx = mu_Rx.*f_Rz;
f_Ry = mu_Ry.*f_Rz;

% System dynamics
Vxdot = 1/param.m*(param.m*Vy.*dpsi + f_Fx.*cos(delta) - f_Fy.*sin(delta) + f_Rx); % body x velocity
Vydot = 1/param.m*(-param.m*Vx.*dpsi + f_Fx.*sin(delta) + f_Fy.*cos(delta) + f_Ry); % body y velocity
dpsidot = 1/param.Iz*((f_Fy.*cos(delta) + f_Fx.*sin(delta))*param.lF - f_Ry*param.lR); % yaw rate

% Xd = [Vxdot;Vydot;dpsidot;wdot];
Xd = [Vxdot;Vydot;dpsidot];

function ret = MF( B, C, D, s)
ret = D*sin(C*atan(B*s));