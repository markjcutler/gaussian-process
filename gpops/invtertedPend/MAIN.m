%---------------------------------------------------%
% Pendulum Cart Swing-Up Problem:                   %
%---------------------------------------------------%

clear; clc; 

%Load solution from file?    ( '' for none )
guessFile = ''; %'oldSoln.mat'; %  'oldSoln.mat'  OR   ''

%Physical parameters
auxdata.massPend = 1;   %Mass of the pendulum
auxdata.frictionCoeff = 0.01;   %Mass of the cart
auxdata.lengthPendulum = 1;   %Set negative to have 0 be the unstable equilibrium
auxdata.gravity = 9.81;   %Length of the pendulum

%Timing parameters:
t0 = 0; 
tfmin = 0; tfmax = 4;

%State parameters [angle; rate];
stateInitial =   [pi,   0]; %Initial state [th,w];
stateFinal =     [0,    0];  %Final state [th,w];
stateBoundLow = -[3*pi, 4*pi];  %[th,w]
stateBoundUpp =  [3*pi, 4*pi];  %[th,w]

%Actuator parameters:
forceBoundLow = -2.5;    %(N-m)
forceBoundUpp =  2.5;    %(N-m)

%Integral parameters:
integralGuess = 50;
integralBoundLow = 0;
integralBoundUpp = 1000;

%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmin; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = stateInitial; 
bounds.phase.initialstate.upper = stateInitial; 
bounds.phase.state.lower = stateBoundLow; 
bounds.phase.state.upper = stateBoundUpp; 
bounds.phase.finalstate.lower = stateFinal; 
bounds.phase.finalstate.upper = stateFinal; 
bounds.phase.control.lower = forceBoundLow; 
bounds.phase.control.upper = forceBoundUpp;
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%

if strcmp(guessFile, '')  %Then use defaults
    guess.phase.time    = [t0; tfmax]; 
    guess.phase.state   = [stateInitial; stateFinal];
    guess.phase.control = [0; 0];
    guess.phase.integral = integralGuess;
else %Load from a data file:
    load(guessFile);
    guess.phase.time = outputPrev.result.solution.phase(1).time;
    guess.phase.state = outputPrev.result.solution.phase(1).state;
    guess.phase.control = outputPrev.result.solution.phase(1).control;
    guess.phase.integral = outputPrev.result.objective;
    
    %Use a mesh that matches with input:
    Npts = length(guess.phase.time);
    nColPts = 4;
    nInterval = floor(Npts/nColPts);
    
    setup.mesh.colpoints = nColPts*ones(1,nInterval);
    setup.mesh.phase(1).fraction = ones(1,nInterval)/nInterval;
    
end

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'InvertedPend_SwingUp';
setup.functions.continuous = @invertedPend_Continuous;
setup.functions.endpoint = @invertedPend_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.mesh.method = 'hp-PattersonRao';
setup.mesh.tolerance = 1e-8;
setup.mesh.maxiteration = 45;
setup.mesh.colpointsmin = 4;
setup.mesh.colpointsmax = 10;
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    xlabel('time (s)')
    ylabel('angle (rad)')
    
subplot(2,1,2); hold on;
    plot(Time, State(2,:),'k.','MarkerSize',mSize)
    plot(Time, State(2,:));
    xlabel('time (s)')
    ylabel('rate (rad/s)')
        
figure(3); clf; hold on
    plot(Time, Force,'k.','MarkerSize',mSize)
    plot(Time, Force);
    title(['cost: ' num2str(output.result.objective)])
    xlabel('time (s)')
    ylabel('torque (N-m)')
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
