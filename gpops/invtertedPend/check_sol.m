clear all; close all;

% check that the solution found by gpops made sense
load('sol.mat')

   
%Physical parameters
auxdata.massPend = 1;   %Mass of the pendulum
auxdata.frictionCoeff = 0*0.01;   %Mass of the cart
auxdata.lengthPendulum = 1;   %Set negative to have 0 be the unstable equilibrium
auxdata.gravity = 9.81;   %Length of the pendulum
    
s_check = zeros(size(State));
s_check(:,1) = State(:,1); % initial condition

% do euler integration using forcing function
dt = Time(1);
for t=1:length(Time)-1
    ds = Inverted_Pend_Dynamics_Forced(s_check(:,t), Force(t), auxdata);
    s_check(:,t+1) = s_check(:,t) + ds*dt;
    dt = Time(t+1) - Time(t);
end

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold all;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    plot(Time, s_check(1,:));
    xlabel('time (s)')
    ylabel('angle (rad)')
    
subplot(2,1,2); hold on;
    plot(Time, State(2,:),'k.','MarkerSize',mSize)
    plot(Time, State(2,:));
    plot(Time, s_check(2,:));
    xlabel('time (s)')
    ylabel('rate (rad/s)')
        
figure(2); clf; hold on
    plot(Time, Force,'k.','MarkerSize',mSize)
    plot(Time, Force);
    xlabel('time (s)')
    ylabel('torque (N-m)')