function output = invertedPend_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

%Call continuous dynamics function:
Xd = Inverted_Pend_Dynamics_Forced(X',F',P)';

%Get the cost integrand:   (force-squared for now)
% costIntegrand = F.^2; %ones(size(F)); %
costIntegrand = X(:,1).^2; % anguluar position
% costIntegrand = X(:,1).^2 + X(:,2).^2; % anguluar position + rate


%Pack up the output:
output.dynamics = Xd;
output.integrand = costIntegrand;

end