%---------------------------------------------------%
% Get car steady-state values                       %
%---------------------------------------------------%

function steady_state_comp
clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
% auxdata.B = 7;
% auxdata.C = 1.6;
% auxdata.D = 1.0;
% auxdata.B = 4;
% auxdata.C = 2;
% auxdata.D = .1;
% auxdata.rF = 0.3;
% auxdata.rR = auxdata.rF;
% auxdata.lF = 1.1;
% auxdata.lR = 1.59;
% auxdata.m = 1450;
% auxdata.Iz = 2741.9;
% auxdata.IwF = 1.8;
% auxdata.IwR = auxdata.IwF;
% auxdata.h = 0.4;
% auxdata.muW = 0;		% guess
% auxdata.MaxTorque = 3000;    % guess
% auxdata.MaxWheelAngle = 25*pi/180.0; % guess

auxdata.B = 3;
auxdata.C = 1.5;
auxdata.D = .2;
auxdata.rF = 0.0303;
auxdata.rR = auxdata.rF;
auxdata.lF = 0.1;
auxdata.lR = 0.107;
auxdata.m = 1.143;
auxdata.Iz = 1.0 / 12.0 * auxdata.m ...
           * (0.1 * 0.1 + 0.2 * 0.2);
auxdata.IwF = 0.8*3.2e-5;
auxdata.IwR = auxdata.IwF;
auxdata.h = 0.02794;
auxdata.muW = 0;		% guess
auxdata.BackEMFGain_up = 20.0; % estimated using step data from none-loaded wheels
auxdata.TorqueOffset = 0.115; % estimated using step data from none-loaded wheels
auxdata.TorqueMin = 0.12; % estimated using step data from none-loaded wheels
auxdata.MaxWheelAngle = 18*pi/180.0; % guess


%-------------------------------------------------------------------%
%----------------------- Control Point -----------------------%
%-------------------------------------------------------------------%
beta_ss = -60*pi/180; % rad
R_ss = 0.5; % m

V_ss = 3.3879; % m/s
omegaF_ss = 8.8479;
omegaR_ss = 32.624;
dpsi_ss = V_ss/R_ss;
Vx_ss = V_ss*cos(beta_ss);
Vy_ss = V_ss*sin(beta_ss);

delta_ss = 8.27*pi/180;
T_ss = 1232.4*2; %1262*2;

x0 = [0.9356, 0.5157, 2.5385, 20.1817, 0.4363, 171.9008];
% x0 = [3.4, 2.3, 8.8, 32, .14, 2500];
lb = [0, 0, 0, 0, 0, 0];
ub = [Inf, Inf, Inf, Inf, auxdata.MaxWheelAngle, 1];


% % Construct a GlobalSearch object
% gs = GlobalSearch;
% % Construct a MultiStart object based on our GlobalSearch attributes
% ms = MultiStart;

% rng(4,'twister') % for reproducibility


f = @(x) find_vals(x, R_ss, beta_ss, auxdata);
options = optimoptions('fsolve','Display','iter', 'MaxIter', 10000, 'MaxFunEvals', 100000);
% [out, fval] = fsolve(f, x0, options)
[out, fval] = lsqnonlin(f, x0, lb, ub, options)

% problem = createOptimProblem('lsqnonlin','x0',x0,'objective',f,...
%     'lb',lb,'ub',ub);
% ms = MultiStart('PlotFcns',@gsplotbestf);
% [xmulti,errormulti] = run(ms,problem,50)


% xmulti
V_ss = out(1)
dpsi_ss = out(2)
omegaF_ss = out(3)
omegaR_ss = out(4)
delta_ss = out(5)
T_ss = out(6)

% xtest = [V_ss, dpsi_ss, omegaF_ss, omegaR_ss, delta_ss, T_ss];
% find_vals(xtest, R_ss, beta_ss, auxdata)


xdot = find_vals(x0, R_ss, beta_ss, auxdata)

function xdot = find_vals(x, R, beta, param)

V = x(1);
dpsi = x(2);
omegaF = x(3);
omegaR = x(4);
delta = x(5);
T = x(6);

xdot = car_dynamics(V*cos(beta), V*sin(beta), dpsi, omegaF, omegaR, delta, T, param);
xdot = [xdot; R*dpsi-V];