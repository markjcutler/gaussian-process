function dw = wheelspeed(t,w,f_x,Tt,T)

MaxWheelSpeed = 352; 
BackEMFGain_up = 9.0; 
BackEMFGain_down = 1.4;
Tmin = 0.12;
Iw = 0.8*3.2e-5;
r = 0.0606 / 2.0;

if nargin > 3
    T = interp1(Tt,T,t);
else
    T = t;
end

K = TorqueGain(T, Tmin);
if (T>0 && K~=0)
    K = K/T;
else
    K = 0;
end

dw = K*BackEMFGain_up*T - BackEMFGain_up*w - 1/Iw*f_x*r;

if w > MaxWheelSpeed && dw > 0
    dw = 0;
end

if dw < 0
    dw = K*BackEMFGain_down*T - BackEMFGain_down*w - 1/Iw*f_x*r;
end


function K = TorqueGain(t, t_min)

if t > 0.55
    K = 75*t + 290;
else
    K = 3.79e3*t^3 - 5.41e3*t^2 + 2.66e3*t - 1.29e2;
end

if t < t_min
    K = 0;
end