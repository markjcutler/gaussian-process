%---------------------------------------------------%
% Spherical Swing-Up Problem:                   %
%---------------------------------------------------%

clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
% auxdata.B = 7;
% auxdata.C = 1.6;
% auxdata.D = 1.0;
auxdata.B = 4;
auxdata.C = 2;
auxdata.D = .1;
auxdata.rF = 0.3;
auxdata.rR = auxdata.rF;
auxdata.lF = 1.1;
auxdata.lR = 1.59;
auxdata.m = 1450;
auxdata.Iz = 2741.9;
auxdata.IwF = 1.8;
auxdata.IwR = auxdata.IwF;
auxdata.h = 0.4;
auxdata.muW = 0;		% guess
auxdata.MaxTorque = 1000;    % guess
auxdata.MaxWheelAngle = 25*pi/180.0; % guess


%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; 
Vx0 = 0.5; Vy0 = 0; dpsi0 = 0; omegaF0 = Vx0/auxdata.rF+0.1; omegaR0 = Vx0/auxdata.rR+0.1;
Vxf = 2;

% beta_ss = -43*pi/180; % rad
% V_ss = 3.3879; % m/s
% R_ss = 1.5; % m
% omegaF_ss = 8.8479;
% omegaR_ss = 32.624;
% dpsi_ss = V_ss/R_ss;
% Vx_ss = V_ss*cos(beta_ss);
% Vy_ss = V_ss*sin(beta_ss);


%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 3;
Vxmax = 6; Vxmin = 0.05;
Vymax = 5; Vymin = -5;
dpsimax = 5; dpsimin = -5;
omegaFmax = 400; omegaFmin = Vxmin/auxdata.rF;
omegaRmax = 400; omegaRmin = Vxmin/auxdata.rF;
deltamax = auxdata.MaxWheelAngle; deltamin = -auxdata.MaxWheelAngle;
torquemax = auxdata.MaxTorque; torquemin = 0;

%Integral parameters:
integralGuess = 1e6;
integralBoundLow = 0;
integralBoundUpp = 1e7;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmax; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = [Vx0, Vy0, dpsi0, omegaF0, omegaR0]; 
bounds.phase.initialstate.upper = [Vx0, Vy0, dpsi0, omegaF0, omegaR0]; 
bounds.phase.state.lower = [Vxmin, Vymin, dpsimin, omegaFmin, omegaRmin]; 
bounds.phase.state.upper = [Vxmax, Vymax, dpsimax, omegaFmax, omegaRmax];
% bounds.phase.finalstate.lower = [0.6, -0.7, 0.6, omegaFmin, omegaRmin]; 
% bounds.phase.finalstate.upper = [0.7, 0.6, 0.7, omegaFmax, omegaRmax];
bounds.phase.finalstate.lower = [Vxmin, -0.7, 0.6, omegaFmin, omegaRmin]; 
bounds.phase.finalstate.upper = [Vxmax, -0.6, 0.7, omegaFmax, omegaRmax];
bounds.phase.control.lower = [deltamin, torquemin]; 
bounds.phase.control.upper = [deltamax, torquemax];
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;
setup.nlp.ipoptoptions.tolerance = 1e-3;
setup.nlp.ipoptoptions.maxiterations = 500;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
guess.phase.time    = [t0; tfmax]; 
guess.phase.state   = [Vx0, Vy0, dpsi0, omegaF0, omegaR0; 
                       Vxf, 0, 0, Vxf/auxdata.rF, Vxf/auxdata.rR];
guess.phase.control = [0 0.1; .3 .1];
guess.phase.integral = integralGuess;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-1;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'Car_';
setup.functions.continuous = @car_Continuous;
setup.functions.endpoint = @car_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

Vx = State(1,:); Vy = State(2,:); dpsi = State(3,:);
omegaF = State(4,:); omegaR = State(5,:);
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2 + Vy.^2);

delta = Force(1,:);
torque = Force(2,:);

figure(1); clf;
mSize = 10;
subplot(3,1,1); hold on;
    plot(Time, V,'k.','MarkerSize',mSize)
    plot(Time, V);
    xlabel('time (s)')
    ylabel('V (m/s)')
    
subplot(3,1,2); hold on;
    plot(Time, beta*180/pi,'k.','MarkerSize',mSize)
    plot(Time, beta*180/pi);
    xlabel('time (s)')
    ylabel('beta (deg)')
    
subplot(3,1,3); hold on;
    plot(Time, dpsi*180/pi,'k.','MarkerSize',mSize)
    plot(Time, dpsi*180/pi);
    xlabel('time (s)')
    ylabel('rate (deg/s)')

figure(3); clf; hold all
plot(Time, omegaF, 'k.','MarkerSize',mSize)
plot(Time, omegaF);
plot(Time, omegaR, 'k.','MarkerSize',mSize)
plot(Time, omegaR);
    xlabel('time (s)')
    ylabel('omega (rad/s)')
    
figure(2); clf; hold all
subplot(2,1,1)
    plot(Time, delta*180/pi,'k.','MarkerSize',mSize)
    plot(Time, delta*180/pi);
    xlabel('time (s)')
    ylabel('delta (deg)')
    
    subplot(2,1,2)
    plot(Time, torque,'k.','MarkerSize',mSize)
    plot(Time, torque);
    xlabel('time (s)')
    ylabel('torque (N-m)')
    
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
