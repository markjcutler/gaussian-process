% prop_car.m

clear all; clc;


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
% auxdata.B = 7;
% auxdata.C = 1.6;
% auxdata.D = 1.0;
auxdata.B = 4;
auxdata.C = 2;
auxdata.D = .1;
auxdata.rF = 0.3;
auxdata.rR = auxdata.rF;
auxdata.lF = 1.1;
auxdata.lR = 1.59;
auxdata.m = 1450;
auxdata.Iz = 2741.9;
auxdata.IwF = 1.8;
auxdata.IwR = auxdata.IwF;
auxdata.h = 0.4;
auxdata.muW = 0;		% guess
auxdata.MaxTorque = 3000;    % guess
auxdata.MaxWheelAngle = 45*pi/180.0; % guess


%-------------------------------------------------------------------%
%----------------------- Initial Conditions -----------------------%
%-------------------------------------------------------------------%
beta_ss = -43*pi/180; % rad
V_ss = 0.9356; % m/s
R_ss = 1.5; % m
omegaF_ss = 2.5385;
omegaR_ss = 20.1817;
dpsi_ss = V_ss/R_ss;
Vx_ss = V_ss*cos(beta_ss);
Vy_ss = V_ss*sin(beta_ss);


% Vx0 = -10; Vy0 = 0; dpsi0 = 0; omegaF0 = 0; omegaR0 = 0;
Vx0 = Vx_ss; Vy0 = Vy_ss; dpsi0 = dpsi_ss; omegaF0 = omegaF_ss; omegaR0 = omegaR_ss;
delta0 = 0.4363;
torque0 = 171.9008;
% delta0 = 20*pi/180;
% torque0 = 3000;
x0 = [Vx0, Vy0, dpsi0, omegaF0, omegaR0];
f0 = [delta0, torque0];



tspan = [0 1.5];
[T Y] = ode45(@(t,y) car_dynamics_ode(t,y,f0,auxdata),tspan,x0); % Solve ODE

%%
figure(1); clf;
plot(T,Y(:,1));
hold all
plot(T,Y(:,2));
plot(T,Y(:,3));
legend('Vx','Vy','dpsi');

figure(2); clf;
plot(T,Y(:,4));
hold all
plot(T,Y(:,5));
legend('omegaF','omegaR');