function Xd = car_dynamics(Vx, Vy, dpsi, omega, delta, torque, param)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [p,q,gx,gy,gz]
%   F = [2 x N] actuator force [torque, dir]
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [5 x N] derivative of the state matrix [dp,dq,dgx,dgy,dgz]
%
%

omegaF = omega;
omegaR = omega;

% Calculate component velocities
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2+Vy.^2);

V_Fx = V.*cos(beta - delta) + dpsi*param.lF.*sin(delta);
V_Fy = V.*sin(beta - delta) + dpsi*param.lF.*cos(delta);
V_Rx = V.*cos(beta);
V_Ry = V.*sin(beta) - dpsi*param.lR;

% Calculate friction coefficients
s_Fx = xSlip(V_Fx,omegaF,param.rF);
s_Fy = ySlip(V_Fy,omegaF,param.rF);
s_F = sqrt(s_Fx.^2+s_Fy.^2);

s_Rx = xSlip(V_Rx,omegaR,param.rR);
s_Ry = ySlip(V_Ry,omegaR,param.rR);
s_R = sqrt(s_Rx.^2+s_Ry.^2);


mu_Fx = -s_Fx./s_F.*MF(param.B, param.C, param.D, s_F);
mu_Fy = -s_Fy./s_F.*MF(param.B, param.C, param.D, s_F);
mu_Fx(isinf(mu_Fx))=0;
mu_Fx(isnan(mu_Fx))=0;
mu_Fy(isinf(mu_Fy))=0;
mu_Fy(isnan(mu_Fy))=0;

mu_Rx = -s_Rx./s_R.*MF(param.B, param.C, param.D, s_R);
mu_Ry = -s_Ry./s_R.*MF(param.B, param.C, param.D, s_R);
mu_Rx(isinf(mu_Rx))=0;
mu_Rx(isnan(mu_Rx))=0;
mu_Ry(isinf(mu_Ry))=0;
mu_Ry(isnan(mu_Ry))=0;


% Calculate friction forces
GRAVITY = 9.81;
num = param.lR*param.m*GRAVITY - param.h*param.m*GRAVITY*mu_Rx;
den = param.h*(mu_Fx.*cos(delta) - mu_Fy.*sin(delta) - mu_Rx);
f_Fz = num./(param.lF + param.lR + den);
f_Rz = param.m*GRAVITY - f_Fz;
% f_Fz = param.m*GRAVITY*param.lR./(param.lR + param.lF);
% f_Rz = param.m*GRAVITY*param.lF./(param.lR + param.lF);
f_Fx = mu_Fx.*f_Fz;
f_Fy = mu_Fy.*f_Fz;
f_Rx = mu_Rx.*f_Rz;
f_Ry = mu_Ry.*f_Rz;

% System dynamics
Vxdot = 1/param.m*(param.m*Vy.*dpsi + f_Fx.*cos(delta) - f_Fy.*sin(delta) + f_Rx); % body x velocity
Vydot = 1/param.m*(-param.m*Vx.*dpsi + f_Fx.*sin(delta) + f_Fy.*cos(delta) + f_Ry); % body y velocity
dpsidot = 1/param.Iz*((f_Fy.*cos(delta) + f_Fx.*sin(delta))*param.lF - f_Ry*param.lR); % yaw rate
% wFdot = 1/param.IwF*(torque/2 - f_Fx*param.rF);
% wRdot = 1/param.IwR*(torque/2 - f_Rx*param.rR);

wFdot = wheelspeed(torque, omegaF, (f_Fx+f_Rx)/2);
wRdot = wFdot;

Xd = [Vxdot;Vydot;dpsidot;wFdot;wRdot];


function slip = xSlip( Vx, omega, r)
slip = (Vx - omega*r)./abs(omega*r);
slip(isinf(slip))=0;
slip(isnan(slip))=0;

function slip = ySlip( Vy, omega, r)
slip = Vy./abs(omega*r);
slip(isinf(slip))=0;
slip(isnan(slip))=0;

function ret = MF( B, C, D, s)
ret = D*sin(C*atan(B*s));
