function Xd = car_dynamics_ode(t,X,F,param)
%
%Define input states:
Vx = X(1);
Vy = X(2);
dpsi = X(3);
omegaF = X(4);
omegaR = X(5);

delta = F(1);
torque = F(2);

Xd = car_dynamics(Vx, Vy, dpsi, omegaF, delta, torque, param);