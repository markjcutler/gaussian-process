%---------------------------------------------------%
% Get car steady-state values                       %
%---------------------------------------------------%

function find_ss_values
clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
% Settings for car from paper
% auxdata.B = 7;
% auxdata.C = 1.6;
% auxdata.D = 1.0;
% auxdata.B = 4;
% auxdata.C = 2;
% auxdata.D = .1;
% auxdata.rF = 0.3;
% auxdata.rR = auxdata.rF;
% auxdata.lF = 1.1;
% auxdata.lR = 1.59;
% auxdata.m = 1450;
% auxdata.Iz = 2741.9;
% auxdata.IwF = 1.8;
% auxdata.IwR = auxdata.IwF;
% auxdata.h = 0.4;
% auxdata.muW = 0;		% guess
% auxdata.MaxTorque = 3000;    % guess
% auxdata.MaxWheelAngle = 25*pi/180.0; % guess

% RC03 data
auxdata.B = 3;
auxdata.C = 1.5;
auxdata.D = .2;
auxdata.rF = 0.0303;
auxdata.rR = auxdata.rF;
auxdata.lF = 0.1;
auxdata.lR = 0.107;
auxdata.m = 1.143;
auxdata.Iz = 1.0 / 12.0 * auxdata.m ...
           * (0.1 * 0.1 + 0.2 * 0.2);
auxdata.IwF = 0.8*3.2e-5;
auxdata.IwR = auxdata.IwF;
auxdata.h = 0.02794;
auxdata.muW = 0;		% guess
auxdata.BackEMFGain_up = 20.0; % estimated using step data from none-loaded wheels
auxdata.TorqueOffset = 0.115; % estimated using step data from none-loaded wheels
auxdata.TorqueMin = 0.12; % estimated using step data from none-loaded wheels
auxdata.MaxWheelAngle = 18*pi/180.0; % guess
auxdata.MaxWheelSpeed = 352;          % roughly measured

%-------------------------------------------------------------------%
%----------------------- Control Point -----------------------%
%-------------------------------------------------------------------%
T_ss = 0.5; % rad
R_ss = 1.0; % m

% x0 = [Vx, Vy, omega, delta]
x0 = [1.0917   -2.2894  127.5719    0.453];
lb = [0, -50, 0, 0];
ub = [50, 0, auxdata.MaxWheelSpeed, 1];


% % Construct a GlobalSearch object
% gs = GlobalSearch;
% % Construct a MultiStart object based on our GlobalSearch attributes
% ms = MultiStart;

% rng(4,'twister') % for reproducibility


f = @(x) find_vals(x, T_ss, R_ss, auxdata);
options = optimoptions('fsolve','Display','iter', 'MaxIter', 10000, 'MaxFunEvals', 100000);
[out, fval] = fsolve(f, x0, options)
% [out, fval] = lsqnonlin(f, x0, lb, ub, options)

% problem = createOptimProblem('lsqnonlin','x0',x0,'objective',f,...
%     'lb',lb,'ub',ub);
% ms = MultiStart('PlotFcns',@gsplotbestf);
% [xmulti,errormulti] = run(ms,problem,50)


% xmulti
Vx_ss = out(1)
Vy_ss = out(2)
V_ss = sqrt(Vx_ss^2 + Vy_ss^2)
dpsi_ss = V_ss/R_ss
omega_ss = out(3)
delta_ss = out(4)
T_ss

beta_ss = atan2(Vy_ss, Vx_ss)*180/pi

% xtest = [V_ss, dpsi_ss, omegaF_ss, omegaR_ss, delta_ss, T_ss];
% find_vals(xtest, R_ss, beta_ss, auxdata)


xdot = find_vals(out, T_ss, R_ss, auxdata)



function xdot = find_vals(x, T, R, param)

Vx = x(1);
Vy = x(2);
omega = x(3);
delta = x(4);
V = sqrt(Vx^2 + Vy^2);
dpsi = V/R;

xdot = car_dynamics(Vx, Vy, dpsi, omega, delta, T, param);
xdot = xdot(1:end-1);
