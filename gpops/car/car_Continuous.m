function output = car_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

% % % %Call continuous dynamics function:
% % % % Xd = Car_Dynamics_Forced(X',F',P)';

%Define input states:
Vx = X(:,1)';
Vy = X(:,2)';
dpsi = X(:,3)';
omegaF = X(:,4)';
omegaR = X(:,5)';

delta = F(:,1)';
torque = F(:,2)';

%Call continuous dynamics function:
Xd = car_dynamics(Vx, Vy, dpsi, omegaF, omegaR, delta, torque, P);

%Get the cost integrand:   (force-squared for now)
costIntegrand = F(:,2).^2; %ones(size(F)); %
% costIntegrand = X(:,1).^2; % anguluar position
% costIntegrand = X(:,1).^2 + X(:,2).^2; % anguluar position + rate


%Pack up the output:
output.dynamics = Xd';
output.integrand = costIntegrand;

end