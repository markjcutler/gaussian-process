function output = mountainCar_Continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
P = input.auxdata;    

%Call continuous dynamics function:
Xd = Mountain_Car_Dynamics_Forced(X',F',P)';

%Get the cost integrand:   (force-squared for now)

% r = polyval(P.coeffs, X(:,1));

x = X(:,1);
s = 0.2;
r = 1 - exp(-(x-1).^2/(2*s^2)) - exp(-(x+1).^2/(2*s^2));

% r2 = 1./(1+exp(-P.sigmoid_gain*(X(:,1)+P.sigmoid_xshift)));
% r3 = 1./(1+exp( P.sigmoid_gain*(X(:,1)-P.sigmoid_xshift)));
% 
% r = r.*r2.*r3;

costIntegrand = r;
% costIntegrand = 10-r.^2; % anguluar position
% costIntegrand = (r-0.3).^2; % anguluar position
% costIntegrand = (X(:,1)+1).^2;

%Pack up the output:
output.dynamics = Xd;
output.integrand = costIntegrand;

end