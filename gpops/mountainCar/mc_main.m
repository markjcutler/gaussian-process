%---------------------------------------------------%
% Pendulum Cart Swing-Up Problem:                   %
%---------------------------------------------------%

clear; clc; 

%Load solution from file?    ( '' for none )
guessFile = ''; %'oldSoln.mat'; %  'oldSoln.mat'  OR   ''

%Physical parameters
% plant parameters
a = 0.15; b = 0.15;
t1 = 1;
c5 = 0; % from p'(0) = 0
c6 = 0; % from p(0) = 0
A = [t1^5, t1^4, t1^3, t1^2; % p(t1) = b
     (-t1)^5, (-t1)^4, (-t1)^3, (-t1)^2; % p(-t1) = a
     5*t1^4, 4*t1^3, 3*t1^2, 2*t1; % p'(t1) = 0
     5*(-t1)^4, 4*(-t1)^3, 3*(-t1)^2, 2*(-t1)]; % p'(-t1) = 0
B = [b;a;0;0];
coeffs = A\B;
coeffs = [coeffs; c5; c6];

auxdata.m = 1;
auxdata.g = -9.81;
auxdata.sigmoid_gain = 35;
auxdata.sigmoid_xshift = 1.3;
auxdata.coeffs = coeffs;
auxdata.vel_coeffs = polyder(coeffs);
auxdata.drag_coeff = 0.2;

% plant parameters
auxdata.rock_a = 0;
auxdata.rock_b = 1;

%Timing parameters:
t0 = 0; 
tfmin = 0; tfmax = 4;

%State parameters [x; dx];
stateInitial =   [0.0001,   0]; %Initial state [x,dx];
stateFinal =     [0,   0];  %Final state [x,dx];
stateBoundLow = -[3, 3];  %[x,dx]
stateBoundUpp =  [3, 3];  %[x,dx]

%Actuator parameters:
forceBoundLow = -2.5;    %(N-m)
forceBoundUpp =  2.5;    %(N-m)

%Integral parameters:
integralGuess = 50;
integralBoundLow = 0;
integralBoundUpp = 100;

%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmax; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = stateInitial; 
bounds.phase.initialstate.upper = stateInitial; 
bounds.phase.state.lower = stateBoundLow; 
bounds.phase.state.upper = stateBoundUpp; 
bounds.phase.finalstate.lower = stateBoundLow; 
bounds.phase.finalstate.upper = stateBoundUpp; 
bounds.phase.control.lower = forceBoundLow; 
bounds.phase.control.upper = forceBoundUpp;
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%

if strcmp(guessFile, '')  %Then use defaults
    guess.phase.time    = [t0; tfmax]; 
    guess.phase.state   = [stateInitial; stateFinal];
    guess.phase.control = [0; 0];
    guess.phase.integral = integralGuess;
else %Load from a data file:
    load(guessFile);
    guess.phase.time = outputPrev.result.solution.phase(1).time;
    guess.phase.state = outputPrev.result.solution.phase(1).state;
    guess.phase.control = outputPrev.result.solution.phase(1).control;
    guess.phase.integral = outputPrev.result.objective;
    
    %Use a mesh that matches with input:
    Npts = length(guess.phase.time);
    nColPts = 4;
    nInterval = floor(Npts/nColPts);
    
    setup.mesh.colpoints = nColPts*ones(1,nInterval);
    setup.mesh.phase(1).fraction = ones(1,nInterval)/nInterval;
    
end

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'MountainCar';
setup.functions.continuous = @mountainCar_Continuous;
setup.functions.endpoint = @mountainCar_Endpoint;
setup.auxdata = auxdata;
setup.bounds = bounds;
setup.guess = guess;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.mesh.method = 'hp-PattersonRao';
setup.mesh.tolerance = 1e-8;
setup.mesh.maxiteration = 45;
setup.mesh.colpointsmin = 4;
setup.mesh.colpointsmax = 10;
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
    plot(Time, State(1,:),'k.','MarkerSize',mSize)
    plot(Time, State(1,:));
    xlabel('time (s)')
    ylabel('position (m)')
    
subplot(2,1,2); hold on;
    plot(Time, State(2,:),'k.','MarkerSize',mSize)
    plot(Time, State(2,:));
    xlabel('time (s)')
    ylabel('velocity (m/s)')
        
figure(3); clf; hold on
    plot(Time, Force,'k.','MarkerSize',mSize)
    plot(Time, Force);
    title(['cost: ' num2str(output.result.objective)])
    xlabel('time (s)')
    ylabel('torque (N-m)')
    
figure(4); clf; hold on
    plot(State(1,:), Force)
    xlabel('Position');
    ylabel('Torque');

figure(5); clf; hold on
    plot(State(2,:), Force)
    xlabel('Velocity');
    ylabel('Torque');
    
figure(6); clf;
    plot3(State(1,:), State(2,:), Force)
    xlabel('Position');
    ylabel('Velocity');
    zlabel('Torque');
    
save('training_mc.mat','Time','State','Force');    

% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
