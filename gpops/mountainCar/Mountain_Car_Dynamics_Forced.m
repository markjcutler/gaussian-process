function Xd = Mountain_Car_Dynamics_Forced(X,F,P_Dyn)
% Xd = Pendulum_Cart_Dynamics_Forced(X,F,P_Dyn)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [th;w]
%   F = [1 x N] actuator force (applied torque)
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [2 x N] derivative of the state matrix [dth;dw]
%
%


x = X(1,:);
dx = X(2,:);

% compute gravity force
slope = polyval(P_Dyn.vel_coeffs, x);
sin_theta = slope./sqrt(1+slope.^2);
gravity_force = P_Dyn.m*P_Dyn.g*sin_theta;

% compute drag force
drag_force = -P_Dyn.drag_coeff*dx;

% compute rock forces
rock_force = 0*drag_force;
for i=1:length(x)
    xx = x(i);
    dxx = dx(i);
    if xx < 0
        if xx < -0.25
            rock_force(i) = -P_Dyn.rock_a*dxx;
        else
            rock_force(i) = -interp1([-0.25, 0], [P_Dyn.rock_a, 0], xx)*dxx;
        end
    else
        if xx > 0.25
            rock_force(i) = -P_Dyn.rock_b*dxx;
        else
            rock_force(i) = -interp1([0, 0.25], [0, P_Dyn.rock_b], xx)*dxx;
        end
    end
end

xdot = dx;
dxdot = (F + gravity_force + drag_force + rock_force)/P_Dyn.m;

Xd = [xdot;dxdot];

end
