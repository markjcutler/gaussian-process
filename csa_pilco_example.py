#!/usr/bin/env python

###################################################
# uncertian_propogation_single_prior.py -- Test whether analytical uncertain propogations match emperical results
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 28 August 2014.
###################################################

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
import matplotlib as mpl

axis_tick_fontsize = 21
mpl.rcParams['xtick.labelsize'] = axis_tick_fontsize
mpl.rcParams['ytick.labelsize'] = axis_tick_fontsize

# define colors
sim_light = '#dfc27d'
sim_dark = '#a6611a'
rw_light = '#80cdc1'
rw_dark = '#018571'
grey = '#9C9C9C'
grey2 = '#ADAEB2'
dark_grey = '#5E5E5E'
dark_yellow = '#FBB131'
light_yellow = '#FEDCA9'

axes_ticks_color = dark_grey

rc('axes', edgecolor=axes_ticks_color, labelcolor=axes_ticks_color)
rc('xtick', color=axes_ticks_color)
rc('ytick', color=axes_ticks_color)
rc('text', color=axes_ticks_color)




def mean_fun(x):
    return np.sin(0.5*x) + x/4


def kernel(x1, x2, sigma_f, l, sigma=0):
    return sigma_f**2/np.sqrt(sigma**2/l**2 + 1)*np.exp(-0.5*(x1 - x2)**2/(l**2 + sigma**2))


def mapping_fun(x):
    return 0.5*x


def K_noise_inv(x, y, sigma_f, l, sigma_w):
    n = len(x)

    K = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            K[i, j] = kernel(x[i], x[j], sigma_f, l)

    return np.linalg.inv(K + sigma_w**2*np.identity(n))


def calc_beta(x, y, sigma_f, l, sigma_w, use_prior=True):
    K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
    if use_prior:
        targets = y - prior_mean_fun(x)
    else:
        targets = y
    beta = np.dot(K_inv, targets)
    return beta


def calc_q(x, mu, sigma, sigma_f, l):
    eta = sigma_f**2/np.sqrt(sigma**2/l**2 + 1)
    q = eta*np.exp(-0.5*(x-mu)**2/(sigma**2 + l**2))
    return q


def gp_post(x_star, x, y, sigma_f, l, sigma_w, comp_cov, cov=0, use_prior=True):
    n_star = len(x_star)
    n = len(x)

    K12 = np.zeros((n, n_star))
    for i in range(n):
        for j in range(n_star):
            K12[i, j] = kernel(x[i], x_star[j], sigma_f, l, cov)

    K21 = K12.transpose()
    if use_prior:
        mean = prior_mean_fun(x_star) + np.dot(K21,
                                           np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w),
                                                  y - prior_mean_fun(x)))
    else:
        mean = np.dot(K21,
                                           np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w),
                                                  y))
    if comp_cov:
        K_star = np.zeros((n_star, n_star))
        for i in range(n_star):
            for j in range(n_star):
                K_star[i, j] = kernel(x_star[i], x_star[j], sigma_f, l, cov)
        cov = K_star - np.dot(K21, np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w), K12))

        return mean, cov
    else:
        return mean

############## PRIOR RBF (really just deterministic GP) ######################
input_prior = np.linspace(-12, 12, 30)
target_prior = mean_fun(input_prior)
print "input_prior: " + str(input_prior)
print "target_prior: " + str(target_prior)
l_prior = 2.5
sigma_f_prior = 1
sigma_w_prior = 0.4
n_prior = len(input_prior)
K_prior = K_noise_inv(input_prior, target_prior, sigma_f_prior, l_prior, sigma_w_prior)


def prior_mean_fun(x):

    try:
        n_star = len(x)
    except:
        n_star = 1
    K12 = np.zeros((n_prior, n_star))
    for i in range(n_prior):
        for j in range(n_star):
            K12[i, j] = kernel(input_prior[i], x[j], sigma_f_prior, l_prior)

    K21 = K12.transpose()
    mean = np.dot(K21, np.dot(K_prior, target_prior))

    return mean




## Hyperparameters
sigma_w = 0.4
sigma_f = 1.0
l = 2.5

# prior
num_func = 25
n_star = 300
x_star = np.linspace(-15, 15, n_star)

# training points
x = np.linspace(-4.5, 4.5, 5) #np.array([-4, -2, 0, 1, 2])
y = mean_fun(x)
mean, cov = gp_post(x_star, x, y, sigma_f, l, sigma_w, True)

##  TEST INPUT
n_star = 3000
mu = -3
sigma = 3.5
if sigma == 0:
    x_uncertain = np.array([mu])
    n_star = 1
else:
    x_uncertain = np.random.normal(loc=mu, scale=sigma, size=n_star)

epdf, bin_edges = np.histogram(x_uncertain, bins=100)
x_epdf = np.linspace(min(x_uncertain), max(x_uncertain), 100)

# certain mean and variance
certain_mean, certain_var = gp_post(np.array([mu]), x, y, sigma_f, l, sigma_w, True)
print "certain_mean: " + str(certain_mean)
print "certain_var: " + str(certain_var)

# send uncertain test input through GP
num_samples = 500
f_samples = np.zeros(n_star*num_samples)
for i in range(n_star):
    f_mean, f_cov = gp_post(np.array([x_uncertain[i]]), x, y, sigma_f, l, sigma_w, True)
    f_samples[i*num_samples:i*num_samples+num_samples] = np.random.normal(loc=f_mean, scale=np.sqrt(f_cov), size=num_samples)
# print f_samples
f_epdf, bin_edges = np.histogram(f_samples, bins=100)
y_fepdf = np.linspace(min(f_samples), max(f_samples), 100)
emean = np.mean(f_samples)
print "Emperical mean: " + str(emean)

evar = np.var(f_samples, ddof=1)
print "Emperical variance: " + str(evar)

# compute moment-matched mean and covariance of propagated values
mean1 = np.dot(calc_beta(x, y, sigma_f, l, sigma_w).transpose(), calc_q(x, mu, sigma, sigma_f, l))
mean2 = np.dot(calc_beta(input_prior, target_prior, sigma_f_prior, l_prior, sigma_w_prior, False),
               calc_q(input_prior, mu, sigma, sigma_f_prior, l_prior))
mean_comp = mean1 + mean2
print "mean prior: " + str(mean2)
print "mean new: " + str(mean1)
print "Calculated mean: " + str(mean_comp)

# compute moment-matched variance
beta = calc_beta(x, y, sigma_f, l, sigma_w)
Q = np.zeros((len(beta), len(beta)))
R = sigma**2*(1/l**2 + 1/l**2) + 1
T = 1/l**2 + 1/l**2 + 1/sigma**2
for i in range(len(beta)):
    for j in range(len(beta)):
        zij = 1/l**2*(x[i] - mu) + 1/l**2*(x[j] - mu)
        Q[i, j] = kernel(x[i], mu, sigma_f, l)*kernel(x[j], mu, sigma_f, l)/np.sqrt(R)*np.exp(0.5*zij**2/T)
alpha = calc_beta(input_prior, target_prior, sigma_f_prior, l_prior, sigma_w_prior, False)
RR = np.zeros((len(alpha), len(alpha)))
R = sigma**2*(1/l_prior**2 + 1/l_prior**2) + 1
T = 1/l_prior**2 + 1/l_prior**2 + 1/sigma**2
for i in range(len(alpha)):
    for j in range(len(alpha)):
        zij = 1/l_prior**2*(input_prior[i] - mu) + 1/l_prior**2*(input_prior[j] - mu)
        RR[i, j] = kernel(input_prior[i], mu, sigma_f_prior, l_prior)*kernel(input_prior[j], mu, sigma_f_prior, l_prior)/np.sqrt(R)*np.exp(0.5*zij**2/T)
part2a = np.dot(beta.transpose(), np.dot(Q, beta))
part2b = np.dot(alpha.transpose(), np.dot(RR, alpha))
Q_tilde = np.zeros((len(alpha), len(beta)))
R = sigma**2*(1/l_prior**2 + 1/l**2) + 1
T = 1/l_prior**2 + 1/l**2 + 1/sigma**2
for i in range(len(alpha)):
    for j in range(len(beta)):
        zij = 1/l_prior**2*(input_prior[i] - mu) + 1/l**2*(x[j] - mu)
        Q_tilde[i, j] = kernel(input_prior[i], mu, sigma_f_prior, l_prior)*kernel(x[j], mu, sigma_f, l)/np.sqrt(R)*np.exp(0.5*zij**2/T)
part2c = 2.0*np.dot(alpha.transpose(), np.dot(Q_tilde, beta))
K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
part1 = sigma_f**2 - np.trace(np.dot(K_inv, Q)) # + sigma_w**2
part3 = mean_comp**2
var_comp = part1 + part2a + part2b + part2c - part3
print "Calculated variance: " + str(var_comp)

alpha = 0.15
linestyle = ['-', '--', '-.', ':']
legend = [r'Mean', r'Existing Data', r'New Data', r'$p_{rw}$']
legend_fontsize = 23
axis_label_fontsize = 23
linewidth = 1.5
annotation_fontsize = 23


ax00 = plt.subplot(221)
ax10 = plt.subplot(223, sharex=ax00)
ax01 = plt.subplot(222, sharey=ax00)

ax00.fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                  mean+1.96*np.sqrt(cov.diagonal()), facecolor='k',
                  edgecolor='k', alpha=alpha)
ax00.plot(x_star, mean, color='k', linewidth=2)
# ax00.plot(x_star, prior_mean_fun(x_star))
# ax10.plot(x_epdf, epdf/np.sum(epdf*(x_epdf[2]-x_epdf[1])))
ax10.plot(x_epdf, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-0.5*(x_epdf-mu)**2/sigma**2), color='k', linewidth=2)
ax01.plot(f_epdf/np.sum(f_epdf*(y_fepdf[2]-y_fepdf[1])), y_fepdf, color='b', linewidth=2, label='Ground Truth')
ax01.plot(1/(np.sqrt(2*var_comp*np.pi))*np.exp(-0.5*(y_fepdf - mean_comp)**2/var_comp), y_fepdf, color='g', linewidth=2, label='Approximation')
# ax01.plot(1/(np.sqrt(2*evar*np.pi))*np.exp(-0.5*(y_fepdf - emean)**2/evar), y_f_epdf)

ax10.set_xlabel(r'$(x_t, u_t)$', size=axis_label_fontsize)
ax00.set_ylabel(r'$x_{t+1}$', size=axis_label_fontsize)
# ax01.set_ylabel(r'$x_{t+1}$', size=axis_label_fontsize)

ax00.set_xlim([-15, 15])
ax10.set_xlim([-15, 15])

ax01.legend(fancybox=True,
            fontsize=legend_fontsize,
            bbox_to_anchor=(0.99, 1.02),
            bbox_transform=plt.gcf().transFigure)


axarr = [ax00, ax01, ax10]

for ax in axarr:
    # ax.set_xlim([-20, 20])
    # ax.set_ylim([-4, 4])

    #ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
    # ax.set_ylabel('Targets', size=axis_label_fontsize)

            # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    # Find at most 101 ticks on the y-axis at 'nice' locations
    max_yticks = 4
    yloc = plt.MaxNLocator(max_yticks)
    ax.yaxis.set_major_locator(yloc)
    xloc = plt.MaxNLocator(max_yticks)
    ax.xaxis.set_major_locator(xloc)


# axarr[0].annotate('Prior', xy=(10, 2),
#                   xycoords='data', xytext=(0, 0),
#                   textcoords='offset points',
#                   bbox=dict(boxstyle="round", fc="1.0",
#                             edgecolor=axes_ticks_color),
#                   fontsize=legend_fontsize)
# axarr[1].annotate('Posterior', xy=(10, 2),
#                   xycoords='data', xytext=(0, 0),
#                   textcoords='offset points',
#                   bbox=dict(boxstyle="round", fc="1.0",
#                             edgecolor=axes_ticks_color),
#                   fontsize=legend_fontsize)
# axarr[1].set_xlabel('Inputs', size=axis_label_fontsize)



plt.savefig('/home/mark/gaussian-process/pilco_example.pdf',
            format='pdf', transparent=True, bbox_inches='tight')
