#!/usr/bin/env python

###################################################
# uncertian_propogation.py -- Test whether analytical uncertain propogations match emperical results
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 28 August 2014.
###################################################

import numpy as np
import matplotlib.pyplot as plt

plt.ion()


def prior_mean_fun(x):
    return 0*(np.sin(0.5*x) + x/4)


def mean_fun(x):
    return np.sin(0.5*x) + x/4


def kernel(x1, x2, sigma=0):
    l = 2.5
    sigma_f = 1
    return sigma_f**2/np.sqrt(sigma**2/l**2 + 1)*np.exp(-0.5*(x1 - x2)**2/(l**2 + sigma**2))


def mapping_fun(x):
    return 0.5*x


def K_noise_inv(x, y):
    n = len(x)
    sigma_n = 0.4

    K = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            K[i, j] = kernel(x[i], x[j])

    return np.linalg.inv(K + sigma_n**2*np.identity(n))


def calc_beta(x, y):
    K_inv = K_noise_inv(x, y)
    beta = np.dot(K_inv, y)
    return beta


def gp_post(x_star, x, y, comp_cov, cov=0):
    n_star = len(x_star)
    n = len(x)
    sigma_n = 0.4

    K = np.zeros((n, n))
    K12 = np.zeros((n, n_star))
    for i in range(n):
        for j in range(n):
            K[i, j] = kernel(x[i], x[j])
        for j in range(n_star):
            K12[i, j] = kernel(x[i], x_star[j], cov)

    K21 = K12.transpose()
    mean = prior_mean_fun(x_star) + np.dot(K21,
                                           np.dot(np.linalg.inv(K + sigma_n**2*np.identity(n)),
                                                  y -
                                                  prior_mean_fun(x)))
    if comp_cov:
        K_star = np.zeros((n_star, n_star))
        for i in range(n_star):
            for j in range(n_star):
                K_star[i, j] = kernel(x_star[i], x_star[j], cov)
        cov = K_star - np.dot(K21, np.dot(np.linalg.inv(K +
                                                        sigma_n**2*np.identity(n)), K12))

        return mean, cov
    else:
        return mean


# prior
num_func = 25
n_star = 300
x_star = np.linspace(-15, 15, n_star)

# training points
x = np.linspace(-4.5, 4.5, 5) #np.array([-4, -2, 0, 1, 2])
y = mean_fun(x)
mean, cov = gp_post(x_star, x, y, True)

##  TEST INPUT
n_star = 3000
mu = 3.5
sigma = 2.9
if sigma == 0:
    x_uncertain = np.array([mu])
    n_star = 1
else:
    x_uncertain = np.random.normal(loc=mu, scale=sigma, size=n_star)

epdf, bin_edges = np.histogram(x_uncertain, bins=100)
x_epdf = np.linspace(min(x_uncertain), max(x_uncertain), 100)

# send uncertain test input through GP
num_samples = 500
f_samples = np.zeros(n_star*num_samples)
for i in range(n_star):
    f_mean, f_cov = gp_post(np.array([x_uncertain[i]]), x, y, True)
    f_samples[i*num_samples:i*num_samples+num_samples] = np.random.normal(loc=f_mean, scale=f_cov, size=num_samples)
# print f_samples
f_epdf, bin_edges = np.histogram(f_samples, bins=100)
y_fepdf = np.linspace(min(f_samples), max(f_samples), 100)
print "Emperical mean: " + str(np.mean(f_samples))

evar = np.var(f_samples)
print "Emperical variance: " +str(evar)

# compute moment-matched mean and covariance of propagated values
mean_comp = gp_post(np.array([mu]), x, y, False, sigma)
print "Calculated mean: " + str(np.mean(mean_comp))

# compute moment-matched variance
beta = calc_beta(x, y)
Q = np.zeros((len(beta), len(beta)))
l = 2.5
sigma_f = 1
sigma_w = 0.4
R = sigma**2*(2/l**2) + 1
T = 2/l**2 + 1/sigma**2
print R
print T
for i in range(len(beta)):
    for j in range(len(beta)):
        zij = (x[i] + x[j] - 2*mu)/l**2
        Q[i, j] = kernel(x[i], mu)*kernel(x[j], mu)/np.sqrt(R)*np.exp(0.5*zij**2/T)
part2 = np.dot(beta.transpose(), np.dot(Q, beta))
K_inv = K_noise_inv(x, y)
#print np.dot(K_inv, Q)
part1 = sigma_f**2 - np.trace(np.dot(K_inv, Q)) + sigma_w**2
part3 = mean_comp**2
var_comp = part1 + part2 - part3
print "part1: " + str(part1)
print "part2: " + str(part2)
print "part3: " + str(part3)
print "Calculated variance:"
print var_comp

plt.clf()
ax00 = plt.subplot(221)
ax10 = plt.subplot(223, sharex=ax00)
ax01 = plt.subplot(222, sharey=ax00)
ax00.fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                   mean+1.96*np.sqrt(cov.diagonal()), facecolor='grey')
ax00.plot(x_star, mean)
ax10.plot(x_epdf, epdf/np.sum(epdf*(x_epdf[2]-x_epdf[1])))
ax10.plot(x_epdf, 1/(sigma*np.sqrt(2*np.pi))*np.exp(-0.5*(x_epdf-mu)**2/sigma**2))
ax01.plot(f_epdf/np.sum(f_epdf*(y_fepdf[2]-y_fepdf[1])), y_fepdf)
ax01.plot(1/(np.sqrt(2*evar*np.pi))*np.exp(-0.5*(y_fepdf - mean_comp)**2/evar), y_fepdf)

plt.show()
