#!/usr/bin/env python

###################################################
# certain_propogation_2gps.py -- propogates a certain
# input through two gp's and combines output based on
#     weights
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday, 16 January 2015.
###################################################

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt


def prior_mean_fun(x):
    return 0 * (np.sin(0.5 * x) + x / 4)


def mean_fun(x):
    return np.sin(0.5 * x)


def kernel(x1, x2, sigma_f, l, sigma=0):
    return sigma_f ** 2 / np.sqrt(sigma ** 2 / l ** 2 + 1) * np.exp(-0.5 * (x1 - x2) ** 2 / (l ** 2 + sigma ** 2))


def K_noise_inv(x, y, sigma_f, l, sigma_w):
    n = len(x)

    K = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            K[i, j] = kernel(x[i], x[j], sigma_f, l)

    return np.linalg.inv(K + sigma_w ** 2 * np.identity(n))


def calc_beta(x, y, sigma_f, l, sigma_w):
    K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
    beta = np.dot(K_inv, y)
    return beta


def gp_post(x_star, x, y, sigma_f, l, sigma_w, comp_cov, cov=0):
    n_star = len(x_star)
    n = len(x)

    K12 = np.zeros((n, n_star))
    for i in range(n):
        for j in range(n_star):
            K12[i, j] = kernel(x[i], x_star[j], sigma_f, l, cov)

    K21 = K12.transpose()
    mean = prior_mean_fun(x_star) + np.dot(K21,
                                           np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w),
                                                  y - prior_mean_fun(x)))
    if comp_cov:
        K_star = np.zeros((n_star, n_star))
        for i in range(n_star):
            for j in range(n_star):
                K_star[i, j] = kernel(x_star[i], x_star[j], sigma_f, l, cov)
        cov = K_star - np.dot(K21, np.dot(K_noise_inv(x, y, sigma_f, l, sigma_w), K12))

        return mean, cov
    else:
        return mean


## Hyperparameters
sigma_w = 0.4
sigma_f = 1.0
l = 2.5

## Simulator Data
# prior
num_func = 25
n_star = 300
x_star = np.linspace(-15, 15, n_star)

# training points
x = np.linspace(-4.5, 4.5, 5)  #np.array([-4, -2, 0, 1, 2])
y = mean_fun(x)
mean, cov = gp_post(x_star, x, y, sigma_f, l, sigma_w, True)

## 'Real World' Data
xrw = np.linspace(-2, 2, 8)
nrw = len(xrw)
yrw = mean_fun(xrw)
for i in range(nrw):
    yrw[i] += np.random.normal(0, 0.03) + 1.4
meanrw, covrw = gp_post(x_star, xrw, yrw, sigma_f, l, sigma_w, True)

# weights:
p_rw = .36
p_sim = 1 - p_rw

##  TEST INPUT -- certain
n_star = 9000
n_star_sim = (int)(n_star * p_sim)
n_star_rw = (int)(n_star * p_rw)
mu = -1.9
sigma = 2.1
if sigma == 0:
    x_uncertain_sim = mu * np.ones(n_star_sim)
    x_uncertain_rw = mu * np.ones(n_star_rw)
else:
    x_uncertain_sim = np.random.normal(loc=mu, scale=sigma, size=n_star_sim)
    x_uncertain_rw = np.random.normal(loc=mu, scale=sigma, size=n_star_rw)


def prop_through_gp(mu, sigma, n_star, x, y, sigma_f, l, simga_w, x_uncertain=None):

    if n_star == 0:
        return 0, 0, 0, 0, 0, 0

    ##  TEST INPUT
    if x_uncertain is None:
        if sigma == 0:
            x_uncertain = mu * np.ones(n_star)
            n_star = 1
        else:
            x_uncertain = np.random.normal(loc=mu, scale=sigma, size=n_star)

    # send uncertain test input through GP
    f_samples = np.zeros(n_star)
    for i in range(n_star):
        f_mean, f_cov = gp_post(np.array([x_uncertain[i]]), x, y, sigma_f, l, sigma_w, True)
        f_samples[i] = np.random.normal(loc=f_mean, scale=np.sqrt(f_cov))
    # print f_samples
    f_epdf, bin_edges = np.histogram(f_samples, bins=100)
    y_fepdf = np.linspace(min(f_samples), max(f_samples), 100)
    mean = np.mean(f_samples)
    var = np.var(f_samples, ddof=1)

    return mean, var, x_uncertain, y_fepdf, f_epdf, f_samples


emean_sim, evar_sim, x_uncertain_sim, y_fepdf_sim, f_epdf_sim, f_samples_sim = prop_through_gp(mu, sigma, n_star_sim, x, y, sigma_f, l, sigma_w)
emean_rw, evar_rw, x_uncertain_rw, y_fepdf_rw, f_epdf_rw, f_samples_rw = prop_through_gp(mu, sigma, n_star_rw, xrw, yrw, sigma_f, l, sigma_w)

x_uncertain = np.append(x_uncertain_rw, x_uncertain_sim)

epdf, bin_edges = np.histogram(x_uncertain, bins=100)
x_epdf = np.linspace(min(x_uncertain), max(x_uncertain), 100)


# combines two normals: http://en.wikipedia.org/wiki/Mixture_distribution
def weighted_normals(p1, m1, v1, m2, v2):
    p2 = 1 - p1
    m = p1 * m1 + p2 * m2
    v = p1 * ((m1 - m) ** 2 + v1) + p2 * ((m2 - m) ** 2 + v2)
    return m, v

f_samples = np.append(f_samples_rw, f_samples_sim)


def emperical_stats(samples):
    f_epdf, bin_edges = np.histogram(samples, bins=100)
    y_fepdf = np.linspace(min(samples), max(samples), 100)
    emperical_mean = np.mean(samples)
    emperical_var = np.var(samples, ddof=1)
    return f_epdf, y_fepdf, emperical_mean, emperical_var

f_epdf, y_fepdf, emperical_mean, emperical_var = emperical_stats(f_samples)

emean, evar = weighted_normals(p_sim, emean_sim, evar_sim,
                               emean_rw, evar_rw)
print "Estimated emperical mean: " + str(emean)
print "Estimated emperical variance: " + str(evar)


def moment_match(mu, x, y, sigma_f, l, sigma_w, sigma):
    # mean
    m = gp_post(np.array([mu]), x, y, sigma_f, l, sigma_w, False, sigma)

    # compute moment-matched variance
    beta = calc_beta(x, y, sigma_f, l, sigma_w)
    Q = np.zeros((len(beta), len(beta)))
    R = sigma ** 2 * (1 / l ** 2 + 1 / l ** 2) + 1
    if sigma == 0:
        T = np.inf
    else:
        T = 1 / l ** 2 + 1 / l ** 2 + 1 / sigma ** 2
    for i in range(len(beta)):
        for j in range(len(beta)):
            zij = 1 / l ** 2 * (x[i] - mu) + 1 / l ** 2 * (x[j] - mu)
            Q[i, j] = kernel(x[i], mu, sigma_f, l) * kernel(x[j], mu, sigma_f, l) / np.sqrt(R) * np.exp(
                0.5 * zij ** 2 / T)
    part2 = np.dot(beta.transpose(), np.dot(Q, beta))
    K_inv = K_noise_inv(x, y, sigma_f, l, sigma_w)
    part1 = sigma_f ** 2 - np.trace(np.dot(K_inv, Q))  # + sigma_w**2
    part3 = m ** 2
    v = part1 + part2 - part3

    # compute input-output covariance
    eta = sigma_f ** 2 / np.sqrt(sigma ** 2 / l ** 2 + 1)
    cov = 0
    for i in range(len(beta)):
        qi = eta * np.exp(-0.5 * (x[i] - mu) ** 2 / (sigma ** 2 + l ** 2))
        cov += beta[i] * qi * sigma ** 2 / (sigma ** 2 + l ** 2) * (x[i] - mu)

    return m, v, cov


mean_comp_sim, var_comp_sim, covar_sim = moment_match(mu, x, y, sigma_f, l, sigma_w, sigma)
mean_comp_rw, var_comp_rw, covar_rw = moment_match(mu, xrw, yrw, sigma_f, l, sigma_w, sigma)
mean_comp, var_comp = weighted_normals(p_sim, mean_comp_sim, var_comp_sim, mean_comp_rw, var_comp_rw)
print "Calculated mean: " + str(np.mean(mean_comp))
print "Calculated variance: " + str(var_comp)


## compute input-output covariance
# estimated
# cov_est_sim = np.cov(x_uncertain_sim, f_samples_sim)
# print "Covariance est sim: " + str(cov_est_sim[0][1])
# print "Covariance calc sim: " + str(covar_sim)
# cov_est_rw = np.cov(x_uncertain_rw, f_samples_rw)
# print "Covariance est rw: " + str(cov_est_rw[0][1])
# print "Covariance calc rw: " + str(covar_rw)
# cov_est = np.cov(x_uncertain, f_samples)
# covar = covar_sim * p_sim + covar_rw * p_rw
# print "Covariance est: " + str(cov_est[0][1])
# print "Covariance calc: " + str(covar)


def empirical_derivm(p_sim, mu, x, y, xrw, yrw, sigma_f, l, simga_w, sigma):
    fdiff = 1e-2
    n_star = 5000*500
    n_star_sim = (int)(n_star * p_sim)
    n_star_rw = (int)(n_star * (1-p_sim))

    x_uncertain_sim = np.random.normal(loc=mu, scale=sigma, size=n_star_sim)
    x_uncertain_rw = np.random.normal(loc=mu, scale=sigma, size=n_star_rw)
    x_uncertain1_sim = x_uncertain_sim - fdiff
    x_uncertain2_sim = x_uncertain_sim + fdiff
    x_uncertain1_rw = x_uncertain_rw - fdiff
    x_uncertain2_rw = x_uncertain_rw + fdiff

    m1sim, v1sim, xf, yf, fe, fs = prop_through_gp(mu-fdiff, sigma, n_star_sim, x, y, sigma_f, l, sigma_w, x_uncertain1_sim)
    m1rw, v1rw, xf, yf, fe, fs = prop_through_gp(mu-fdiff, sigma, n_star_rw, xrw, yrw, sigma_f, l, sigma_w, x_uncertain1_rw)
    m2sim, v2sim, xf, yf, fe, fs = prop_through_gp(mu+fdiff, sigma, n_star_sim, x, y, sigma_f, l, sigma_w, x_uncertain2_sim)
    m2rw, v2rw, xf, yf, fe, fs = prop_through_gp(mu+fdiff, sigma, n_star_rw, xrw, yrw, sigma_f, l, sigma_w, x_uncertain2_rw)

    m1, v1 = weighted_normals(p_sim, m1sim, v1sim, m1rw, v1rw)
    m2, v2 = weighted_normals(p_sim, m2sim, v2sim, m2rw, v2rw)

    dmdm = (m2-m1)/(2*fdiff)
    dsdm = (v2-v1)/(2*fdiff)
    return dmdm, dsdm


def closed_form_derivm(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma):
    fdiff = 1e-2
    m1sim, v1sim, c1sim = moment_match(mu-fdiff, x, y, sigma_f, l, sigma_w, sigma)
    m1rw, v1rw, c1rw = moment_match(mu-fdiff, xrw, yrw, sigma_f, l, sigma_w, sigma)
    m2sim, v2sim, c2sim = moment_match(mu+fdiff, x, y, sigma_f, l, sigma_w, sigma)
    m2rw, v2rw, c2rw = moment_match(mu+fdiff, xrw, yrw, sigma_f, l, sigma_w, sigma)
    m1, v1 = weighted_normals(p_sim, m1sim, v1sim, m1rw, v1rw)
    m2, v2 = weighted_normals(p_sim, m2sim, v2sim, m2rw, v2rw)
    dmdm = (m2-m1)/(2*fdiff)
    dsdm = (v2-v1)/(2*fdiff)

    return dmdm, dsdm


def empirical_derivs(p_sim, mu, x, y, xrw, yrw, sigma_f, l, simga_w, sigma):
    fdiff = 1e-2
    n_star = 5000*500
    n_star_sim = (int)(n_star * p_sim)
    n_star_rw = (int)(n_star * (1-p_sim))

    x_uncertain1_sim = np.random.normal(loc=mu, scale=sigma-fdiff, size=n_star_sim)
    x_uncertain1_rw = np.random.normal(loc=mu, scale=sigma-fdiff, size=n_star_rw)
    x_uncertain2_sim = np.random.normal(loc=mu, scale=sigma+fdiff, size=n_star_sim)
    x_uncertain2_rw = np.random.normal(loc=mu, scale=sigma+fdiff, size=n_star_rw)

    m1sim, v1sim, xf, yf, fe, fs = prop_through_gp(mu-fdiff, sigma, n_star_sim, x, y, sigma_f, l, sigma_w, x_uncertain1_sim)
    m1rw, v1rw, xf, yf, fe, fs = prop_through_gp(mu-fdiff, sigma, n_star_rw, xrw, yrw, sigma_f, l, sigma_w, x_uncertain1_rw)
    m2sim, v2sim, xf, yf, fe, fs = prop_through_gp(mu+fdiff, sigma, n_star_sim, x, y, sigma_f, l, sigma_w, x_uncertain2_sim)
    m2rw, v2rw, xf, yf, fe, fs = prop_through_gp(mu+fdiff, sigma, n_star_rw, xrw, yrw, sigma_f, l, sigma_w, x_uncertain2_rw)

    m1, v1 = weighted_normals(p_sim, m1sim, v1sim, m1rw, v1rw)
    m2, v2 = weighted_normals(p_sim, m2sim, v2sim, m2rw, v2rw)

    dmds = (m2-m1)/(2*fdiff)
    dsds = (v2-v1)/(2*fdiff)
    return dmds, dsds


def closed_form_derivs(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma):
    fdiff = 1e-2
    m1sim, v1sim, c1sim = moment_match(mu, x, y, sigma_f, l, sigma_w, sigma-fdiff)
    m1rw, v1rw, c1rw = moment_match(mu, xrw, yrw, sigma_f, l, sigma_w, sigma-fdiff)
    m2sim, v2sim, c2sim = moment_match(mu, x, y, sigma_f, l, sigma_w, sigma+fdiff)
    m2rw, v2rw, c2rw = moment_match(mu, xrw, yrw, sigma_f, l, sigma_w, sigma+fdiff)
    m1, v1 = weighted_normals(p_sim, m1sim, v1sim, m1rw, v1rw)
    m2, v2 = weighted_normals(p_sim, m2sim, v2sim, m2rw, v2rw)
    dmds = (m2-m1)/(2*fdiff)
    dsds = (v2-v1)/(2*fdiff)

    return dmds, dsds



dmdm, dsdm = closed_form_derivm(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma)
edmdm, edsdm = empirical_derivm(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma)
dmds, dsds = closed_form_derivs(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma)
edmds, edsds = empirical_derivs(p_sim, mu, x, y, xrw, yrw, sigma_f, l, sigma_w, sigma)

print "Finite difference dmdm: " + str(dmdm)
print "Finite difference empirical dmdm: " + str(edmdm)
print "Finite difference dsdm: " + str(dsdm)
print "Finite difference empirical dsdm: " + str(edsdm)

plt.clf()
ax00 = plt.subplot(221)
ax10 = plt.subplot(223, sharex=ax00)
ax01 = plt.subplot(222, sharey=ax00)
ax00.fill_between(x_star, mean - 1.96 * np.sqrt(cov.diagonal()),
                  mean + 1.96 * np.sqrt(cov.diagonal()), facecolor='blue', alpha=0.2)
ax00.plot(x_star, mean)
ax00.fill_between(x_star, meanrw - 1.96 * np.sqrt(covrw.diagonal()),
                  meanrw + 1.96 * np.sqrt(covrw.diagonal()), facecolor='green', alpha=0.2)
ax00.plot(x_star, meanrw)
if sigma == 0:
    ax10.plot(mu, 0.0, '*')
else:
    ax10.plot(x_epdf, epdf / np.sum(epdf * (x_epdf[2] - x_epdf[1])))
    ax10.plot(x_epdf, 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-0.5 * (x_epdf - mu) ** 2 / sigma ** 2))
ax01.plot(f_epdf / np.sum(f_epdf * (y_fepdf[2] - y_fepdf[1])), y_fepdf)
if n_star_rw > 0:
    ax01.plot(f_epdf_rw / np.sum(f_epdf_rw * (y_fepdf_rw[2] - y_fepdf_rw[1])), y_fepdf_rw)
ax01.plot(f_epdf_sim / np.sum(f_epdf_sim * (y_fepdf_sim[2] - y_fepdf_sim[1])), y_fepdf_sim)
ax01.plot(1 / (np.sqrt(2 * var_comp * np.pi)) * np.exp(-0.5 * (y_fepdf - mean_comp) ** 2 / var_comp), y_fepdf)
ax01.plot(1 / (np.sqrt(2 * evar * np.pi)) * np.exp(-0.5 * (y_fepdf - mean_comp) ** 2 / evar), y_fepdf)
if n_star_rw > 0:
    ax01.plot(1 / (np.sqrt(2 * evar_rw * np.pi)) * np.exp(
        -0.5 * (y_fepdf_rw - emean_rw) ** 2 / evar_rw), y_fepdf_rw)
ax01.plot(1 / (np.sqrt(2 * evar_sim * np.pi)) * np.exp(
    -0.5 * (y_fepdf_sim - emean_sim) ** 2 / evar_sim), y_fepdf_sim)

plt.show()
