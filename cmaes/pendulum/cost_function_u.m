function total_cost = cost_function_u(u)%params)

x0 = [0.1, 0];


% x0 = [0.1, 0];
policy_start = [sin(x0(1)) cos(x0(1)) x0(2)];

% nc = 20;                       % size of controller training set
policy.maxU = 2.5;                                        % max. amplitude of 
                                                          % torque
ns_p = length(policy_start);
ns = length(x0);
nu = length(policy.maxU);
nl = 4;
                                                          
% %% decision variables
% m = [sin(x0(1)) cos(x0(1)) x0(2)];
% S = diag([0.00001, 0.01, 0.01]);
% var.M = gaussian(m, S, nc);
% var.t = 0.1*randn(nc, nu);
% var.l = log([0.7 0.7 1])';

% var.M = reshape(decision_vars(1:nc*ns_p), [nc,ns_p])';
% var.t = decision_vars(nc*ns_p+1:nc*(ns_p+1));
% var.l = decision_vars(end-ns_p+1:end);

%% parameters
P_Dyn.massPend = 1;    %Pendulum mass
P_Dyn.frictionCoeff = 0; % coefficient of friction
P_Dyn.lengthPendulum = 1;    %Pendulum length
P_Dyn.gravity = 9.81;    %Gravity


% 2. Set up the scenario
dt = 0.1/2;                      % [s] sampling time
T = 3;                         % [s] prediction time
H = ceil(T/dt);                % prediction steps (optimization horizon)


% 4. Set up the policy structure
% policy.fcn = @(policy,m,s)conCat(@congp,@gSat,policy,m,s);%@(policy,m,s)conCat(@congp,@gSat,policy,m,s);% controller 
%                                                           % representation
% policy.p.inputs = var.M'; % init. location of 
%                                                           % basis functions
% policy.p.targets = var.t;    % init. policy targets 
%                                                           % (close to zero)
% policy.p.hyp = [var.l; log([1 0.01]')];                  % initialize 
                                                          % hyper-parameters

% dynamics = @dynamics_inverted_pend;


% 5. Set up the cost structure
cost.fcn = @loss_pendulum;                           % cost function
cost.gamma = 1;                                      % discount factor
cost.p = 1.0;                                        % length of pendulum
cost.width = 0.5;                                    % cost function width
cost.target = [0 0 sin(0) cos(0)]';                               % target state
cost.L = 1;
cost.z = cost.target;
Q = zeros(nl);
Q(3,3) = cost.L^2;
Q(4,4) = cost.L^2;
cost.W = Q/cost.width^2;


s = x0';
sp = [sin(s(1)); cos(s(1)); s(2)];
% u = zeros(H,nu);
latent = zeros(H+1, ns+nu);
L = zeros(1, H);

OPTIONS = odeset('RelTol', 1e-3, 'AbsTol', 1e-6);

for i = 1:H % --------------------------------------------- generate trajectory
%     tic
  
  % 1. Apply policy ... or random actions --------------------------------------
%   if isfield(policy, 'fcn')
%     u(i,:) = policy.fcn(policy,sp,zeros(ns_p));
% %     u(i,:) = [8.2487*pi/180, 1232.9];
%   else
%     u(i,:) = policy.maxU.*(2*rand(1,nu)-1);
% %     u(i,:) = [8.2487*pi/180, 1232.9]; %[0.3, 100];
%   end
  latent(i,:) = [s' u(i,:)];                                  % latent state

  % 2. Simulate dynamics ------------------------------------------------------
  tspan = [0 dt/2 dt]; 
  [~, y] = ode45(@(t,y) dynamics_inverted_pend(t,y,u(i,:),P_Dyn), tspan, s, OPTIONS);
    s = y(3,:)';                                                 % next state 
	sp = [sin(s(1)); cos(s(1)); s(2)];


  % 4. Augment state and randomize ---------------------------------------------
%   state(simi) = next(simi); state(augi) = plant.augment(state);
%   x(i+1,simi) = state(simi) + randn(size(simi))*chol(plant.noise);
%   x(i+1,augi) = plant.augment(x(i+1,:));
  
  % 5. Compute Cost ------------------------------------------------------------
  sl = [s; sin(s(1)); cos(s(1))];
  L(i) = lossSat(cost, sl, zeros(nl));
%   toc
end
total_cost = sum(L);