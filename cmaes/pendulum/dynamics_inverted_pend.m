function Xd = dynamics_inverted_pend(t,y,F,P_Dyn)
% Xd = Pendulum_Cart_Dynamics_Forced(X,F,P_Dyn)
%
% FUNCTION:
%   This function is used to compute the dynamics of the inverted pendulum
%   system. The inputs are the system state, actuator force, and paramters.
%   
% INPUTS:
%
%   X = [2 x N] state matrix: [th;w]
%   F = [1 x N] actuator force (applied torque)
%   P_Dyn = a parameter struct with fields:
%       M = pendulum mass
%       L = pendulum length
%       b = friction coefficient
%       g = gravitational acceleration
%
% OUTPUTS:
%   dX = [2 x N] derivative of the state matrix [dth;dw]
%
%
th = y(1);
w = y(2);

%Define Parameters
M = P_Dyn.massPend;    %Pendulum mass
b = P_Dyn.frictionCoeff; % coefficient of friction
L = P_Dyn.lengthPendulum;    %Pendulum length
g = P_Dyn.gravity;    %Gravity

%Define input states:
% th %Angle of the pendulum: 0 = straight up, pi = straight down
% w %Angular rate of the pendulum


inertia = M*L^2/3;
gravity_torque = M*g*L*sin(th)/2;

wd = (F - b*w + gravity_torque) / inertia;
thd = w;

%Express as a vector
Xd = [thd;wd];

end