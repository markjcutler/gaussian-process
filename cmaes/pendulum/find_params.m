% find_params.m

% function cost = cost_function(x0, var)%params)

% include some paths
try
  rd = '../../pilcoV0.9/';
  addpath([rd 'base'],[rd 'util'],[rd 'gp'],[rd 'control'],[rd 'loss'],[rd 'test']);
catch
end

x0 = [0.1, 0];
policy_start = [sin(x0(1)) cos(x0(1)) x0(2)];

nc = 20;                       % size of controller training set

nu = 1;
                                                          
%% decision variables
m = [sin(x0(1)) cos(x0(1)) x0(2)];
ns = length(m);
S = diag([0.00001, 0.01, 0.01]);
M = gaussian(m, S, nc)';
t = 0.1*randn(nc, nu);
l = log([0.7 0.7 1])';

dv = [reshape(M, [nc*ns, 1]); t; l];

H = 60;
u = 2*rand(H,nu) - 1;
u = 0.0001*u;

% c = cost_function(dv,x0,nc)

options = optimset('Display', 'iter');
% x = fminsearch(@(x) cost_function(x,x0,nc), dv, options)
% x = fminsearch(@(x) cost_function_u(x,x0,nc), u, options)

x = cmaes('cost_function_u', u, 1)