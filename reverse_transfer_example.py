#!/usr/bin/env python

###################################################
# reverse_transfer_example.py -- simple example of reverse transfer for MFRL using GPs
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 18 December 2014.
###################################################

import numpy as np
import matplotlib.pyplot as plt
import copy


def mean_fun(x):
    return np.sin(0.2*x) #+ x/4


def kernel(x1, x2):
    l = 2.5
    return np.exp(-0.5/l**2*np.linalg.norm(x1 - x2)**2)

# plotting
n_star = 300
x_star = np.linspace(-15, 15, n_star)
K_star = np.zeros((n_star, n_star))
for i in range(n_star):
    for j in range(n_star):
        K_star[i, j] = kernel(x_star[i], x_star[j])


# training points
x = np.linspace(-15, 15, 25)
n = len(x)
f = mean_fun(x)
for i in range(n):
    f[i] += np.random.normal(0, 0.03)

K = np.zeros((n, n))
K12 = np.zeros((n, n_star))
K21 = np.zeros((n_star, n))
for i in range(n):
    for j in range(n):
        K[i, j] = kernel(x[i], x[j])
    for j in range(n_star):
        K12[i, j] = kernel(x[i], x_star[j])
for i in range(n_star):
    for j in range(n):
        K21[i, j] = kernel(x_star[i], x[j])

sigma = 0.1
mean = np.dot(K21, np.dot(np.linalg.inv(K + sigma**2*np.identity(n)), f))
cov = K_star - np.dot(K21, np.dot(np.linalg.inv(K + sigma**2*np.identity(n)), K12))

fig, ax = plt.subplots()
ax.plot(x, f, '*', color='g')

ax.plot(x_star, mean, color='g', label='Simulator')
ax.fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                   mean+1.96*np.sqrt(cov.diagonal()), facecolor='g', alpha=0.1)


# training points
x = np.linspace(2, 10, 8)
n = len(x)
f = mean_fun(x)
for i in range(n):
    f[i] += np.random.normal(0, 0.03) + 0.4

K = np.zeros((n, n))
K12 = np.zeros((n, n_star))
K21 = np.zeros((n_star, n))
for i in range(n):
    for j in range(n):
        K[i, j] = kernel(x[i], x[j])
    for j in range(n_star):
        K12[i, j] = kernel(x[i], x_star[j])
for i in range(n_star):
    for j in range(n):
        K21[i, j] = kernel(x_star[i], x[j])

sigma = 0.1
mean = np.dot(K21, np.dot(np.linalg.inv(K + sigma**2*np.identity(n)), f))
cov = K_star - np.dot(K21, np.dot(np.linalg.inv(K + sigma**2*np.identity(n)), K12))

ax.plot(x, f, '*', color='b')

ax.plot(x_star, mean, color='b', label='Real World')
ax.fill_between(x_star, mean-1.96*np.sqrt(cov.diagonal()),
                   mean+1.96*np.sqrt(cov.diagonal()), facecolor='b', alpha=0.1)
ax.plot([2.1, 2.1], [-1.5, 2.0], 'k--')
ax.plot([9.5, 9.5], [-1.5, 2.0], 'k--')
ax.axis('off')
ax.legend(loc=2)

plt.show()

#path = '/home/mark/gaussian-process/gp_reverse_transfer.pdf'
#plt.savefig(path, format='pdf', transparent=True, bbox_inches='tight')
