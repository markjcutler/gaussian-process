#!/usr/bin/env python

###################################################
# local min
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Thursday, 18 December 2014.
###################################################

import numpy as np
import matplotlib.pyplot as plt


def fun(x):
    return 20 + (x**4 - 7*x**3 + 6*x**2 + 4*x - 16)


# training points
x = np.linspace(-3, 7, 100)
f = fun(x)
xp = x + 1.5

xmin = 4.5408
fig, ax = plt.subplots()
ax.plot(x, f, linewidth=2)
ax.plot(xp, f+20, linewidth=2)
ax.plot(xmin+1.5, fun(xmin)+20, 'o', color='g', markersize=7)
ax.plot(xmin+1.5, fun(xmin+1.5), 'o', color='b', markersize=7)
ax.plot([xmin+1.5, xmin+1.5], [-90, 250], 'k--')
ax.legend(['Real World', 'Simulator'])
ax.axis('off')

path = '/home/mark/gaussian-process/local_min.pdf'
plt.savefig(path, format='pdf', transparent=True, bbox_inches='tight')
